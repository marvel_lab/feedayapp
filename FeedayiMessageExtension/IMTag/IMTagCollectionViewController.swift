//
//  IMTagCollectionViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/11/3.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import FeedayCommonKit
import FeedayUIKit

private let cellID = "IMTagCollectionCell"

class IMTagCollectionViewController: UICollectionViewController {
    
    var viewModel = IMTagViewModel()
    
    @IBOutlet weak var collectionLayout: UICollectionViewFlowLayout!
    
    private var sizingCell: IMTagCollectionCell?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cellNib = UINib(nibName: cellID, bundle: nil)
        collectionView?.registerNib(cellNib, forCellWithReuseIdentifier: cellID)
        sizingCell = cellNib.instantiateWithOwner(nil, options: nil).first as? IMTagCollectionCell
        
        TagsManager.sharedManager.loadTags()
        
        TagsManager.sharedManager.tagListSignal.asObservable()
            .subscribeNext { [weak self] (_) in
                self?.collectionView?.reloadData()
            }
            .addDisposableTo(disposeBag)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        collectionView?.reloadData()
    }
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.getTagList().count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellID, forIndexPath: indexPath) as! IMTagCollectionCell
        
        if let tag = viewModel.getTag(atIndex: indexPath.item) {
            cell.model = tag
        }
        cell.updateUI()
        
        return cell
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        collectionView.deselectItemAtIndexPath(indexPath, animated: true)
        
        guard let tag = viewModel.getTag(atIndex: indexPath.item) else { return }
        
        if let vc = IMImageViewController.instantiateViewController() {
            vc.viewModel.currentTag = tag
            vc.viewModel.currentMainTab = .Tags
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension IMTagCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if let cell = sizingCell, tag = viewModel.getTag(atIndex: indexPath.item) {
            cell.model = tag
            cell.updateUI()
            return cell.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)
        }
        
        return CGSizeZero
    }
}

extension IMTagCollectionViewController: Rxable {}

extension IMTagCollectionViewController: Instantiatable {
    
    static func storyboardName() -> String {
        return StoryboardName.TagCollection.rawValue
    }
    
    static func storyboardID() -> String {
        return "IMTagCollectionViewController"
    }
}
