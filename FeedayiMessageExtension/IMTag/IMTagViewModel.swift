//
//  IMTagViewModel.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/11/3.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import Foundation
import InstagramKit
import FeedayCommonKit

class IMTagViewModel {
    
    func getTagList() -> [InstagramTag] {
        return TagsManager.sharedManager.tagList
    }
    
    func getTag(atIndex index: Int) -> InstagramTag? {
        let list = getTagList()
        guard index < list.count else {
            return nil
        }
        
        return list[index]
    }
    
}
