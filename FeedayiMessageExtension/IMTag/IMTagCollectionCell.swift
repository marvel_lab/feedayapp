//
//  IMTagCollectionCell.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/11/3.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import FeedayUIKit
import InstagramKit
import FeedayCommonKit
import RxSwift
import RxCocoa

class IMTagCollectionCell: UICollectionViewCell {
    
    var model = InstagramTag()
    
    @IBOutlet weak var tagLabel: UILabel!
    @IBOutlet weak var blockView: UIView!
    @IBOutlet weak var bgView: UIView!
    
    private var gradientLayer: CAGradientLayer?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let gLayer = CAGradientLayer.init()
        gLayer.frame = bgView.layer.bounds
        gLayer.colors = [UIColor.f2StrongPinkColor().CGColor, UIColor.f2OrangeRedColor().CGColor]
        gLayer.locations = [0, 1]
        gLayer.startPoint = CGPointMake(0, 0.5)
        gLayer.endPoint = CGPointMake(1, 0.5)
        gLayer.cornerRadius = bgView.layer.cornerRadius
        bgView.layer.addSublayer(gLayer)
        self.gradientLayer = gLayer
        
        bgView
            .rx_observe(CGRect.self, "bounds")
            .subscribeNext { [weak self] (rect) in
                guard let this = self else { return }
                guard let rect = rect else { return }
                this.bgView.layer.frame = rect
                this.gradientLayer?.frame = rect
            }
            .addDisposableTo(disposeBag)
    }
    
    func updateUI() {
        tagLabel.text = "#" + model.name ?? ""
        blockView.hidden = true
        bgView.hidden = false
    }
}

extension IMTagCollectionCell: Rxable {}
