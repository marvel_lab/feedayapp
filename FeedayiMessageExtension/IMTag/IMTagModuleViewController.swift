//
//  IMTagModuleViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/11/3.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import FeedayUIKit

class IMTagModuleViewController: UIViewController {

    var viewModel = IMTagViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let vc = segue.destinationViewController as? IMTagCollectionViewController {
            vc.viewModel = viewModel
        }
    }
}

extension IMTagModuleViewController: Instantiatable {
    
    static func storyboardID() -> String {
        return "IMTagModuleViewController"
    }
    
    static func storyboardName() -> String {
        return StoryboardName.TagCollection.rawValue
    }
}
