//
//  IMSearchPeopleViewModel.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/11/8.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import Foundation
import RxSwift
import InstagramKit
import FeedayCommonKit

class IMSearchPeopleViewModel: NSObject, Rxable {
    
    var rx_searchText = Variable<String>("")
    
    var rx_searching = Variable<Bool>(false)
    
    var rx_errorString = Variable<String>("")
    
    var rx_users = Variable<[InstagramUser]>([])
    
    var rx_selectedUser = Variable<InstagramUser?>(nil)
    
    var rx_cancelSearch = PublishSubject<Void>()
    
    var users: [InstagramUser] {
        return rx_users.value
    }
    
    var selectedUser: InstagramUser? {
        get {
            return rx_selectedUser.value
        }
        set {
            rx_selectedUser.value = newValue
        }
    }
    
    var searchText: String {
        get {
            return rx_searchText.value
        }
        set {
            rx_searchText.value = newValue
        }
    }
    
    func search() {
        rx_cancelSearch.onNext()
        
        rx_searching.value = true
        let username = rx_searchText.value.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        SearchPeopleRequest()
            .rx_request(username)
            .takeUntil(rx_cancelSearch)
            .doOn { [weak self] (_) in
                self?.rx_searching.value = false
            }
            .doOnError { (error) in
                let err = error as NSError
                let str = err.localizedDescription
                self.rx_errorString.value = str
            }
            .subscribeNext { (users) in
                self.rx_users.value = users
            }
            .addDisposableTo(disposeBag)
    }
    
    func clear() {
        rx_cancelSearch.onNext()
        rx_users.value = []
    }
    
    func getUser(forIndex index: Int) -> InstagramUser? {
        guard index < users.count else { return nil }
        return users[index]
    }
    
    func getImageViewModel() -> IMImageViewModel {
        let vm = IMImageViewModel()
        vm.currentMainTab = .Search
        vm.currentUser = selectedUser
        return vm
    }
    
}
