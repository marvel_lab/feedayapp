//
//  IMSearchPeopleViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/11/8.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import Messages
import RxSwift
import RxCocoa
import MBProgressHUD
import FeedayCommonKit
import FeedayUIKit

private let cellID = "IMSearchPeopleCell"
private let imageCollectionSegue = "ImageCollection"

class IMSearchPeopleViewController: UITableViewController {
    
    var viewModel = IMSearchPeopleViewModel()
    
    @IBOutlet weak var searchTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initSearchTextField()
        observeSearching()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.users.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellID, forIndexPath: indexPath) as! IMSearchPeopleCell
        
        if let user = viewModel.getUser(forIndex: indexPath.row) {
            cell.user = user
        }
        
        cell.updateUI()
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        guard let user = viewModel.getUser(forIndex: indexPath.row) else { return }
        
        viewModel.selectedUser = user
        
        if let vc = IMImageViewController.instantiateViewController() {
            vc.viewModel = viewModel.getImageViewModel()
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func searchButtonPressed() {
        view.endEditing(true)
        viewModel.search()
    }
    
    @IBAction func cancelButtonPressed() {
        view.endEditing(true)
        searchTextField.text = ""
        viewModel.clear()
        enterCompactMode()
    }
    
    @IBAction func searchTextFieldActivated() {
        //enterExpandMode()
    }
    
    private func initSearchTextField() {
        let txt = "Search people and brands"
        let str = NSMutableAttributedString.init(string: txt)
        let range = NSMakeRange(0, txt.characters.count)
        
        str.addAttributes([NSFontAttributeName: FeedayConstant.regularFont(15)], range: range)
        str.addAttributes([NSForegroundColorAttributeName: UIColor.init(hexString: "#929292")!], range: range)
        searchTextField.attributedPlaceholder = str
    }
    
    private func observeSearching() {
        viewModel
            .rx_searching
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribeNext { [weak self] (searching) in
                guard let this = self else { return }
                
                if searching == true {
                    MBProgressHUD.showHUDAddedTo(this.view, animated: true)
                } else {
                    MBProgressHUD.hideHUDForView(this.view, animated: true)
                }
            }
            .addDisposableTo(disposeBag)
        
        viewModel
            .rx_users
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribeNext { [weak self] (_) in
                self?.tableView.reloadData()
            }
            .addDisposableTo(disposeBag)
        
        searchTextField
            .rx_text
            .bindTo(viewModel.rx_searchText)
            .addDisposableTo(disposeBag)
        
        IMGlobalState
            .sharedState
            .rx_presentStyle
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribeNext { [weak self] (style) in
                if style == .Expanded {
                    self?.searchTextField.becomeFirstResponder()
                }
            }
            .addDisposableTo(disposeBag)
    }
    
    private func enterExpandMode() {
        RxEventHub
            .sharedHub
            .notify(WillEnterExpandModeEventInfoProvider(), data: ())
    }
    
    private func enterCompactMode() {
        RxEventHub
            .sharedHub
            .notify(WillEnterCompactModeEventInfoProvider(), data: ())
    }
}

extension IMSearchPeopleViewController: Rxable {}

extension IMSearchPeopleViewController: Instantiatable {
    
    static func storyboardID() -> String {
        return "IMSearchPeopleViewController"
    }
    
    static func storyboardName() -> String {
        return "IMSearchPeople"
    }
}
