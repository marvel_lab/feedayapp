//
//  IMSearchPeopleCell.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/11/8.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import InstagramKit
import RxSwift
import SDWebImage
import FeedayCommonKit

class IMSearchPeopleCell: UITableViewCell, Rxable {
    
    var user: InstagramUser?
    
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var placeholderView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    func updateUI() {
        avatarView.layer.masksToBounds = true
        avatarView.layer.cornerRadius = 25
        
        placeholderView.layer.masksToBounds = true
        placeholderView.layer.cornerRadius = 25
        
        guard let user = user else {
            avatarView.image = nil
            nameLabel.text = ""
            return
        }
        
        nameLabel.text = user.username
        
        if let url = user.profilePictureURL {
            avatarView.sd_setImageWithURL(url, placeholderImage: nil, options: [.RetryFailed, .HighPriority])
            /*
            SDWebImageManager
                .sharedManager()
                .rx_loadImage(url, options: [.RetryFailed, .HighPriority], timeout: 5, retry: 3)
                .observeOn(MainScheduler.instance)
                .subscribeNext { [weak self] (image) in
                    self?.avatarView.image = image
                }
                .addDisposableTo(disposeBag)
             */
        }
    }
    
    
}
