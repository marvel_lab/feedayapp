//
//  IMImageCollectionViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/10/30.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import RxSwift
import MJRefresh
import FeedayCommonKit
import FeedayUIKit

class IMImageCollectionViewController: UICollectionViewController, Rxable {
    
    var viewModel = IMImageViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        observeMedias()
        observePresentStyle()
        viewModel.loadMedias(true)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        setupRefresh()
    }
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let number = viewModel.medias.count
        return number
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let reuseID = CollectionCellID.Image.rawValue
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(
            reuseID,
            forIndexPath: indexPath
            ) as! IMImageCell
        
        if let media = viewModel.getMedia(forIndex: indexPath.item) {
            cell.media = media
        }
        
        cell.updateUI()
        
        return cell
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        collectionView.deselectItemAtIndexPath(indexPath, animated: true)
        
        guard let media = viewModel.getMedia(forIndex: indexPath.item) else {
            return
        }
        
        viewModel.sendMessage(withInstagramMedia: media)
    }
    
    private func setupRefresh() {
        let labelColor = UIColor.init(hexString: "#858E99")
        
        let header = MJRefreshNormalHeader.init { [weak self] in
            if self?.viewModel.rx_loading.value == true {
                self?.collectionView?.mj_header?.endRefreshing()
                return
            }
            
            self?.viewModel.loadMedias(true)
        }
        
        header.lastUpdatedTimeText = {  (date) -> String in
            guard let date = date else {
                return "Last Update: Unknown"
            }
            
            guard let calendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian) else {
                return "Last Update: Unknown"
            }
            let unitFlags: NSCalendarUnit = [NSCalendarUnit.Year, NSCalendarUnit.Month, NSCalendarUnit.Day, NSCalendarUnit.Hour, NSCalendarUnit.Minute]
            let components1 = calendar.components(unitFlags, fromDate: date)
            let components2 = calendar.components(unitFlags, fromDate: NSDate())
            
            let formatter = NSDateFormatter()
            if components1.day == components2.day {
                formatter.dateFormat = "HH:mm"
                let dateString = formatter.stringFromDate(date)
                return "Last Update: Today \(dateString)"
            } else if components1.year == components2.year {
                formatter.dateFormat = "MM-dd HH:mm"
                let dateString = formatter.stringFromDate(date)
                return "Last Update: \(dateString)"
            } else {
                formatter.dateFormat = "yyyy-MM-dd HH:mm"
                let dateString = formatter.stringFromDate(date)
                return "Last Update: \(dateString)"
            }
        }
        header.lastUpdatedTimeLabel.textColor = labelColor
        header.stateLabel.textColor = labelColor
        
        collectionView?.mj_header = header
        
        let footer = MJRefreshAutoNormalFooter.init { [weak self] in
            if self?.viewModel.rx_loading.value == true {
                self?.collectionView?.mj_footer?.endRefreshing()
                return
            }
            
            self?.viewModel.loadMedias()
        }
        footer.stateLabel.textColor = labelColor
        collectionView?.mj_footer = footer
        
        updateFooterArea()
        updateFooterHidden()
    }
    
    private func updateFooterHidden() {
        if viewModel.medias.count == 0 {
            collectionView?.mj_footer?.hidden = true
        } else {
            collectionView?.mj_footer?.hidden = false
        }
    }
    
    private func observeMedias() {
        viewModel
            .rx_medias
            .asObservable()
            .observeOn(MainScheduler.instance)
            .doOnError { [weak self] (_) in
                self?.updateFooterHidden()
            }
            .subscribeNext { [weak self] (medias) in
                self?.collectionView?.reloadData()
                self?.updateFooterHidden()
            }
            .addDisposableTo(disposeBag)
        
        viewModel
            .rx_loading
            .asObservable()
            .subscribeNext { [weak self] (loading) in
                if loading == false {
                    self?.collectionView?.mj_header?.endRefreshing()
                    self?.collectionView?.mj_footer?.endRefreshing()
                }
            }
            .addDisposableTo(disposeBag)
    }
    
    private func observePresentStyle() {
        IMGlobalState.sharedState
            .rx_presentStyle
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribeNext { [weak self] (style) in
                self?.updateFooterArea()
            }
            .addDisposableTo(disposeBag)
    }
    
    private func updateFooterArea() {
        guard let footer = collectionView?.mj_footer as? MJRefreshAutoNormalFooter else { return }
        
        if IMGlobalState.sharedState.presentStyle == .Compact {
            footer.triggerAutomaticallyRefreshPercent = 1.2
        } else {
            footer.triggerAutomaticallyRefreshPercent = 2
        }
    }
}
