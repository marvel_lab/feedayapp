//
//  IMImageViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/10/30.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import RxSwift
import MBProgressHUD
import FeedayCommonKit
import FeedayUIKit

class IMImageViewController: UIViewController {
    
    @IBOutlet weak var modeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var emptyHintLabel: UILabel!
    
    var viewModel = IMImageViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        observeLoading()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        modeLabel.text = viewModel.getBackTitle()
        titleLabel.text = viewModel.getTitle()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let vc = segue.destinationViewController as? IMImageCollectionViewController {
            vc.viewModel = viewModel
        }
    }
    
    @IBAction func backButtonPressed() {
        navigationController?.popViewControllerAnimated(true)
    }
    
    private func observeLoading() {
        viewModel
            .rx_loading
            .asObservable()
            .subscribeNext { [weak self] (flag) in
                guard let this = self else { return }
                this.loadingIndicator.hidden = !flag
            }
            .addDisposableTo(disposeBag)
        
        viewModel
            .rx_initLoaded
            .asObservable()
            .subscribe { [weak self] (_) in
                self?.updateEmptyHintLabel()
            }
            .addDisposableTo(disposeBag)
        updateEmptyHintLabel()
    }
    
    private func updateEmptyHintLabel() {
        if viewModel.initLoaded == false {
            emptyHintLabel.hidden = true
            return
        }
        
        emptyHintLabel.hidden = (viewModel.medias.count > 0)
    }
}

extension IMImageViewController: Rxable {}

extension IMImageViewController: Instantiatable {
    
    static func storyboardID() -> String {
        return StoryboardID.ImageViewController.rawValue
    }
    
    static func storyboardName() -> String {
        return StoryboardName.ImageCollection.rawValue
    }
}
