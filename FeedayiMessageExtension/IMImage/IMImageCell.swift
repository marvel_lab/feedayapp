//
//  IMImageCell.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/10/30.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import RxSwift
import InstagramKit
import SDWebImage
import FeedayUIKit
import FeedayCommonKit

class IMImageCell: UICollectionViewCell, Rxable {
    
    @IBOutlet weak var mediaImageView: UIImageView!
    
    var media: InstagramMedia? {
        didSet {
            rx_mediaDidSet.onNext()
        }
    }
    
    private var rx_mediaDidSet = PublishSubject<Void>()
    
    func updateUI() {
        guard let media = media else {
            mediaImageView.image = nil
            return
        }
        
        let url = media.thumbnailURL
        SDWebImageManager
            .sharedManager()
            .rx_loadImage(url, options: [.HighPriority, .ProgressiveDownload], timeout: 5, retry: -1)
            .takeUntil(rx_mediaDidSet)
            .observeOn(MainScheduler.instance)
            .subscribeNext { [weak self] (image) in
                self?.mediaImageView?.image = image
            }
            .addDisposableTo(disposeBag)
    }
    
}
