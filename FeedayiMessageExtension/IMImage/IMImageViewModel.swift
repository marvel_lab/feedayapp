//
//  IMImageViewModel.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/10/30.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import InstagramKit
import RxSwift
import FeedayCommonKit
import FeedayUIKit

class IMImageViewModel: NSObject, Rxable {
    
    var currentMainTab = MainTab.Feed
    
    var currentUser: InstagramUser?
    var currentUserRequest: InstagramUserMediasRequest?
    
    var currentTag: InstagramTag?
    var currentTagRequest: InstagramTagMediasRequest?
    
    var rx_medias = Variable<[InstagramMedia]>([])
    
    var rx_loading = Variable<Bool>(false)
    
    var rx_initLoaded = Variable<Bool>(false)
    var initLoaded: Bool {
        get {
            return rx_initLoaded.value
        }
        set {
            rx_initLoaded.value = newValue
        }
    }
    
    var medias: [InstagramMedia] {
        return rx_medias.value
    }
    
    func getBackTitle() -> String {
        switch currentMainTab {
        case .Feed:
            return "Feed"
        case .Favorite:
            return "Favorites"
        case .Tags:
            return "Tags"
        case .Search:
            return "Search"
        case .Trending:
            return "Trending"
        }
    }
    
    func getTitle() -> String {
        var title = ""
        
        switch currentMainTab {
        case .Feed, .Favorite, .Search, .Trending:
            title = currentUser?.username ?? ""
        case .Tags:
            title = currentTag?.name ?? ""
        }
        
        if title.characters.count > 17 {
            let str = NSString.init(string: title)
            title = str.substringToIndex(14) + "..."
        }
        
        return title
    }
    
    func getMedia(forIndex index: Int) -> InstagramMedia? {
        guard index < medias.count else { return nil }
        
        let media = medias[index]
        return media
    }
    
    func loadMedias(reload: Bool = false) {
        rx_loadMedias(reload).subscribe().addDisposableTo(disposeBag)
    }
    
    func sendMessage(withInstagramMedia media: InstagramMedia) {
        rx_loading.value = true
        IMMessageComposer
            .sharedComposer
            .rx_composeMessage(withInstagramMedia: media)
            .doOn { [weak self] (_) in
                self?.rx_loading.value = false
            }
            .subscribeNext { (message) in
                RxEventHub
                    .sharedHub
                    .notify(WillSendMessageEventInfoProvider(), data: message)
            }
            .addDisposableTo(disposeBag)
    }
    
    func rx_loadMedias(reload: Bool = false) -> Observable<[InstagramMedia]> {
        var signal: Observable<[InstagramMedia]>
        
        switch currentMainTab {
        case .Feed, .Favorite, .Search, .Trending:
            signal = rx_loadPeopleMedias(reload)
        case .Tags:
            signal = rx_loadTagMedias(reload)
        }
        
        if reload == true {
            initLoaded = false
        }
        
        signal = signal
            .map { [weak self] (medias) -> [InstagramMedia] in
                self?.initLoaded = true
                return medias
            }
            .doOnError { [weak self] (_) in
                self?.initLoaded = true
        }
        
        return signal
    }
    
    func rx_loadPeopleMedias(reload: Bool = false) -> Observable<[InstagramMedia]> {
        rx_loading.value = true
        guard let user = currentUser else {
            rx_loading.value = false
            return Observable<[InstagramMedia]>.just([])
        }
        
        if currentUserRequest == nil {
            currentUserRequest = InstagramUserMediasRequest()
            currentUserRequest?.user = user
        }
        
        guard let request = currentUserRequest else {
            rx_loading.value = false
            return Observable<[InstagramMedia]>.just([])
        }
        
        var signal = reload == true ? request.rx_reloadData() : request.rx_loadData()
        
        signal = signal
            .doOn { [weak self] (_) in
                self?.rx_loading.value = false
            }
            .map { [weak self] (medias) -> [InstagramMedia] in
                if let medias = self?.currentUserRequest?.medias {
                    self?.rx_medias.value = medias
                }
                return medias
        }
        
        return signal
    }
    
    func rx_loadTagMedias(reload: Bool = false) -> Observable<[InstagramMedia]> {
        rx_loading.value = true
        guard let tag = currentTag else {
            rx_loading.value = false
            return Observable<[InstagramMedia]>.just([])
        }
        
        if currentTagRequest == nil {
            currentTagRequest = InstagramTagMediasRequest()
            currentTagRequest?.tag = tag
        }
        
        guard let request = currentTagRequest else {
            rx_loading.value = false
            return Observable<[InstagramMedia]>.just([])
        }
        
        var signal = reload == true ? request.rx_reloadData() : request.rx_loadData()
        
        signal = signal
            .doOn { [weak self] (_) in
                self?.rx_loading.value = false
            }
            .map { [weak self] (medias) -> [InstagramMedia] in
                if let medias = self?.currentTagRequest?.medias {
                    self?.rx_medias.value = medias
                }
                return medias
        }
        
        return signal
    }
}
