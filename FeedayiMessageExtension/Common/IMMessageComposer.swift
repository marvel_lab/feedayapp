//
//  IMMessageComposer.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/10/30.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import Messages
import RxSwift
import SDWebImage
import InstagramKit
import FeedayCommonKit

private let kCanvasRect = CGRectMake(0, 0, 375, 210)

class IMMessageComposer: NSObject, Rxable {
    
    static let sharedComposer = IMMessageComposer()
    
    func rx_composeMessage(withInstagramMedia media: InstagramMedia) -> Observable<MSMessage> {
        let signal = Observable<MSMessage>.create { (observer) -> Disposable in
            let url = media.isVideo ? media.thumbnailURL : media.standardResolutionImageURL
            SDWebImageManager
                .sharedManager()
                .rx_loadImage(url, options: .HighPriority, timeout: 5, retry: 3)
                .observeOn(MainScheduler.instance)
                .doOnError { (error) in
                    observer.onError(error)
                }
                .subscribeNext { (image) in
                    self.composeMessage(withInstagramMedia: media, image: image) { message in
                        observer.onNext(message)
                        observer.onCompleted()
                    }
                }
                .addDisposableTo(self.disposeBag)
            
            return NopDisposable.instance
        }
        
        return signal
    }
    
    func composeMessage(withInstagramMedia media: InstagramMedia,
                                           image: UIImage,
                                           onComplete: (message: MSMessage) -> Void) {
        
        let message = MSMessage.init()
        
        let layout = MSMessageTemplateLayout.init()
        
        generateWiderImage(originalImage: image) { image in
            
            layout.image = image
            layout.caption = media.user.fullName
            layout.subcaption = media.user.username
            
            let numberFormatter = NSNumberFormatter.init()
            numberFormatter.numberStyle = .DecimalStyle
            numberFormatter.decimalSeparator = ","
            
            switch media.likesCount {
            case 0:
                layout.trailingCaption = "no like"
            case 1:
                layout.trailingCaption = "1 like"
            default:
                layout.trailingCaption = (numberFormatter.stringFromNumber(NSNumber.init(long: media.likesCount)) ?? "0") + " likes"
            }
            
            switch media.commentsCount {
            case 0:
                layout.trailingSubcaption = "no comment"
            case 1:
                layout.trailingSubcaption = "1 comment"
            default:
                layout.trailingSubcaption = (numberFormatter.stringFromNumber(NSNumber.init(long: media.commentsCount)) ?? "0") + " comments"
            }
            
            message.layout = layout
            
            message.URL = NSURL.init(string: media.link)
            
            onComplete(message: message)
        }
    }
    
    func generateWiderImage(originalImage image: UIImage, onComplete: (image: UIImage) -> Void) {
        dispatch_async(dispatch_get_main_queue()) {
            
            let size = image.size
            
            if size.width == 0 || size.height == 0 {
                onComplete(image: UIImage())
                return
            }
            
            let ratio = size.height / size.width
            
            let newWidth = kCanvasRect.width
            let newHeight = kCanvasRect.width * ratio

            UIGraphicsBeginImageContext(kCanvasRect.size)
            image.drawInRect(CGRectMake(0, 0, newWidth, newHeight))
            let rendered = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            onComplete(image: rendered ?? image)
        }
    }
}
