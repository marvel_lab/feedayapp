//
//  IMGlobalState.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/11/7.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import Foundation
import Messages
import RxSwift
import Alamofire

class IMGlobalState {
    
    static let sharedState = IMGlobalState()
    
    var reachabilityStatus: NetworkReachabilityManager.NetworkReachabilityStatus {
        get {
            return rx_reachabilityStatus.value
        }
        set {
            rx_reachabilityStatus.value = newValue
        }
    }
    
    var rx_reachabilityStatus: Variable<NetworkReachabilityManager.NetworkReachabilityStatus> = Variable(.Unknown)
    
    var reachable: Bool {
        get {
            return rx_reachable.value
        }
        set {
            rx_reachable.value = newValue
        }
    }
    
    var rx_reachable = Variable<Bool>(false)
    
    var presentStyle: MSMessagesAppPresentationStyle {
        get {
            return rx_presentStyle.value
        }
        set {
            rx_presentStyle.value = newValue
        }
    }
    
    var rx_presentStyle = Variable<MSMessagesAppPresentationStyle>(MSMessagesAppPresentationStyle.Compact)
    
    private var reachabilityManager = NetworkReachabilityManager.init(host: "www.instagram.com")
    
    init() {
        initReachability()
    }
    
    func initReachability() {
        reachabilityManager?.listener = { [weak self] status in
            self?.reachabilityStatus = status
            self?.reachable = self?.reachabilityManager?.isReachable ?? false
        }
        
        reachabilityManager?.startListening()
    }
}
