//
//  IMConstants.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/10/28.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import Messages
import FeedayCommonKit

enum StoryboardName: String {
    case MainInterface = "MainInterface"
    case ImageCollection = "ImageCollection"
    case PeopleCollection = "PeopleCollection"
    case TagCollection = "TagCollection"
}

enum StoryboardID: String {
    case MainViewController = "IMMainViewController"
    case PeopleCollectionViewController = "IMPeopleCollectionViewController"
    case ImageViewController = "IMImageViewController"
    case NavigationController = "IMNavigationController"
}

enum MainTab: Int {
    case Feed = 0
    case Favorite
    case Tags
    case Search
    case Trending
}

enum NoLoginTab: Int {
    case Trending = 0
    case ConnectInstagram
}

enum ImageAsset: String {
    case AvatarPlaceholder = "Avatar-Placeholder"
    
    func image() -> UIImage? {
        return UIImage.init(named: rawValue)
    }
}

enum CollectionCellID: String {
    case People = "IMPeopleCollectionViewCell"
    case Image = "IMImageCell"
}

class WillSendMessageEventInfoProvider: RxEventProvider<MSMessage> {}
class WillEnterExpandModeEventInfoProvider: RxEventProvider<Void> {}
class WillEnterCompactModeEventInfoProvider: RxEventProvider<Void> {}
class WillChangeMainTabEventInfoProvider: RxEventProvider<MainTab> {}
class WillOpenMainAppEventInfoProvider: RxEventProvider<NSURL?> {}
class WillDismissExtensionEventInfoProvider: RxEventProvider<Void> {}
