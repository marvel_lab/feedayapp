//
//  IMOpenMessageViewModel.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/11/22.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import InstagramKit
import RxSwift
import FeedayUIKit
import FeedayCommonKit

class IMOpenMessageViewModel: NSObject {
    
    var mediaURL: NSURL?
    
    var shortCode = ""
    
    var media: InstagramMedia? {
        get {
            return rx_media.value
        }
        set {
            rx_media.value = newValue
        }
    }
    
    var rx_media = Variable<InstagramMedia?>(nil)
    
    var zoomImage: UIImage?
    var rx_startZooming = PublishSubject<Void>()
    var rx_stopZooming = PublishSubject<Void>()
    
    func validate() -> Bool {
        guard ShareDefaultsManager.sharedManager.isLoggedIn == true else {
            return false
        }
        
        guard let url = mediaURL else {
            return false
        }
        
        guard let urlString = url.absoluteString else {
            return false
        }
        
        do {
            let re = try NSRegularExpression.init(
                pattern: "^https://www.instagram.com/p/",
                options: NSRegularExpressionOptions.CaseInsensitive)
            let matchNum = re
                .numberOfMatchesInString(
                    urlString,
                    options: NSMatchingOptions.ReportCompletion,
                    range: NSRange.init(location: 0, length: urlString.characters.count)
            )
            
            guard matchNum >= 1 else { return false }
            
            guard let shortCode = url
                .lastPathComponent?
                .stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()) else {
                    return false
            }
            
            self.shortCode = shortCode
            return true
        } catch {
            return false
        }
    }
    
    func rx_loadData() -> Observable<InstagramMedia> {
        let signal = Observable<InstagramMedia>.create { (observer) -> Disposable in
            let engine = InstagramEngine.sharedEngine()
            engine.accessToken = ShareDefaultsManager.sharedManager.accessToken
            engine.getMedia(
                "shortcode/\(self.shortCode)",
                withSuccess: { (media) in
                    self.media = media
                    observer.onNext(media)
                    observer.onCompleted()
                },
                failure: { (error, code) in
                    observer.onError(error)
            })
            
            return NopDisposable.instance
        }
        
        return signal
    }
    
    func openMedia() {
        guard let mediaID = media?.Id where mediaID.isEmpty == false else { return }
        let url = NSURL(string: "feedayios://media?id=\(mediaID)&short_code=\(shortCode)")
        RxEventHub.sharedHub.notify(WillOpenMainAppEventInfoProvider(), data: url)
    }
}
