//
//  IMZoomImageViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/11/26.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import RxSwift
import FeedayUIKit
import FeedayCommonKit

class IMZoomImageViewController: UIViewController {

    @IBOutlet weak var imageScrollView: ImageScrollView!
    
    var viewModel = IMOpenMessageViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture = UITapGestureRecognizer()
        tapGesture
            .rx_event
            .subscribeNext { [weak self] (_) in
                self?.viewModel.rx_stopZooming.onNext()
            }
            .addDisposableTo(disposeBag)
        imageScrollView.addGestureRecognizer(tapGesture)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        guard let image = viewModel.zoomImage else { return }
        imageScrollView.displayImage(image)
    }
}

extension IMZoomImageViewController: Instantiatable {
    
    static func storyboardID() -> String {
        return "IMZoomImageViewController"
    }
    
    static func storyboardName() -> String {
        return "IMOpenMessage"
    }
}

extension IMZoomImageViewController: Rxable {}
