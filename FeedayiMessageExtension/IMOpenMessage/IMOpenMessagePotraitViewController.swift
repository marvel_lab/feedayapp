//
//  IMOpenMessagePotraitViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/11/22.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import InstagramKit
import RxSwift
import SDWebImage
import MBProgressHUD
import FeedayUIKit
import FeedayCommonKit

class IMOpenMessagePotraitViewController: UIViewController, IMOpenMessageViewControllerProtocol {
    
    var viewModel = IMOpenMessageViewModel()
    
    @IBOutlet weak var usernameLabel:      UILabel!
    @IBOutlet weak var realnameLabel:      UILabel!
    @IBOutlet weak var likesCountLabel:    UILabel!
    @IBOutlet weak var commentsCountLabel: UILabel!
    
    @IBOutlet weak var mainImageView:   UIImageView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var openInstagramButton: UIButton!
    
    static func storyboardName() -> String {
        return "IMOpenMessage"
    }
    
    static func storyboardID() -> String {
        return "IMOpenMessagePotraitViewController"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        updateUI()
    }
    
    @IBAction func openInInstagramButtonPressed() {
        viewModel.openMedia()
    }
    
    @IBAction func openZoomingImageButtonPressed() {
        guard let image = mainImageView.image else { return }
        ext_openZoomingImage(image)
    }

    func updateUI() {
        // Avatar image view
        ext_updateAvatarImageView(avatarImageView, media: viewModel.media)
        
        
        // Main image view
        ext_updatePostImageView(mainImageView, media: viewModel.media)
                
        // Username & Realname
        ext_updateUsernameLabel(usernameLabel, media: viewModel.media)
        ext_updateRealnameLabel(realnameLabel, media: viewModel.media)
        
        // Likes & Comments count
        ext_updateLikesCountLabel(likesCountLabel, media: viewModel.media)
        ext_updateCommentsCountLabel(commentsCountLabel, media: viewModel.media)
    }
}
