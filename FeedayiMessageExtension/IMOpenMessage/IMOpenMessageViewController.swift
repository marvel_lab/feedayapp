//
//  IMOpenMessageViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/11/16.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import InstagramKit
import RxSwift
import SDWebImage
import MBProgressHUD
import FeedayUIKit
import FeedayCommonKit

protocol IMOpenMessageViewControllerProtocol: Rxable, Instantiatable {
    
    var viewModel: IMOpenMessageViewModel { get set }
    
    func ext_updateAvatarImageView(imageView: UIImageView, media: InstagramMedia?)
    
    func ext_updatePostImageView(imageView: UIImageView, media: InstagramMedia?)
    
    func ext_updateRealnameLabel(label: UILabel, media: InstagramMedia?)
    
    func ext_updateUsernameLabel(label: UILabel, media: InstagramMedia?)
}

extension IMOpenMessageViewControllerProtocol where Self: UIViewController {

    func ext_updateAvatarImageView(imageView: UIImageView, media: InstagramMedia?) {
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = imageView.bounds.width / 2
        imageView.layer.borderWidth = 0.5
        imageView.layer.borderColor = UIColor(hexString: "#cecece")?.CGColor
        
        guard let media = media else { return }
        
        if let profileURL = media.user.profilePictureURL {
            imageView.sd_setImageWithURL(profileURL, placeholderImage: nil, options: [.RetryFailed, .ProgressiveDownload])
        } else {
            imageView.image = nil
        }
    }
    
    func ext_updatePostImageView(imageView: UIImageView, media: InstagramMedia?) {
        imageView.image = nil
        
        guard let media = media else { return }
        
        // If media is video, it doesn't have `standardResolutionImageURL`
        let url = media.isVideo == true ? media.thumbnailURL : media.standardResolutionImageURL
        SDWebImageManager
            .sharedManager()
            .rx_loadImage(url, options: [.HighPriority, .ProgressiveDownload, .RetryFailed], timeout: 5, retry: -1)
            .observeOn(MainScheduler.instance)
            .subscribeNext { (image) in
                imageView.image = image
            }
            .addDisposableTo(disposeBag)
    }
    
    func ext_updateRealnameLabel(label: UILabel, media: InstagramMedia?) {
        label.text = media?.user.fullName ?? ""
        if label.text?.isEmpty == true {
            label.text = media?.user.username ?? ""
        }
    }
    
    func ext_updateUsernameLabel(label: UILabel, media: InstagramMedia?) {
        label.text = media?.user.username ?? ""
    }
    
    func ext_updateLikesCountLabel(label: UILabel, media: InstagramMedia?) {
        guard let media = media else { return }
        
        let numberFormatter = NSNumberFormatter.init()
        numberFormatter.numberStyle = .DecimalStyle
        numberFormatter.decimalSeparator = ","
        
        switch media.likesCount {
        case 0:
            label.text = "no like"
        case 1:
            label.text = "1 like"
        default:
            label.text = (numberFormatter.stringFromNumber(NSNumber.init(long: media.likesCount)) ?? "0") + " likes"
        }
    }
    
    func ext_updateCommentsCountLabel(label: UILabel, media: InstagramMedia?) {
        guard let media = media else { return }
        
        let numberFormatter = NSNumberFormatter.init()
        numberFormatter.numberStyle = .DecimalStyle
        numberFormatter.decimalSeparator = ","
        
        switch media.commentsCount {
        case 0:
            label.text = "no comment"
        case 1:
            label.text = "1 comment"
        default:
            label.text = (numberFormatter.stringFromNumber(NSNumber.init(long: media.commentsCount)) ?? "0") + " comments"
        }
    }
    
    func ext_openZoomingImage(image: UIImage) {
        viewModel.zoomImage = image
        viewModel.rx_startZooming.onNext()
    }
}

class IMOpenMessageViewController: UIViewController, Rxable, Instantiatable {
    
    var viewModel = IMOpenMessageViewModel()
    
    @IBOutlet weak var containerView: UIView!
    
    private weak var contentViewController: UIViewController?

    private weak var zoomViewController: IMZoomImageViewController?
    
    static func storyboardName() -> String {
        return "IMOpenMessage"
    }
    
    static func storyboardID() -> String {
        return "IMOpenMessageViewController"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard viewModel.validate() == true else {
            dismissViewControllerAnimated(true, completion: nil)
            return
        }
        loadData()
        
        observeEnterCompactMode()
        observeChangeZooming()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateContentViewController()
        updateZoomViewController()
    }
    
    private func updateZoomViewController() {
        guard let vc = zoomViewController else { return }
        guard let image = viewModel.zoomImage else { return }
        vc.imageScrollView.displayImage(image)
    }
    
    private func updateContentViewController() {
        let isPortrait = (view.bounds.height > view.bounds.width)
        
        if isPortrait {
            if let vc = contentViewController as? IMOpenMessagePotraitViewController {
                vc.updateUI()
                return
            }
            
            contentViewController?.removeFromParentViewController()
            contentViewController?.view?.removeFromSuperview()
            contentViewController = IMOpenMessagePotraitViewController.instantiateViewController()
            if let vc = contentViewController as? IMOpenMessagePotraitViewController {
                vc.viewModel = viewModel
                addChildViewController(vc)
                containerView.addSubview(vc.view)
                vc.view.frame = containerView.bounds
                vc.didMoveToParentViewController(self)
            }
        } else {
            if let vc = contentViewController as? IMOpenMessageLandscapeViewController {
                vc.updateUI()
                return
            }
            
            contentViewController?.removeFromParentViewController()
            contentViewController?.view?.removeFromSuperview()
            contentViewController = IMOpenMessageLandscapeViewController.instantiateViewController()
            if let vc = contentViewController as? IMOpenMessageLandscapeViewController {
                vc.viewModel = viewModel
                addChildViewController(vc)
                containerView.addSubview(vc.view)
                vc.view.frame = containerView.bounds
                vc.didMoveToParentViewController(self)
            }
        }
    }
    
    private func loadData() {
        MBProgressHUD.showHUDAddedTo(view, animated: true)
        viewModel.rx_loadData()
        .observeOn(MainScheduler.instance)
        .doOn { [weak self] (_) in
            if let this = self {
                MBProgressHUD.hideHUDForView(this.view, animated: true)
            }
        }
        .doOnError { (error) in
            FeedayAlertHelper.showAlert(inViewController: self, message: (error as NSError).localizedDescription)
        }
        .subscribeNext { [weak self] (_) in
            self?.updateContentViewController()
        }
        .addDisposableTo(disposeBag)
    }
    
    private func observeEnterCompactMode() {
        IMGlobalState
            .sharedState
            .rx_presentStyle
            .asObservable()
            .skip(1)
            .subscribeNext { (style) in
                if style == .Compact {
                    RxEventHub.sharedHub.notify(WillDismissExtensionEventInfoProvider(), data: ())
                }
            }
            .addDisposableTo(disposeBag)
    }
    
    private func observeChangeZooming() {
        viewModel
            .rx_startZooming
            .asObservable()
            .subscribeNext { [weak self] in
                guard let this = self else { return }
                
                if let vc = this.zoomViewController {
                    vc.view.removeFromSuperview()
                    vc.removeFromParentViewController()
                }
                this.zoomViewController = nil
                
                if let vc = IMZoomImageViewController.instantiateViewController() {
                    vc.viewModel = this.viewModel
                    this.zoomViewController = vc
                    this.addChildViewController(vc)
                    this.view.addSubview(vc.view)
                    vc.view.frame = this.view.bounds
                    vc.didMoveToParentViewController(this)
                }
            }
            .addDisposableTo(disposeBag)
        
        viewModel
            .rx_stopZooming
            .asObservable()
            .subscribeNext { [weak self] (image) in
                guard let this = self else { return }
                
                if let vc = this.zoomViewController {
                    vc.view.removeFromSuperview()
                    vc.removeFromParentViewController()
                }
                this.zoomViewController = nil
            }
            .addDisposableTo(disposeBag)
    }
}
