//
//  IMMainViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/10/28.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import Alamofire
import FeedayUIKit
import FeedayCommonKit

class IMMainViewController: UIViewController {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var feedButton: UIButton!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var tagsButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var trendingButton: UIButton!
    @IBOutlet weak var addPeopleAndTagsButton: UIButton!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var containerGap: NSLayoutConstraint!
    
    @IBOutlet weak var containerWrapView: UIView!
    
    @IBOutlet weak var navigationDivideView: UIView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var navigationViewHeight: NSLayoutConstraint!
    @IBOutlet weak var navigationViewWidth: NSLayoutConstraint!
    
    @IBOutlet weak var noNetworkView: UIView!
    @IBOutlet weak var partialEmptyDataButton: UIButton!
    
    // MARK: - Public Data
    
    var viewModel = IMMainViewModel()
    
    // MARK: - Private Data
    
    private weak var contentViewController: UIViewController?
    
    private var searchPeopleViewCotroller: IMSearchPeopleViewController?
    
    private var feedViewController: IMPeopleCollectionViewController?
    
    private var favoriteViewController: IMPeopleCollectionViewController?
    
    private var trendingViewController: IMPeopleCollectionViewController?
    
    private var tagViewController: IMTagCollectionViewController?
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        initUI()
        observeNetworkStatus()
        observeMainTabChange()
        observePresentStyleChange()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let userHasData = PeopleManager.sharedManager.feedPeopleList.count > 0
            || TagsManager.sharedManager.tagList.count > 0
        
        [feedButton, favoriteButton, tagsButton, searchButton]
            .forEach { (btn) in
                btn.hidden = !userHasData
        }
        
        [trendingButton, addPeopleAndTagsButton]
            .forEach { (btn) in
                btn.hidden = userHasData
        }
        
        if userHasData == false {
            viewModel.currentTab = .Trending
        }
        
        updateUI()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - IBAction
    
    @IBAction func feedButtonPressed() {
        viewModel.currentTab = .Feed
    }
    
    @IBAction func favoriteButtonPressed() {
        viewModel.currentTab = .Favorite
    }
    
    @IBAction func tagButtonPressed() {
        viewModel.currentTab = .Tags
    }
    
    @IBAction func searchButtonPressed() {
        viewModel.currentTab = .Search
    }
    
    @IBAction func trendingButtonPressed() {
        viewModel.currentTab = .Trending
    }
    
    @IBAction func addPeopleAndTagsButtonPressed() {
        RxEventHub
            .sharedHub
            .notify(WillOpenMainAppEventInfoProvider(), data: nil)
    }
    
    @IBAction func partialEmptyDataButtonPressed() {
        var url: NSURL?
        
        switch viewModel.currentTab {
        case .Feed, .Favorite:
            url = NSURL(string: FeedayConstant.LinkType.AddPeople.rawValue)
        case .Tags:
            url = NSURL(string: FeedayConstant.LinkType.AddHashtag.rawValue)
        default:
            break
        }
        
        if let url = url {
            RxEventHub.sharedHub.notify(WillOpenMainAppEventInfoProvider(), data: url)
        }
    }
    
    // MARK: - Private Method
    
    private func tabButtons() -> [UIButton] {
        return [
            feedButton,
            favoriteButton,
            tagsButton,
            searchButton,
            trendingButton,
            addPeopleAndTagsButton
        ]
    }
    
    private func initUI() {
        iconImageView.clipsToBounds = true
        iconImageView.layer.masksToBounds = true
        iconImageView.layer.cornerRadius = 4.6
        
        navigationViewWidth.constant = 331
    }
    
    private func observeNetworkStatus() {
        noNetworkView.alpha = 0
        noNetworkView.hidden = true
        IMGlobalState
            .sharedState
            .rx_reachable
            .asObservable()
            .subscribeNext { [weak self] (reachable) in
                if reachable == false {
                    self?.noNetworkView.alpha = 1
                    self?.noNetworkView.hidden = false
                } else {
                    self?.noNetworkView.alpha = 0
                    self?.noNetworkView.hidden = true
                }
            }
            .addDisposableTo(disposeBag)
    }
    
    private func observeMainTabChange() {
        RxEventHub
            .sharedHub
            .eventObservable(WillChangeMainTabEventInfoProvider())
            .asObservable()
            .subscribeNext { [weak self] (tab) in
                self?.viewModel.currentTab = tab
            }
            .addDisposableTo(disposeBag)
        
        viewModel
            .rx_currentTab
            .asObservable()
            .subscribeNext { [weak self] (_) in
                self?.updateUI()
            }
            .addDisposableTo(disposeBag)
    }
    
    private func observePresentStyleChange() {
        IMGlobalState
            .sharedState
            .rx_presentStyle
            .asObservable()
            .subscribeNext { [weak self] (style) in
                if style == .Compact && self?.viewModel.currentTab == .Search {
                    self?.viewModel.currentTab = .Feed
                }
            }
            .addDisposableTo(disposeBag)
    }
    
    private func updateUI() {
        containerGap.constant = 12
        navigationViewHeight.constant = 38
        navigationDivideView.hidden = false
        navigationView.hidden = false
        partialEmptyDataButton.alpha = 0
        partialEmptyDataButton.userInteractionEnabled = false
        partialEmptyDataButton.titleLabel?.numberOfLines = 0
        partialEmptyDataButton.titleLabel?.textAlignment = .Center
        view.layoutIfNeeded()
        
        if let vc = contentViewController {
            vc.view.removeFromSuperview()
            vc.removeFromParentViewController()
            contentViewController = nil
        }
        
        tabButtons().forEach { (button) in
            button.selected = false
        }
        
        switch viewModel.currentTab {
        case .Feed:
            feedButton.selected = true
            if let vc = loadFeedViewController() {
                embedContentViewController(vc)
                
                if vc.viewModel.getPeopleList().count == 0 {
                    partialEmptyDataButton.setTitle("You have not added any feed.\nAdd now in the app!", forState: .Normal)
                    partialEmptyDataButton.alpha = 1
                    partialEmptyDataButton.userInteractionEnabled = true
                }
            }
        case .Favorite:
            favoriteButton.selected = true
            if let vc = loadFavoriteViewController() {
                embedContentViewController(vc)
                
                if vc.viewModel.getPeopleList().count == 0 {
                    partialEmptyDataButton.setTitle("You have not added any favorites.\nAdd now in the app!", forState: .Normal)
                    partialEmptyDataButton.alpha = 1
                    partialEmptyDataButton.userInteractionEnabled = true
                }
            }
        case .Tags:
            tagsButton.selected = true
            if let vc = loadTagViewController() {
                embedContentViewController(vc)
                
                if vc.viewModel.getTagList().count == 0 {
                    partialEmptyDataButton.setTitle("You have not added any tags.\nAdd now in the app!", forState: .Normal)
                    partialEmptyDataButton.alpha = 1
                    partialEmptyDataButton.userInteractionEnabled = true
                }
            }
        case .Search:
            searchButton.selected = true
            containerGap.constant = 0
            navigationViewHeight.constant = 0
            navigationDivideView.hidden = true
            navigationView.hidden = true
            view.layoutIfNeeded()
            if let vc = loadSearchPeopleViewController() {
                embedContentViewController(vc, wrapView: containerWrapView)
                RxEventHub.sharedHub.notify(WillEnterExpandModeEventInfoProvider(), data: ())
                
            }
        case .Trending:
            trendingButton.selected = true
            if let vc = loadTrendingViewController() {
                embedContentViewController(vc)
            }
            break
        }
    }
    
    private func loadFeedViewController() -> IMPeopleCollectionViewController? {
        if let vc = feedViewController {
            return vc
        }
        
        let vc = IMPeopleCollectionViewController.instantiateViewController()
        vc?.viewModel = viewModel.getFeedPeopleViewModel()
        feedViewController = vc
        return vc
    }
    
    private func loadFavoriteViewController() -> IMPeopleCollectionViewController? {
        if let vc = favoriteViewController {
            return vc
        }
        
        let vc = IMPeopleCollectionViewController.instantiateViewController()
        vc?.viewModel = viewModel.getFavoritePeopleViewModel()
        favoriteViewController = vc
        return vc
    }
    
    private func loadTrendingViewController() -> IMPeopleCollectionViewController? {
        if let vc = trendingViewController {
            return vc
        }
        
        let vc = IMPeopleCollectionViewController.instantiateViewController()
        vc?.viewModel = viewModel.getTrendingPeopleViewModel()
        trendingViewController = vc
        return vc
    }
    
    private func loadTagViewController() -> IMTagCollectionViewController? {
        if let vc = tagViewController {
            return vc
        }
        
        let vc = IMTagCollectionViewController.instantiateViewController()
        vc?.viewModel = viewModel.getTagViewModel()
        tagViewController = vc
        return vc
    }
    
    private func loadSearchPeopleViewController() -> IMSearchPeopleViewController? {
        if let vc = searchPeopleViewCotroller {
            return vc
        }
        
        let vc = IMSearchPeopleViewController.instantiateViewController()
        vc?.viewModel = viewModel.getSearchPeopleViewModel()
        searchPeopleViewCotroller = vc
        return vc
    }
    
    private func embedContentViewController(vc: UIViewController, wrapView: UIView? = nil) {
        let outView = wrapView == nil ? containerView : wrapView!
        addChildViewController(vc)
        outView.addSubview(vc.view)
        vc.view.frame = outView.bounds
        vc.didMoveToParentViewController(self)
        
        contentViewController = vc
    }
}

extension IMMainViewController: Rxable {}

extension IMMainViewController: Instantiatable {
    
    static func storyboardID() -> String {
        return StoryboardID.MainViewController.rawValue
    }
    
    static func storyboardName() -> String {
        return StoryboardName.MainInterface.rawValue
    }
    
    static func storyboardIDForNavigationController() -> String? {
        return StoryboardID.NavigationController.rawValue
    }
}
