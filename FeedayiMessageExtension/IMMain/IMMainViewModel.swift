//
//  IMMainViewModel.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/10/28.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import Foundation
import Messages
import RxSwift

class IMMainViewModel {

    var currentTab: MainTab {
        get {
            return rx_currentTab.value
        }
        set {
            rx_currentTab.value = newValue
        }
    }
    
    var rx_currentTab = Variable<MainTab>(.Feed)
    
    func getFeedPeopleViewModel() -> IMPeopleViewModel {
        let vm = IMPeopleViewModel()
        vm.mode = .Feed
        return vm
    }
    
    func getFavoritePeopleViewModel() -> IMPeopleViewModel {
        let vm = IMPeopleViewModel()
        vm.mode = .Favorite
        return vm
    }
    
    func getTrendingPeopleViewModel() -> IMPeopleViewModel {
        let vm = IMPeopleViewModel()
        vm.mode = .Trending
        return vm
    }
    
    func getTagViewModel() -> IMTagViewModel {
        let vm = IMTagViewModel()
        return vm
    }
    
    func getSearchPeopleViewModel() -> IMSearchPeopleViewModel {
        let vm = IMSearchPeopleViewModel()
        return vm
    }
}
