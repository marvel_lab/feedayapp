//
//  IMPeopleViewModel.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/10/28.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import Foundation
import InstagramKit
import FeedayCommonKit

class IMPeopleViewModel {
    
    enum Mode {
        case Feed
        case Favorite
        case Trending
    }
    
    var mode = Mode.Feed
    
    func getPeopleList() -> [InstagramUser] {
        let mgr = PeopleManager.sharedManager
        
        switch mode {
        case .Feed:
            return mgr.feedPeopleList
        case .Favorite:
            return mgr.favoritePeopleList
        case .Trending:
            return mgr.featuredPeopleList
        }
    }
    
    func getPerson(atIndex index: Int) -> InstagramUser? {
        let peopleList = getPeopleList()
        guard index < peopleList.count else {
            return nil
        }
        
        return peopleList[index]
    }
    
}
