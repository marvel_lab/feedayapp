//
//  IMPeopleCollectionViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/10/28.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import RxSwift
import FeedayUIKit
import FeedayCommonKit

class IMPeopleCollectionViewController: UICollectionViewController {

    var viewModel = IMPeopleViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        PeopleManager.sharedManager.loadFeedPeople()
        PeopleManager.sharedManager.loadFavoritePeople()
        PeopleManager.sharedManager.loadFeaturedPeopleList()
        
        PeopleManager
            .sharedManager
            .peopleListSignal
            .observeOn(MainScheduler.instance)
            .subscribeNext { [weak self] (_) in
                self?.collectionView?.reloadData()
            }
            .addDisposableTo(disposeBag)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        collectionView?.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.getPeopleList().count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(
            CollectionCellID.People.rawValue,
            forIndexPath: indexPath
        ) as! IMPeopleCollectionViewCell
        
        // Configure the cell
        if let user = viewModel.getPerson(atIndex: indexPath.item) {
            cell.user = user
            cell.updateUI()
        }
        
        return cell
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        collectionView.deselectItemAtIndexPath(indexPath, animated: true)
        
        guard let user = viewModel.getPerson(atIndex: indexPath.item) else { return }
        
        if let vc = IMImageViewController.instantiateViewController() {
            vc.viewModel.currentUser = user
            
            switch viewModel.mode {
            case .Feed:
                vc.viewModel.currentMainTab = .Feed
            case .Favorite:
                vc.viewModel.currentMainTab = .Favorite
            case .Trending:
                vc.viewModel.currentMainTab = .Trending
            }
            navigationController?.pushViewController(vc, animated: true)
        }
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}

extension IMPeopleCollectionViewController: Instantiatable {
    static func storyboardID() -> String {
        return StoryboardID.PeopleCollectionViewController.rawValue
    }
    
    static func storyboardName() -> String {
        return StoryboardName.PeopleCollection.rawValue
    }
}

extension IMPeopleCollectionViewController: Rxable {}
