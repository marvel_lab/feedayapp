//
//  IMPeopleCollectionViewCell.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/10/28.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import SDWebImage
import InstagramKit
import FeedayCommonKit

class IMPeopleCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    
    var user: InstagramUser?
    
    override func awakeFromNib() {
        avatarImageView.layer.masksToBounds = true
        avatarImageView.layer.cornerRadius = 35
    }
    
    func updateUI() {
        if let url = user?.profilePictureURL {
            avatarImageView.sd_setImageWithURL(
                url,
                placeholderImage: ImageAsset.AvatarPlaceholder.image(),
                options: [.RetryFailed, .ProgressiveDownload]
            )
        } else {
            avatarImageView.image = ImageAsset.AvatarPlaceholder.image()
        }
        
        if let username = user?.username {
            usernameLabel.text = username
        } else {
            usernameLabel.text = ""
        }
    }
}
