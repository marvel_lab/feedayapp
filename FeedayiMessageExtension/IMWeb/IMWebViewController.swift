//
//  IMWebViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/11/13.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import MBProgressHUD
import RxSwift
import FeedayUIKit
import FeedayCommonKit

class IMWebViewController: UIViewController {
    
    @IBOutlet weak var webview: UIWebView!
    
    var urlString: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        IMGlobalState
            .sharedState
            .rx_presentStyle
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribeNext { [weak self] (_) in
                self?.webview.scrollView.contentInset  = UIEdgeInsetsMake(0, 0, 0, 0)
            }
            .addDisposableTo(disposeBag)
        
        title = urlString
        loadURL()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBarHidden = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.navigationBarHidden = true
    }
    
    private func loadURL() {
        guard let url = NSURL.init(string: urlString) else { return }
        
        webview.loadRequest(NSURLRequest.init(URL: url))
    }
}

extension IMWebViewController: UIWebViewDelegate {
    
    func webViewDidStartLoad(webView: UIWebView) {
        MBProgressHUD.showHUDAddedTo(view, animated: true)
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
        MBProgressHUD.hideHUDForView(view, animated: true)
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        MBProgressHUD.hideHUDForView(view, animated: true)
    }
}

extension IMWebViewController: Rxable {}

extension IMWebViewController: Instantiatable {
    
    static func storyboardID() -> String {
        return "IMWebViewController"
    }
    
    static func storyboardName() -> String {
        return "IMWeb"
    }
}
