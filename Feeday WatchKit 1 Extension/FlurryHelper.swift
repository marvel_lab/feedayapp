//
//  FlurryHelper.swift
//  Feeday
//
//  Created by 陈圣晗 on 15/12/18.
//  Copyright © 2015年 Feeday. All rights reserved.
//

import WatchKit
import Flurry_iOS_SDK

class FlurryHelper: NSObject {
    
    static let sharedInstance = FlurryHelper()
    
    override private init() {
        super.init()
        
        Flurry.startSession("9H2P5ZZ3B8XTZBPQ4ZPQ")
        //Flurry.setDebugLogEnabled(true)
    }
    
    func logWhenAppOpen() {
        FlurryWatch.logWatchEvent("Watch_App_Open")
    }
}
