//
//  InterfaceController.swift
//  Feeday WatchKit 1 Extension
//
//  Created by 陈圣晗 on 15/12/15.
//  Copyright © 2015年 Feeday All rights reserved.
//

import Foundation
import WatchKit

import InstagramKit
import SDWebImage
import RxSwift

import FeedayCommonKit

private let disposeBag = DisposeBag()

// MARK: - Protocol

protocol ImageCollectionProtocol {
    var imageCollectionTable: WKInterfaceTable! {get set}
    func configImageCollectionTable()
}

// MARK: - Extension

extension WKInterfaceTable {
    func ImageCollectionRow_ConfigByRowCount(rowCount: Int) {
        if rowCount <= 0 {
            return
        }
        
        self.setNumberOfRows(rowCount, withRowType: ImageCollectionRow.rowType)
        for index in 0..<rowCount {
            let row = self.rowControllerAtIndex(index) as! ImageCollectionRow
            row.setupPlaceholderForRowIndex(index)
        }
    }
    
    func ImageCollectionRow_SetupByInstagramMedias(instagramMedias: [InstagramMedia]) {
        
        let rowCount = self.numberOfRows
        
        if rowCount <= 0 {
            return
        }
        
        var medias1 = [InstagramMedia]()
        var medias2 = [InstagramMedia]()
        var medias3 = [InstagramMedia]()
        
        for (index, media) in instagramMedias.enumerate() {
            switch(index) {
            case 0...2:
                medias1.append(media)
            case 3...5:
                medias2.append(media)
            case 6...8:
                medias3.append(media)
            default:
                break
            }
        }
        
        let row1 = self.rowControllerAtIndex(0) as! ImageCollectionRow
        row1.loadImagesFromInstagramMedias(medias1)
        
        if rowCount == 1 {
            return
        }
        
        let row2 = self.rowControllerAtIndex(1) as! ImageCollectionRow
        row2.loadImagesFromInstagramMedias(medias2)
        
        if rowCount == 2 {
            return
        }
        
        let row3 = self.rowControllerAtIndex(2) as! ImageCollectionRow
        row3.loadImagesFromInstagramMedias(medias3)
    }
}

// MARK: - View Model

class ImageCollectionRow: NSObject {
    
    static let rowType = "ImageCollectionRow"
    
    @IBOutlet var image1: WKInterfaceImage!
    
    @IBOutlet var image2: WKInterfaceImage!
    
    @IBOutlet var image3: WKInterfaceImage!
    
    @IBOutlet var bottomGap: WKInterfaceGroup!
    
    /**
     Setup Placeholder For Row Index
     
     - parameter index: 0...2
     */
    func setupPlaceholderForRowIndex(index: Int) {
        switch (index) {
        case 0...2:
            let i = index
            
            let img1 = UIImage(named: "Placeholder\(i * 3 + 1)")
            self.image1.setImage(img1)
            
            let img2 = UIImage(named: "Placeholder\(i * 3 + 2)")
            self.image2.setImage(img2)
            
            let img3 = UIImage(named: "Placeholder\(i * 3 + 3)")
            self.image3.setImage(img3)
        default:
            let img = UIImage(named: "Placeholder1")
            self.image1.setImage(img)
            self.image2.setImage(img)
            self.image3.setImage(img)
        }
        
        if (index > 0) {
            self.bottomGap.setHidden(true);
        }
    }
    
    private func loadImage(fromMedia media: InstagramMedia, intoImageInterface imageInterface: WKInterfaceImage) {
        
        InstagramManager.sharedManager.loadCookieData()
        
        // Big Picture For FEED
        if media.thumbnailURL.absoluteString == media.standardResolutionImageURL.absoluteString {
            LargeImageLoader.sharedLoader.loadImage(media.thumbnailURL, complete: { (image) in
                imageInterface.setImage(image)
            })
            
            return
        }
        
        SDWebImageManager
            .sharedManager()
            .downloadImageWithURL(
                media.thumbnailURL,
                options: [.RetryFailed, .ProgressiveDownload],
                progress: nil,
                completed: { (image: UIImage!, error: NSError!, cacheType: SDImageCacheType, flag: Bool, url: NSURL!) -> Void in
                    imageInterface.setImage(image)
            })
    }
    
    private func loadImagesFromInstagramMedias(medias: [InstagramMedia]) {
        if medias.count == 0 {
            return
        }
        
        // media1
        let media1 = medias[0]
        loadImage(fromMedia: media1, intoImageInterface: image1)
        
        // media2
        if medias.count == 1 {
            return
        }
        
        let media2 = medias[1]
        loadImage(fromMedia: media2, intoImageInterface: image2)
        
        // media3
        if medias.count == 2 {
            return
        }
        
        let media3 = medias[2]
        loadImage(fromMedia: media3, intoImageInterface: image3)
    }
}

// MARK: - Interface Controllers

// MARK: - FEED
class InterfaceController: WKInterfaceController, ImageCollectionProtocol {
    
    @IBOutlet var anonymousHint: WKInterfaceGroup!
    
    @IBOutlet var anonymousHintLabel: WKInterfaceLabel!
    
    @IBOutlet var imageCollectionTable: WKInterfaceTable!
    
    var rowCount: Int { return ShareDefaultsManager.sharedManager.isPremium ? 3 : 1 }
    var rowCountPrev : Int = 0
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        print("InterfaceController::awakeWithContext")
        
        if let vcID = self.valueForKey("_viewControllerID") as? NSString {
            print("Page One: \(vcID)")
        }
        
        // This is root controller awake
        FlurryHelper.sharedInstance.logWhenAppOpen()
        FlurryManager.logUserIdIfLoggedIn()
        
        // Configure interface objects here.
        //self.configImageCollectionTable()
        //self.loadImages()
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        print("InterfaceController::willActivate")
        
        self.setupUI()
        self.configImageCollectionTable()
        self.loadImages()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
        print("InterfaceController::didDeactivate")
    }
    
    func loadImages() {
        // load feed
        InstagramManager.sharedManager.loadCookieData()
        if ShareDefaultsManager.sharedManager.isLoggedIn {
            InstagramManager
                .sharedManager
                .rx_fetchFeedMedias()
                .subscribeNext { (medias: [InstagramMedia]) in
                    self.imageCollectionTable.ImageCollectionRow_SetupByInstagramMedias(medias)
                }
                .addDisposableTo(disposeBag)
        }
    }
    
    func setupUI() {
        // check if user login
        if ShareDefaultsManager.sharedManager.isLoggedIn {
            self.anonymousHint.setHidden(true)
            self.imageCollectionTable.setHidden(false)
        } else {
            self.anonymousHint.setHidden(false)
            self.imageCollectionTable.setHidden(true)
        }
    }
    
    func configImageCollectionTable() {
        if (self.rowCount == self.rowCountPrev) {
            return
        }
        self.rowCountPrev = self.rowCount
        self.imageCollectionTable.ImageCollectionRow_ConfigByRowCount(self.rowCount)
    }
}

// MARK: - PEOPLE
class PeopleInterfaceController: WKInterfaceController, ImageCollectionProtocol {
    
    @IBOutlet var anonymousHint: WKInterfaceGroup!
    
    @IBOutlet var anonymousHintLabel: WKInterfaceLabel!
    
    @IBOutlet var imageCollectionTable: WKInterfaceTable!
    
    @IBOutlet var hintGroup: WKInterfaceGroup!
    
    var rowCount: Int { return ShareDefaultsManager.sharedManager.isPremium ? 3 : 1 }
    var rowCountPrev : Int = 0
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        print("PeopleInterfaceController::awakeWithContext")
        
        // Configure interface objects here.
        //self.configImageCollectionTable()
        //self.loadImages()
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        print("PeopleInterfaceController::willActivate")
        
        self.setupUI()
        self.configImageCollectionTable()
        self.loadImages()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
        print("PeopleInterfaceController::didDeactivate")
    }
    
    func loadImages() {
        // load people medias
        if ShareDefaultsManager.sharedManager.isLoggedIn {
            InstagramManager
                .sharedManager
                .rx_fetchPeopelMedias()
                .subscribeNext { (medias: [InstagramMedia]) in
                    self.imageCollectionTable.ImageCollectionRow_SetupByInstagramMedias(medias)
                }
                .addDisposableTo(disposeBag)
        }
    }
    
    func setupUI() {
        
        // hide all at first
        self.anonymousHint.setHidden(true)
        self.hintGroup.setHidden(true)
        self.imageCollectionTable.setHidden(true)
        
        // check if user login
        
        if ShareDefaultsManager.sharedManager.isLoggedIn {
            // check people setting
            if PeopleManager.sharedManager.favoritePeopleList.count > 0 {
                self.imageCollectionTable.setHidden(false)
            } else {
                self.hintGroup.setHidden(false)
            }
        } else {
            self.anonymousHint.setHidden(false)
        }
    }
    
    func configImageCollectionTable() {
        if (self.rowCount == self.rowCountPrev) {
            return
        }
        self.rowCountPrev = self.rowCount
        self.imageCollectionTable.ImageCollectionRow_ConfigByRowCount(self.rowCount)
    }
}

// MARK: - HASHTAG
class HashtagInterfaceController: WKInterfaceController, ImageCollectionProtocol {
    
    @IBOutlet var imageCollectionTable: WKInterfaceTable!
    
    @IBOutlet var anonymousHint: WKInterfaceGroup!
    
    @IBOutlet var anonymousHintLabel: WKInterfaceLabel!
    
    @IBOutlet var hintGroup: WKInterfaceGroup!
    
    var rowCount: Int { return ShareDefaultsManager.sharedManager.isPremium ? 3 : 1 }
    var rowCountPrev : Int = 0
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        print("HashtagInterfaceController::awakeWithContext")
        
        // Configure interface objects here.
        //self.configImageCollectionTable()
        //self.loadImages()
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        print("HashtagInterfaceController::willActivate")
        
        self.setupUI()
        self.configImageCollectionTable()
        self.loadImages()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
        print("HashtagInterfaceController::didDeactivate")
    }
    
    func loadImages() {
        // load hashtag medias
        if ShareDefaultsManager.sharedManager.isLoggedIn {
            InstagramManager
                .sharedManager
                .rx_fetchHashtagsMedias()
                .subscribeNext { (medias: [InstagramMedia]) in
                    self.imageCollectionTable.ImageCollectionRow_SetupByInstagramMedias(medias)
                }
                .addDisposableTo(disposeBag)
        }
    }
    
    func setupUI() {
        // hide all at first
        self.anonymousHint.setHidden(true)
        self.hintGroup.setHidden(true)
        self.imageCollectionTable.setHidden(true)
        
        // check if user login
        if ShareDefaultsManager.sharedManager.isLoggedIn {
            // check people setting
            if TagsManager.sharedManager.tagList.count > 0 {
                self.imageCollectionTable.setHidden(false)
            } else {
                self.hintGroup.setHidden(false)
            }
        } else {
            self.anonymousHint.setHidden(false)
        }
    }
    
    func configImageCollectionTable() {
        if (self.rowCount == self.rowCountPrev) {
            return
        }
        self.rowCountPrev = self.rowCount
        self.imageCollectionTable.ImageCollectionRow_ConfigByRowCount(self.rowCount)
    }
}
