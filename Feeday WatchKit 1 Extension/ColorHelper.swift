//
//  ColorHelper.swift
//  Feeday
//
//  Created by 陈圣晗 on 15/12/18.
//  Copyright © 2015年 Feeday. All rights reserved.
//

import WatchKit

class ColorHelper: NSObject {
    
    static let sharedInstance = ColorHelper()
    
    let backgroundColors = [
        UIColor(hex: 0x141414), UIColor(hex: 0x5A5A5A), UIColor(hex: 0xD7D7D7),
        UIColor(hex: 0x9D9D9D), UIColor(hex: 0xEAEAEA), UIColor(hex: 0x505050),
        UIColor(hex: 0x767676), UIColor(hex: 0xFFFFFF), UIColor(hex: 0xBEBEBE)
    ]
    
    override private init() {
        super.init()
    }
    
    func getBackgroundColorForBoxOrder(boxOrder: Int) -> UIColor {
        switch (boxOrder) {
        case 1...9:
            if let color = self.backgroundColors[boxOrder - 1] {
                return color
            }
            return UIColor.blackColor()
        default:
            return UIColor.blackColor()
        }
    }
}
