//
//  FeedayConstant.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/4/17.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit

public class FeedayConstant: NSObject {
    
    public enum SendPhotoType: Int {
        case Email = 0
        case Message
        case WhatsApp
        case Messenger
    }
    
    public enum LinkType: String {
        case App = "feedayios://app"
        case Home = "feedayios://home"
        case Setting = "feedayios://setting"
        case AddPeople = "feedayios://add_people"
        case AddFavorite = "feedayios://add_favorite"
        case AddHashtag = "feedayios://add_hashtag"
        case InAppPurchase = "feedayios://in_app_purchase"
        case SendPhoto = "feedayios://send_photo"
        case Media = "feedayios://media"
        case Login = "feedayios://login"
        case IMessageTutorial = "feedayios://imessage_tutorial"
        case NotificationCenterTutorial = "feedayios://notification_center_tutorial"
    }
    
    public static let mainRedColor = UIColor(red: 255/255.0, green: 19/255.0, blue: 83/255.0, alpha: 1)
    
    public static func boldFont(size: CGFloat) -> UIFont {
        return UIFont(name: "ProximaNova-Bold", size: size)!
    }
    
    public static func semiBoldFont(size: CGFloat) -> UIFont {
        return UIFont(name: "ProximaNova-Semibold", size: size)!
    }
    
    public static func regularFont(size: CGFloat) -> UIFont {
        return UIFont(name: "ProximaNova-Regular", size: size)!
    }
    
    public static func lightFont(size: CGFloat) -> UIFont {
        return UIFont(name: "ProximaNova-Light", size: size)!
    }
    
    public static func regularSystemFont(size: CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue", size: size)!
    }
}
