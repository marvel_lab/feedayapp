//
//  FeedayTranslationHelper.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/4/28.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit

public extension String {
    public func localize() -> String {
        return FeedayTranslationHelper.localize(self)
    }
}

public extension NSString {
    public func localize() -> String {
        return (self as String).localize()
    }
}

@objc public class FeedayTranslationHelper: NSObject {
    @objc public static func localize(str: String) -> String {
//        let locale = NSLocale.currentLocale()
//        print(locale.displayNameForKey(NSLocaleIdentifier, value: locale.localeIdentifier))
        let localized = NSLocalizedString(str, comment: str)
        return localized
    }
}
