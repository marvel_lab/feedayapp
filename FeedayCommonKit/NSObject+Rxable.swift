import Foundation
import RxSwift

public protocol Rxable {
    var disposeBag: DisposeBag { get }
}

public extension Rxable where Self: NSObject {
    
    var disposeBag: DisposeBag {
        if let bag = objc_getAssociatedObject(self, &key) as? DisposeBag {
            return bag
        }
        
        let bag = DisposeBag()
        objc_setAssociatedObject(self, &key, bag, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        return bag
    }
}

private var key: Void?