//
//  FlurryManager.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/4/22.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit

import Flurry_iOS_SDK

private let kAppKey = "9H2P5ZZ3B8XTZBPQ4ZPQ"

@objc public class FlurryManager: NSObject {
    
    @objc public static let sharedManager = FlurryManager()
    
    @objc public static var debugLog = false
    
    @objc public enum FlurryEvent: Int {
        case AppLaunch = 0
        
        case TapAddPeople
        case TapAddHashtag
        case TapPremiumButton
        case TapShowUsernameOn
        case TapShowUsernameOff
        case TapRateUs
        case TapShare
        case TapLogout
        case TapAsymmetric
        case TapMono
        case TapGrid
        case TapFeedInWidget
        case TapFavoriteInWidget
        case TapTagsInWidget
        
        case AddFeedPeople
        case AddFavoritePeople
        case AddTag
        case OpenIMessageExtension

        case TimingMessageWelcomeVideo

        case TapBuyPremiumbutton
        
        var evenName: String {
            switch self {
            case .AppLaunch:
                return "App_Launch"
            case .TapAddPeople:
                return "Tap_AddPeople"
            case .TapAddHashtag:
                return "Add_Tag_Homepage"
            case .TapPremiumButton:
                return "Tap_PremiumButton"
            case TapShowUsernameOn:
                return "Tap_ShowUsername_On"
            case TapShowUsernameOff:
                return "Tap_ShowUsername_Off"
            case .TapRateUs:
                return "Tap_RateUs"
            case .TapShare:
                return "Tap_Share"
            case .TapLogout:
                return "Tap_Logout"
            case .TapAsymmetric:
                return "Tap_Asymmetric"
            case .TapMono:
                return "Tap_Mono"
            case .TapGrid:
                return "Tap_Grid"
            case .TapFeedInWidget:
                return "Widget_Tap_Feed"
            case .TapFavoriteInWidget:
                return "Widget_Tap_Favorite"
            case .TapTagsInWidget:
                return "Widget_Tap_Tags"
            case .AddFeedPeople:
                return "Add_FeedPeople"
            case .AddFavoritePeople:
                return "Add_FavoritePeople"
            case .AddTag:
                return "Add_Tag"
            case .OpenIMessageExtension:
                return "Open_IMessageExtension"
            case .TimingMessageWelcomeVideo:
                return "Timing_MessageWelcomeVideo"
            case .TapBuyPremiumbutton:
                return "Buy Premium button"
            }
        }
    }
    
    @objc public static func startSession() {
        Flurry.startSession(kAppKey)
        Flurry.setSessionReportsOnCloseEnabled(true)
        Flurry.setSessionReportsOnPauseEnabled(true)
        Flurry.setBackgroundSessionEnabled(true)
        
        Flurry.setDebugLogEnabled(debugLog)
    }
    
    @objc public static func logEvent(event: FlurryEvent) {
        Flurry.logEvent(event.evenName)
    }
    
    @objc public static func logExtEvent(event: FlurryEvent) {
        FlurryWatch.logWatchEvent(event.evenName)
    }

    @objc public static func logUserIdIfLoggedIn() {
        let userId = ShareDefaultsManager
            .sharedManager
            .currentUserId
            .stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        if userId.isEmpty == true {
            return
        }

        Flurry.setUserID(userId)
    }

    @objc public static func logTimeEventStart(event: FlurryEvent) {
        Flurry.logEvent(event.evenName, timed: true)
    }

    @objc public static func logTimeEventStop(event: FlurryEvent) {
        Flurry.endTimedEvent(event.evenName, withParameters: nil)
    }
}
