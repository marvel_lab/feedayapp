import UIKit
import InstagramKit
import RxSwift

public class SearchTagsRequest: NSObject {
    
    public func rx_request(tagName: String) -> Observable<[InstagramTag]> {
        
        if tagName.isEmpty {
            return Observable<[InstagramTag]>.just([])
        }
        
        let signal = Observable<[InstagramTag]>.create { (observer) -> Disposable in
            
            let engine = InstagramEngine.sharedEngine()
            engine.accessToken = ShareDefaultsManager.sharedManager.accessToken
            
            engine.searchTagsWithName(
                tagName,
                withSuccess: { (tags, page) in
                    observer.onNext(tags)
                    observer.onCompleted()
                },
                failure: { (error, code) in
                    observer.onError(error)
            })
            
            return NopDisposable.instance
        }
        
        return signal
    }
}