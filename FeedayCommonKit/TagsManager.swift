//
//  TagsManager.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/10.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import CloudKit

import RxSwift
import InstagramKit

public class TagsManager: NSObject, Rxable {

    public static let sharedManager = TagsManager()
    
    public var rx_intentToDoPremiumThings: Variable<Bool> = Variable(false)
    
    public required override init() {
        super.init()
        loadTags()
    }
    
    public var fileManager: ShareFileManager {
        return ShareFileManager.sharedManager
    }
    
    static private var commonError: NSError {
        return NSError.init(domain: "TagsManager", code: -1, userInfo: [NSLocalizedDescriptionKey: "Data Issue"])
    }
    
    private var commonError: NSError {
        return TagsManager.commonError
    }
    
    // MARK: - Featured Tags
    
    public var rx_featuredTagsList: Variable<[InstagramTag]> = Variable([])
    
    public var featuredTagsList: [InstagramTag] {
        get {
            return self.rx_featuredTagsList.value
        }
        set {
            self.rx_featuredTagsList.value = newValue
        }
    }
    
    public func loadFeaturedTagsList() {
        rx_loadFeaturedTagsList().subscribe().addDisposableTo(disposeBag)
    }
    
    public func loadFeaturedTagsListFromServer() {
        rx_loadFeaturedTagsListFromServer().subscribe().addDisposableTo(disposeBag)
    }
    
    public func loadFeaturedTagsListFromLocal() {
        rx_loadFeaturedTagsListFromLocal().subscribe().addDisposableTo(disposeBag)
    }
    
    public func rx_loadFeaturedTagsList() -> Observable<[InstagramTag]> {
        let sig = Observable
            .of(rx_loadFeaturedTagsListFromLocal(), rx_loadFeaturedTagsListFromServer())
            .concat()
        return sig
    }
    
    public func rx_loadFeaturedTagsListFromServer() -> Observable<[InstagramTag]> {
        let sig = AdminDataManager
            .sharedManager
            .rx_loadFeaturedTagsList()
            .map { (_) -> [InstagramTag] in
                self.featuredTagsList = AdminDataManager.sharedManager.featuredTagsList
                ShareFileManager.sharedManager.saveFeaturedTagsList(self.featuredTagsList)
                return self.featuredTagsList
            }
        return sig
    }
    
    public func rx_loadFeaturedTagsListFromLocal() -> Observable<[InstagramTag]> {
        featuredTagsList = ShareFileManager.sharedManager.loadFeatruedTagsList()
        let sig = Observable<[InstagramTag]>.just(featuredTagsList)
        return sig
    }
    
    // MARK: - Tags
    
    public var tagList = [InstagramTag]()
    
    public var tagListSignal: Observable<[InstagramTag]> {
        return tagListSubject.asObservable()
    }
    
    private var tagListSubject = PublishSubject<[InstagramTag]>()
    
    public func loadTags() {
        tagList = fileManager.loadTagList()
        tagListSubject.onNext(tagList)
        
        if BundleManager.sharedManager.isMainApp == false {
            return
        }
        
        loadTagsFromServer { [weak self] (tags) in
            guard let this = self else { return }
            this.tagListSubject.onNext(this.tagList)
        }
    }
    
    public func addTag(tag: InstagramTag) {
        if tagList.count >= 1 && ShareDefaultsManager.sharedManager.isPremium == false {
            rx_intentToDoPremiumThings.value = true
            return
        }
        
        if !tagList.contains(tag) {
            tagList.append(tag)
            tagListSubject.onNext(tagList)
            fileManager.saveTagList(tagList)
            
            FlurryManager.logEvent(.AddTag)
        }
    }
    
    public func removeTag(tag: InstagramTag) {
        if tagList.contains(tag) {
            tagList.removeAtIndex(tagList.indexOf(tag)!)
            tagListSubject.onNext(tagList)
            fileManager.saveTagList(tagList)
        }
    }
    
    public func moveTag(tag: InstagramTag, toIndex index: Int) {
        if tagList.contains(tag) {
            tagList.removeAtIndex(tagList.indexOf(tag)!)
            tagList.insert(tag, atIndex: index)
            tagListSubject.onNext(tagList)
            fileManager.saveTagList(tagList)
        }
    }
    
    public func isTagCollected(tag: InstagramTag) -> Bool {
        return tagList.contains(tag)
    }
    
    public func loadTagsFromServer(onNext: ([InstagramTag] -> Void)? = nil, onError: (NSError -> Void)? = nil) {
        let db = CloudkitManager.sharedManager.mainPrivateDB
        let recordID = CKRecordID(recordName: "Tags")
        
        db.fetchRecordWithID(recordID) { fetchedRecord, error in
            if let err = error {
                onError?(err)
                return
            }
            
            guard let record = fetchedRecord, data = record["data"] as? NSData else {
                onError?(self.commonError)
                return
            }
            
            guard let list = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? [InstagramTag] else {
                onError?(self.commonError)
                return
            }
            
            for tag in list {
                if self.tagList.contains(tag) == false {
                    self.tagList.append(tag)
                }
            }
            
            self.fileManager.saveTagList(self.tagList)
            
            onNext?(list)
        }
    }

    public func rx_saveTags() -> Observable<Void> {
        ShareFileManager.sharedManager.saveTagList(tagList)
        return rx_saveTagsToServer()
    }
    
    private func saveTagsToServer(onNext: (Void -> Void)? = nil, onError: (NSError -> Void)? = nil) {
        let db = CloudkitManager.sharedManager.mainPrivateDB
        let recordID = CKRecordID(recordName: "Tags")
        
        let data = NSKeyedArchiver.archivedDataWithRootObject(self.tagList)
        
        db.fetchRecordWithID(recordID) { fetchedRecord, error in
            let record: CKRecord
            
            if let r = fetchedRecord {
                record = r
            } else {
                record = CKRecord(recordType: "KVStore", recordID: recordID)
                
            }
            
            record["data"] = data
            db.saveRecord(record) { savedRecord, error in
                if let err = error {
                    onError?(err)
                    return
                }
                
                onNext?()
            }
        }
    }
    
    private func rx_saveTagsToServer() -> Observable<Void> {
        return Observable<Void>.create { [weak self] (observer) -> Disposable in
            
            guard let this = self else {
                observer.onError(TagsManager.commonError)
                return NopDisposable.instance
            }
            
            this.saveTagsToServer(
                { () in
                    observer.onNext()
                    observer.onCompleted()
                },
                onError: { (error) in
                    observer.onError(error)
            })
            
            return NopDisposable.instance
        }
    }
}
