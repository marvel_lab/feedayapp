//
//  InstagramUser+Addition.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/5.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import Foundation
import InstagramKit
import RxSwift

public typealias PhotoInfo = (media: InstagramMedia, image: UIImage, type: FeedayConstant.SendPhotoType)

public extension InstagramUser {
    
    public override var hashValue: Int {
        return Id.hashValue
    }
    
    public var webLink: NSURL? {
        return NSURL.init(string: "http://instagram.com/\(username)")
    }
    
    public var appLink: NSURL? {
        return NSURL.init(string: "instagram://user?username=\(username)")
    }
    
    public override func isEqual(object: AnyObject?) -> Bool {
        guard let obj = object as? InstagramUser else { return false }
        
        return isEqualToUser(obj)
    }
    
    public func isFavorite() -> Bool {
        return PeopleManager.sharedManager.isFavorite(self)
    }
    
    public func isFeed() -> Bool {
        return PeopleManager.sharedManager.isFeed(self)
    }
}

public extension InstagramTag {
    
    public override var hashValue: Int {
        return name.hashValue
    }
    
    public override func isEqual(object: AnyObject?) -> Bool {
        guard let obj = object as? InstagramTag else { return false }
        
        return isEqualToTag(obj)
    }
    
    public func isCollected() -> Bool {
        return TagsManager.sharedManager.isTagCollected(self)
    }
}

public extension InstagramMedia {
    
    public var appLink: NSURL? {
        return NSURL.init(string: "instagram://media?id=\(Id)")
    }
    
    public var sendPhotoTextLong: String {
        return "Here's a photo for you by \(user.fullName ?? user.username) \(link) • via.feedayapp.com"
    }
    
    public var sendPhotoTextShort: String {
        return "Here's a photo for you by \(user.fullName ?? user.username) • via.feedayapp.com"
    }
}

public extension InstagramEngine {
    
    public static func sharedEngineWithToken() -> InstagramEngine {
        let engine = InstagramEngine.sharedEngine()
        engine.accessToken = ShareDefaultsManager.sharedManager.accessToken
        return engine
    }
    
    public func rx_getUserDetails(userID: String) -> Observable<InstagramUser> {
        let sig = Observable<InstagramUser>.create { (observer) -> Disposable in
            self.getUserDetails(
                userID,
                withSuccess: { (user) in
                    observer.onNext(user)
                    observer.onCompleted()
                },
                failure: { (error, code) in
                    observer.onError(error)
            })
            
            return NopDisposable.instance
        }
        
        return sig
    }
}
