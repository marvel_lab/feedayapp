import UIKit
import InstagramKit
import RxSwift

public class SearchPeopleRequest: NSObject {
    
    public func rx_request(username: String) -> Observable<[InstagramUser]> {
        
        if username.isEmpty {
            return Observable<[InstagramUser]>.just([])
        }
        
        let signal = Observable<[InstagramUser]>.create { (observer) -> Disposable in
            
            let engine = InstagramEngine.sharedEngine()
            engine.accessToken = ShareDefaultsManager.sharedManager.accessToken
            
            engine.searchUsersWithString(
                username,
                withSuccess: { (users, page) -> Void in
                    observer.onNext(users)
                    observer.onCompleted()
                },
                failure: {
                    (error, code) -> Void in
                    observer.onError(error)
            })
            
            return NopDisposable.instance
        }
        
        return signal
    }
}
