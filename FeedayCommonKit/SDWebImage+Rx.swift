//
//  SDWebImage+Rx.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/9/5.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import SDWebImage
import RxSwift

extension SDWebImageManager: Rxable {}

public extension SDWebImageManager {
    
    public func rx_loadImage(url: NSURL, options: SDWebImageOptions, timeout: NSTimeInterval, retry: Int) -> Observable<UIImage> {
        guard url.absoluteString?.isEmpty == false else {
            return Observable<UIImage>.error(NSError.init(domain: "SDWebImageManager", code: -1, userInfo: [NSLocalizedDescriptionKey: "SDWebImageManager::Empty Image URL"]))
        }
        
        let sig1 = Observable<UIImage>
            .create { [weak self] (observer) -> Disposable in
                guard let this = self else { return NopDisposable.instance }
                
                let operation = this.downloadImageWithURL(url, options: options, progress: nil, completed: { (image, error, cacheType, complete, url) in
                    if error != nil {
                        observer.onError(error)
                        return
                    }
                    
                    guard let img = image else {
                        observer.onError(NSError.init(domain: "SDWebImageManager", code: -1, userInfo: [NSLocalizedDescriptionKey: "SDWebImageManager::Loading Image Failed"]))
                        return
                    }
                    
                    observer.onNext(img)
                    
                    if complete == true {
                        observer.onCompleted()
                    }
                })
                
                return AnonymousDisposable.init {
                    operation.cancel()
                }
            }
        
        let sig2 = Observable<UIImage>
            .create { [weak self] (observer) -> Disposable in
                guard let this = self else { return NopDisposable.instance }
                
                sig1
                    .timeout(timeout, scheduler: MainScheduler.instance)
                    .subscribe(observer)
                    .addDisposableTo(this.disposeBag)
                
                return NopDisposable.instance
            }
        
        return sig2.retry(retry > 0 ? retry : 100)
    }
    
}
