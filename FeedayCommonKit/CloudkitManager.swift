//
//  CloudkitManager.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/8/7.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import CloudKit

import RxSwift

private let KVStoreRecordType: String = "KVStore"
private let KVStoreRecordDataKey: String = "data"

public class CloudkitManager: NSObject, Rxable {
    
    public static let sharedManager = CloudkitManager()
    
    public var mainContainer: CKContainer {
        return CKContainer.init(identifier: "iCloud.de.fiorearcangelo.Feeday")
    }
    
    public var mainPublicDB: CKDatabase {
        return mainContainer.publicCloudDatabase
    }
    
    public var mainPrivateDB: CKDatabase {
        return mainContainer.privateCloudDatabase
    }
    
    public var commonError: NSError {
        return NSError.init(domain: "CloudkitManager", code: -1, userInfo: [NSLocalizedDescriptionKey: "CloudkitManager: Data Issue"])
    }
}

// MARK: - Store Premium State

public extension CloudkitManager {
    
    private var premiumRecordKey: String {
        return "Premium"
    }
    
    public func loadPremium(onComplete: ((Bool, NSError?) -> Void)? = nil) {
        rx_loadPremium()
            .subscribe { (event) in
                if let error = event.error as? NSError {
                    onComplete?(false, error)
                    return
                }
                
                onComplete?(true, nil)
            }
            .addDisposableTo(disposeBag)
    }
    
    public func savePremium(premium: Bool, onComplete: ((Bool, NSError?) -> Void)? = nil) {
        rx_savePremium(premium)
            .subscribe { (event) in
                if let error = event.error as? NSError {
                    onComplete?(false, error)
                    return
                }
                
                onComplete?(true, nil)
            }
            .addDisposableTo(disposeBag)
    }
    
    public func rx_loadPremium() -> Observable<Bool> {
        return Observable<Bool>.create { (observer) -> Disposable in
            let db = self.mainPrivateDB
            let recordID = CKRecordID(recordName: self.premiumRecordKey)
            
            db.fetchRecordWithID(recordID) { fetchedRecord, error in
                if let record = fetchedRecord,
                    data = record[KVStoreRecordDataKey] as? NSData,
                    flag = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? Bool
                {
                    observer.onNext(flag)
                    observer.onCompleted()
                    return
                }
                
                
                observer.onNext(false)
            }
            
            return NopDisposable.instance
        }
    }
    
    public func rx_savePremium(premium: Bool) -> Observable<Bool> {
        return Observable<Bool>.create { (observer) -> Disposable in
            let db = self.mainPrivateDB
            let recordID = CKRecordID(recordName: self.premiumRecordKey)
            
            let data = NSKeyedArchiver.archivedDataWithRootObject(premium)
            
            db.fetchRecordWithID(recordID) { fetchedRecord, error in
                let record: CKRecord
                
                if let fetchedRecord = fetchedRecord {
                    record = fetchedRecord
                } else {
                    record = CKRecord(recordType: KVStoreRecordType, recordID: recordID)
                    
                }
                
                record[KVStoreRecordDataKey] = data
                db.saveRecord(record) { savedRecord, error in
                    if let err = error {
                        observer.onError(err)
                        return
                    }
                    
                    observer.onNext(true)
                    observer.onCompleted()
                }
            }
            
            return NopDisposable.instance
        }
    }
    
}

// TODO: refactored related code here