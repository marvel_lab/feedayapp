//
//  InstagramManager.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/4/20.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import RxSwift
import InstagramKit

public class InstagramManager: NSObject {
    
    public static let sharedManager = InstagramManager()
    
}

// MARK: - Common

public extension InstagramManager {
    
    public var accessToken: String {
        return ShareDefaultsManager.sharedManager.accessToken
    }
    
    public var engine: InstagramEngine {
        let engine = InstagramEngine.sharedEngine()
        engine.accessToken = self.accessToken
        return engine
    }
    
}

// MARK: - Load media info

public extension InstagramManager {
    
    public func rx_loadMediaDetails(mediaID: String = "") -> Observable<InstagramMedia> {
        return Observable<InstagramMedia>.create { [weak self] (observer) -> Disposable in
            guard let this = self else { return NopDisposable.instance }
            
            this.engine.getMedia(
                mediaID,
                withSuccess: { (media) in
                    observer.onNext(media)
                    observer.onCompleted()
                },
                failure: { (error, code) in
                    observer.onError(error)
            })
            
            return NopDisposable.instance
        }
    }
}

// MARK: - Fetch media

public extension InstagramManager {
    
    public func rx_fetchFeedMedias() -> Observable<[InstagramMedia]> {
        let mgr = ShareDefaultsManager.sharedManager
        var list = [
            mgr.feedPeopleID(forLine: 0),
            mgr.feedPeopleID(forLine: 1),
            mgr.feedPeopleID(forLine: 2)
        ]
        list = list.filter { (str: String) -> Bool in
            return !str.isEmpty
        }
        
        if list.count == 3 {
            let sig1 = self.rx_fetchUserMedias(list[0], keepMediasNumber: 3)
            let sig2 = self.rx_fetchUserMedias(list[1], keepMediasNumber: 3)
            let sig3 = self.rx_fetchUserMedias(list[2], keepMediasNumber: 3)
            
            let sig = Observable.combineLatest(sig1, sig2, sig3, resultSelector: { (m1, m2, m3) -> [InstagramMedia] in
                var medias: [InstagramMedia] = []
                medias.appendContentsOf(m1)
                medias.appendContentsOf(m2)
                medias.appendContentsOf(m3)
                return medias
            })
            return sig
        }
        
        if list.count == 2 {
            var keepNumForLine1 = 6
            if mgr.feedPeopleID(forLine: 2).isEmpty {
                keepNumForLine1 = 3
            }
            
            let sig1 = self.rx_fetchUserMedias(list[0], keepMediasNumber: keepNumForLine1)
            let sig2 = self.rx_fetchUserMedias(list[1], keepMediasNumber: 9 - keepNumForLine1)
            let sig = Observable.combineLatest(sig1, sig2, resultSelector: { (m1, m2) -> [InstagramMedia] in
                var medias: [InstagramMedia] = []
                medias.appendContentsOf(m1)
                medias.appendContentsOf(m2)
                return medias
            })
            return sig
        }
        
        if list.count == 1 {
            let sig = self.rx_fetchUserMedias(list[0], keepMediasNumber: 9)
            return sig
        }
        
        return Observable<[InstagramMedia]>.just([])
    }
    
    public func rx_fetchPeopelMedias() -> Observable<[InstagramMedia]> {
        let mgr = ShareDefaultsManager.sharedManager
        var list = [
            mgr.peopleID(forLine: 0),
            mgr.peopleID(forLine: 1),
            mgr.peopleID(forLine: 2)
        ]
        list = list.filter { (str: String) -> Bool in
            return !str.isEmpty
        }
        
        if list.count == 3 {
            let sig1 = self.rx_fetchUserMedias(list[0], keepMediasNumber: 3)
            let sig2 = self.rx_fetchUserMedias(list[1], keepMediasNumber: 3)
            let sig3 = self.rx_fetchUserMedias(list[2], keepMediasNumber: 3)
            let sig = Observable.combineLatest(sig1, sig2, sig3, resultSelector: { (m1, m2, m3) -> [InstagramMedia] in
                var medias: [InstagramMedia] = []
                medias.appendContentsOf(m1)
                medias.appendContentsOf(m2)
                medias.appendContentsOf(m3)
                return medias
            })
            return sig
        }
        
        if list.count == 2 {
            var keepNumForLine1 = 6
            if mgr.peopleID(forLine: 2).isEmpty {
                keepNumForLine1 = 3
            }
            
            let sig1 = self.rx_fetchUserMedias(list[0], keepMediasNumber: keepNumForLine1)
            let sig2 = self.rx_fetchUserMedias(list[1], keepMediasNumber: 9 - keepNumForLine1)
            let sig = Observable.combineLatest(sig1, sig2, resultSelector: { (m1, m2) ->  [InstagramMedia] in
                var medias: [InstagramMedia] = []
                medias.appendContentsOf(m1)
                medias.appendContentsOf(m2)
                return medias
            })
            return sig
        }
        
        if list.count == 1 {
            let sig = self.rx_fetchUserMedias(list[0], keepMediasNumber: 9)
            return sig
        }
        
        return Observable<[InstagramMedia]>.just([])
    }
    
    public func rx_fetchHashtagsMedias() -> Observable<[InstagramMedia]> {
        let mgr = ShareDefaultsManager.sharedManager
        var list = [
            mgr.hashtag(forLine: 0),
            mgr.hashtag(forLine: 1),
            mgr.hashtag(forLine: 2)
        ]
        list = list.filter { (str: String) -> Bool in
            return !str.isEmpty
        }
        
        if list.count == 3 {
            let sig1 = self.rx_fetchHashtagMedias(list[0], keepMediasNumber: 3)
            let sig2 = self.rx_fetchHashtagMedias(list[1], keepMediasNumber: 3)
            let sig3 = self.rx_fetchHashtagMedias(list[2], keepMediasNumber: 3)
            let sig = Observable.combineLatest(sig1, sig2, sig3, resultSelector: { (m1, m2, m3) -> [InstagramMedia] in
                var medias: [InstagramMedia] = []
                medias.appendContentsOf(m1)
                medias.appendContentsOf(m2)
                medias.appendContentsOf(m3)
                return medias
            })
            return sig
        }
        
        if list.count == 2 {
            var keepNumForLine1 = 6
            if mgr.hashtag(forLine: 2).isEmpty {
                keepNumForLine1 = 3
            }
            
            let sig1 = self.rx_fetchHashtagMedias(list[0], keepMediasNumber: keepNumForLine1)
            let sig2 = self.rx_fetchHashtagMedias(list[1], keepMediasNumber: 9 - keepNumForLine1)
            
            let sig = Observable.combineLatest(sig1, sig2, resultSelector: { (m1, m2) ->  [InstagramMedia] in
                var medias: [InstagramMedia] = []
                medias.appendContentsOf(m1)
                medias.appendContentsOf(m2)
                return medias
            })
            return sig
        }
        
        if list.count == 1 {
            let sig = self.rx_fetchHashtagMedias(list[0], keepMediasNumber: 9)
            return sig
        }
        
        return Observable<[InstagramMedia]>.just([])
    }
    
}

// MARK: - Cookie

public extension InstagramManager {
    
    public func storeCookieData() {
        
        guard let url = NSURL(string: "https://www.instagram.com") else {return}
        
        guard let cookies = NSHTTPCookieStorage.sharedHTTPCookieStorage().cookiesForURL(url) else {return}
        
        let data = NSKeyedArchiver.archivedDataWithRootObject(cookies)
        
        ShareDefaultsManager.sharedManager.sharedCookieData = data
    }
    
    public func loadCookieData() {
        
        guard let data = ShareDefaultsManager.sharedManager.sharedCookieData else {return}
        
        guard let cookies = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? [NSHTTPCookie] else {return}
        
        for cookie in cookies {
            NSHTTPCookieStorage.sharedHTTPCookieStorage().setCookie(cookie)
        }
    }
    
}

// MARK: - Common

public extension InstagramManager {
    
    public func rx_fetchUserMedias(userID: String, keepMediasNumber num: Int = 9) -> Observable<[InstagramMedia]> {
        let sig = Observable<[InstagramMedia]>.create { (observer) -> Disposable in
            self.engine.getMediaForUser(userID, withSuccess:
                { (medias: [InstagramMedia], page: InstagramPaginationInfo) in
                    let medias = num != -1 ? self.keep(num, fromMedias: medias) : medias
                    observer.onNext(medias)
                    observer.onCompleted()
                }, failure:
                { (error: NSError, code: Int) in
                    observer.onError(error)
            })
            
            return NopDisposable.instance
        }
        
        return sig
    }
    
    public func rx_fetchHashtagMedias(hashtag: String, keepMediasNumber num: Int = 9) -> Observable<[InstagramMedia]> {
        let sig = Observable<[InstagramMedia]>.create { (observer) -> Disposable in
            self.engine.getMediaWithTagName(hashtag, withSuccess:
                { (medias: [InstagramMedia], page: InstagramPaginationInfo) in
                    let medias = num != -1 ? self.keep(num, fromMedias: medias) : medias
                    observer.onNext(medias)
                    observer.onCompleted()
                }, failure:
                { (error: NSError, code: Int) in
                    observer.onError(error)
            })
            
            return NopDisposable.instance
        }
        return sig
    }
    
    public func keep(num: Int, fromMedias medias: [InstagramMedia]) -> [InstagramMedia] {
        // sort first
        let medias = medias
            .filter { media -> Bool in
                return media.user != nil && media.thumbnailURL != nil
            }
            .sort { media1, media2 -> Bool in
                return media1.createdDate.compare(media2.createdDate) == .OrderedDescending
        }
        
        var arr: [InstagramMedia] = []
        var i = 0
        while i < num {
            let media = i < medias.count ? medias[i] : InstagramMedia()
            arr.append(media)
            i += 1
        }
        return arr
    }
    
}
