//
//  SDiOSVersion+Readable.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/4/28.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import Foundation
import SDVersion

public extension SDiOSVersion {
    static public func readableDeviceVersion() -> SDiOSReadableDeviceVersion {
        let version         = SDiOSVersion.deviceVersion()
        let readableVersion = SDiOSReadableDeviceVersion(deviceVersion: version)
        return readableVersion
    }

    @objc static public func readableDeviceVersionName() -> String {
        let readableVersion = SDiOSVersion.readableDeviceVersion()
        return readableVersion.rawValue
    }
}

public enum SDiOSReadableDeviceVersion: String {
    case iPhone4       = "iPhone4"
    case iPhone4S      = "iPhone4S"
    case iPhone5       = "iPhone5"
    case iPhone5C      = "iPhone5C"
    case iPhone5S      = "iPhone5S"
    case iPhone6       = "iPhone6"
    case iPhone6Plus   = "iPhone6Plus"
    case iPhone6S      = "iPhone6S"
    case iPhone6SPlus  = "iPhone6SPlus"
    case iPhoneSE      = "iPhoneSE"
    case iPad1         = "iPad1"
    case iPad2         = "iPad2"
    case iPadMini      = "iPadMini"
    case iPad3         = "iPad3"
    case iPad4         = "iPad4"
    case iPadAir       = "iPadAir"
    case iPadMini2     = "iPadMini2"
    case iPadAir2      = "iPadAir2"
    case iPadMini3     = "iPadMini3"
    case iPadMini4     = "iPadMini4"
    case iPadPro       = "iPadPro"
    case iPodTouch1Gen = "iPodTouch1Gen"
    case iPodTouch2Gen = "iPodTouch2Gen"
    case iPodTouch3Gen = "iPodTouch3Gen"
    case iPodTouch4Gen = "iPodTouch4Gen"
    case iPodTouch5Gen = "iPodTouch5Gen"
    case iPodTouch6Gen = "iPodTouch6Gen"
    case Simulator     = "Simulator"

    public init(deviceVersion: DeviceVersion) {
        switch deviceVersion {
            case .iPhone4:
                self = .iPhone4
            case .iPhone4S:
                self = .iPhone4S
            case .iPhone5:
                self = .iPhone5
            case .iPhone5C:
                self = .iPhone5C
            case .iPhone5S:
                self = .iPhone5S
            case .iPhone6:
                self = .iPhone6
            case .iPhone6Plus:
                self = .iPhone6Plus
            case .iPhone6S:
                self = .iPhone6S
            case .iPhone6SPlus:
                self = .iPhone6SPlus
            case .iPhoneSE:
                self = .iPhoneSE
            case .iPad1:
                self = .iPad1
            case .iPad2:
                self = .iPad2
            case .iPadMini:
                self = .iPadMini
            case .iPad3:
                self = .iPad3
            case .iPad4:
                self = .iPad4
            case .iPadAir:
                self = .iPadAir
            case .iPadMini2:
                self = .iPadMini2
            case .iPadAir2:
                self = .iPadAir2
            case .iPadMini3:
                self = .iPadMini3
            case .iPadMini4:
                self = .iPadMini4
            case .iPadPro9Dot7Inch, .iPadPro12Dot9Inch:
                self = .iPadPro
            case .iPodTouch1Gen:
                self = .iPodTouch1Gen
            case .iPodTouch2Gen:
                self = .iPodTouch2Gen
            case .iPodTouch3Gen:
                self = .iPodTouch3Gen
            case .iPodTouch4Gen:
                self = .iPodTouch4Gen
            case .iPodTouch5Gen:
                self = .iPodTouch5Gen
            case .iPodTouch6Gen:
                self = .iPodTouch6Gen
            case .Simulator:
                self = .Simulator

        }
    }
}