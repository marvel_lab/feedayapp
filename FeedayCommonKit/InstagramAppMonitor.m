//
// Created by Shenghan Chen on 15/11/5.
// Copyright (c) 2015 Nils Kasseckert. All rights reserved.
//

#import "InstagramAppMonitor.h"

@interface InstagramAppMonitor ()

@end

static NSString *kKeyForIsInstagramInstalled = @"GROUP_IS_INSTAGRAM_INSTALLED";

@implementation InstagramAppMonitor
{

}

#pragma mark - Public

+ (NSUserDefaults *)getGroupUserDefaults
{
    return [[NSUserDefaults alloc] initWithSuiteName:@"group.Feeday.him"];
}

+ (void)setIsInstagramInstalled:(BOOL)isInstagramInstalled
{
    NSUserDefaults *userDefaults = [self getGroupUserDefaults];
    [userDefaults setBool:isInstagramInstalled forKey:kKeyForIsInstagramInstalled];
    [userDefaults synchronize];
}

+ (BOOL)getIsInstagramInstalled
{
    NSUserDefaults *userDefaults = [self getGroupUserDefaults];
    return [userDefaults boolForKey:kKeyForIsInstagramInstalled];
}

#pragma mark - Life Cycle

#pragma mark - Accessor

#pragma mark - Bindings

#pragma mark - Private

@end