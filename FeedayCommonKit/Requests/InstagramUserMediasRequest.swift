//
//  InstagramUserMediasRequest.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/10/31.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import Foundation
import InstagramKit
import RxSwift

public class InstagramUserMediasRequest: InstagramMediasRequest {
   
    public var user: InstagramUser?
    
    public override func rx_loadData() -> Observable<[InstagramMedia]> {
        guard let user = user else { return emptyMediasObservable() }
        
        let signal = Observable<[InstagramMedia]>.create { (observer) -> Disposable in
            
            self.engine
                .getMediaForUser(
                    user.Id,
                    count: self.pageSize,
                    maxId: self.paginationInfo?.nextMaxId,
                    withSuccess: { (medias, page) in
                        self.appendMedias(medias)
                        self.paginationInfo = page
                        observer.onNext(medias)
                        observer.onCompleted()
                    },
                    failure: { (error, code) in
                        observer.onError(error)
                })
            
            return NopDisposable.instance
        }
        
        return signal
    }
}
