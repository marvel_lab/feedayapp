//
//  InstagramMediasRequest.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/11/3.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import Foundation
import InstagramKit
import RxSwift

public class InstagramMediasRequest: NSObject {
    
    public var engine = InstagramManager.sharedManager.engine
    
    public var pageSize = 20
    
    public var medias = [InstagramMedia]()
    
    public var paginationInfo: InstagramPaginationInfo?
    
    public func rx_loadData() -> Observable<[InstagramMedia]> {
        return emptyMediasObservable()
    }
    
    public func rx_reloadData() -> Observable<[InstagramMedia]> {
        reset()
        let signal = rx_loadData()
        return signal
    }
    
    public func reset() {
        medias.removeAll()
        paginationInfo = nil
    }
    
    internal func emptyMediasObservable() -> Observable<[InstagramMedia]> {
        return Observable<[InstagramMedia]>.just([])
    }
    
    internal func appendMedias(medias: [InstagramMedia]) {
        for media in medias {
            if self.medias.contains(media) == false {
                self.medias.append(media)
            }
        }
    }
}
