//
//  InstagramTagMediasRequest.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/11/3.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import Foundation
import InstagramKit
import RxSwift

public class InstagramTagMediasRequest: InstagramMediasRequest {
    
    public var tag: InstagramTag?
    
    public override func rx_loadData() -> Observable<[InstagramMedia]> {
        guard let tag = tag else { return emptyMediasObservable() }
        
        let signal = Observable<[InstagramMedia]>.create { (observer) -> Disposable in
            
            self.engine
                .getMediaWithTagName(
                    tag.name,
                    count: self.pageSize,
                    maxId: self.paginationInfo?.nextMaxId,
                    withSuccess: { (medias, page) in
                        self.appendMedias(medias)
                        self.paginationInfo = page
                        observer.onNext(medias)
                        observer.onCompleted()
                    },
                    failure: { (error, code) in
                        observer.onError(error)
                })
            
            return NopDisposable.instance
        }
        
        return signal
    }
}
