//
//  PeopleManager.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/9.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import CloudKit

import RxSwift
import InstagramKit

public class PeopleManager: NSObject, Rxable {
    
    public static let sharedManager = PeopleManager()
    
    public required override init() {
        super.init()
        
        loadFeedPeople()
        loadFavoritePeople()
    }
    
    public var fileManager: ShareFileManager {
        return ShareFileManager.sharedManager
    }
    
    public var peopleListSignal: Observable<[InstagramUser]> {
        let sig = Observable.of(feedPeopleListSignal, favoritePeopleListSignal).merge()
        return sig
    }
    
    static private var commonError: NSError {
        return NSError.init(domain: "PeopleManager", code: -1, userInfo: [NSLocalizedDescriptionKey: "Data Issue"])
    }
    
    private var commonError: NSError {
        return PeopleManager.commonError
    }
    
    // MARK: - Featured People
    
    public var rx_featuredPeopleList: Variable<[InstagramUser]> = Variable([])
    
    public var featuredPeopleList: [InstagramUser] {
        get {
            return self.rx_featuredPeopleList.value
        }
        set {
            self.rx_featuredPeopleList.value = newValue
        }
    }
    
    public func loadFeaturedPeopleList() {
        loadFeaturedPeopleListFromLocal()
        
        if BundleManager.sharedManager.isMainApp == false {
            return
        }
        
        loadFeaturedPeopleListFromServer()
    }
    
    public func loadFeaturedPeopleListFromServer() {
        AdminDataManager
            .sharedManager
            .rx_loadFeaturedPeopleList()
            .subscribeNext { (_) in
                self.featuredPeopleList = AdminDataManager.sharedManager.featuredPeopleList
                ShareFileManager.sharedManager.saveFeaturedPeopleList(self.featuredPeopleList)
            }
            .addDisposableTo(disposeBag)
    }
    
    public func loadFeaturedPeopleListFromLocal() {
        self.featuredPeopleList = ShareFileManager.sharedManager.loadFeatruedPeopleList()
    }
    
    // MARK: - Feed
    
    public var gotProfileURLFromPasteboard: Bool {
        get {
            return rx_gotProfileURLFromPasteboard.value
        }
        set {
            rx_gotProfileURLFromPasteboard.value = newValue
        }
    }
    public var rx_gotProfileURLFromPasteboard: Variable<Bool> = Variable(false)
    
    public var feedPeopleList = [InstagramUser]()
    
    public var feedPeopleListSignal: Observable<[InstagramUser]> {
        return feedPeopleListSubject.asObservable()
    }
    
    private var feedPeopleListSubject = PublishSubject<[InstagramUser]>()
    
    public func addFeedPeople(user: InstagramUser) {
        if !feedPeopleList.contains(user) {
            feedPeopleList.append(user)
            feedPeopleListSubject.onNext(feedPeopleList)
            fileManager.saveFeedPeopleList(feedPeopleList)
            
            FlurryManager.logEvent(.AddFeedPeople)
        }
    }
    
    public func removeFeedPeople(user: InstagramUser) {
        if feedPeopleList.contains(user) {
            feedPeopleList.removeAtIndex(feedPeopleList.indexOf(user)!)
            feedPeopleListSubject.onNext(feedPeopleList)
            fileManager.saveFeedPeopleList(feedPeopleList)
        }
    }
    
    public func moveFeedPeople(user: InstagramUser, toIndex index: Int) {
        if feedPeopleList.contains(user) {
            feedPeopleList.removeAtIndex(feedPeopleList.indexOf(user)!)
            feedPeopleList.insert(user, atIndex: index)
            feedPeopleListSubject.onNext(feedPeopleList)
            fileManager.saveFeedPeopleList(feedPeopleList)
        }
    }
    
    public func loadFeedPeople() {
        feedPeopleList = fileManager.loadFeedPeopleList()
        feedPeopleListSubject.onNext(feedPeopleList)
        
        if BundleManager.sharedManager.isMainApp == false {
            return
        }
        
        loadFeedPeopleFromServer(
            { [weak self] users in
                guard let this = self else { return }
                this.feedPeopleListSubject.onNext(this.feedPeopleList)
            },
            onError: { error in
                print(error)
        })
    }
    
    public func rx_loadFeedPeopleFromServer() -> Observable<[InstagramUser]> {
        return Observable<[InstagramUser]>.create { [weak self] (observer) -> Disposable in
            
            guard let this = self else {
                observer.onError(PeopleManager.commonError)
                return NopDisposable.instance
            }
            
            this.loadFeedPeopleFromServer(
                { (users) in
                    observer.onNext(users)
                    observer.onCompleted()
                },
                onError: { (error) in
                    observer.onError(error)
            })
            
            return NopDisposable.instance
        }
    }
    
    public func loadFeedPeopleFromServer(onNext: ([InstagramUser] -> Void)? = nil, onError: (NSError -> Void)? = nil) {
        let db = CloudkitManager.sharedManager.mainPrivateDB
        let recordID = CKRecordID(recordName: "FeedPeople")
        
        db.fetchRecordWithID(recordID) { fetchedRecord, error in
            if let err = error {
                onError?(err)
                return
            }
            
            guard let record = fetchedRecord, data = record["data"] as? NSData else {
                onError?(PeopleManager.commonError)
                return
            }
            
            guard let list = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? [InstagramUser] else {
                onError?(PeopleManager.commonError)
                return
            }
            
            for user in list {
                if self.feedPeopleList.contains(user) == false {
                    self.feedPeopleList.append(user)
                }
            }
            
            self.fileManager.saveFeedPeopleList(self.feedPeopleList)
            
            onNext?(list)
        }
    }
    
    public func rx_saveFeedPeople() -> Observable<Void> {
        ShareFileManager.sharedManager.saveFeedPeopleList(feedPeopleList)
        return rx_saveFeedPeopleToServer()
    }
    
    private func rx_saveFeedPeopleToServer() -> Observable<Void> {
        return Observable<Void>.create { [weak self] (observer) -> Disposable in
            
            guard let this = self else {
                observer.onError(PeopleManager.commonError)
                return NopDisposable.instance
            }
            
            this.saveFeedPeopleToServer(
                { () in
                    observer.onNext()
                    observer.onCompleted()
                },
                onError: { (error) in
                    observer.onError(error)
            })
            
            return NopDisposable.instance
        }
    }
    
    private func saveFeedPeopleToServer(onNext: (Void -> Void)? = nil, onError: (NSError -> Void)? = nil) {
        let db = CloudkitManager.sharedManager.mainPrivateDB
        let recordID = CKRecordID(recordName: "FeedPeople")
        
        let data = NSKeyedArchiver.archivedDataWithRootObject(self.feedPeopleList)
        
        db.fetchRecordWithID(recordID) { fetchedRecord, error in
            let record: CKRecord
            
            if let r = fetchedRecord {
                record = r
            } else {
                record = CKRecord(recordType: "KVStore", recordID: recordID)

            }
            
            record["data"] = data
            db.saveRecord(record) { savedRecord, error in
                if let err = error {
                    onError?(err)
                    return
                }
                
                onNext?()
            }
        }
    }
    
    public func isFeed(user: InstagramUser) -> Bool {
        return feedPeopleList.contains(user)
    }
    
    public func addPeopleFromPasteboard(onComplete: ((Bool) -> ())?) {
        rx_addPeopleFromPasteboard()
            .subscribeNext { (flag) in
                onComplete?(flag)
            }
            .addDisposableTo(disposeBag)
    }
    
    public func rx_addPeopleFromPasteboard() -> Observable<Bool> {
        var shouldPerform = false
        var performUsername = ""
        
        let performCheck = {
            guard let urlString = UIPasteboard.generalPasteboard().string else { return }
            guard let url = NSURL.init(string: urlString) else { return }
            
            print("Got URL: \(url)")
            
            guard let hostname = url.host else { return }
            guard hostname.containsString("instagram.com") else { return }
            
            guard let components = url.pathComponents else { return }
            guard components.count >= 2 else { return }
            
            let username = components[1].stringByTrimmingCharactersInSet(
                NSCharacterSet.whitespaceAndNewlineCharacterSet()
            )
            
            guard username.characters.count > 1 else { return }
            
            // check if people already in feed
            let alreadyInFeed = PeopleManager.sharedManager.feedPeopleList.contains { (user) -> Bool in
                return user.username == username
            }
            if alreadyInFeed {
                print("\(username) is already in Feed")
                return
            }
            shouldPerform = true
            performUsername = username
        }
        
        performCheck()
        guard shouldPerform == true else { return Observable<Bool>.just(false) }
        
        self.gotProfileURLFromPasteboard = true
        
        // search and add people
        let request = SearchPeopleRequest()
        let signal = request
            .rx_request(performUsername)
            .doOnError { (error) in
                let error = error as NSError
                dump(error)
            }
            .map { (users) -> Bool in
                for user in users {
                    if user.username == performUsername {
                        PeopleManager.sharedManager.addFeedPeople(user)
                        print("Got \(user.username)")
                        return true
                    }
                }
                
                return false
            }
            .observeOn(MainScheduler.instance)
        
        return signal
    }
    
    // MARK: - Favorite
    
    public var favoritePeopleList = [InstagramUser]()
    
    public var favoritePeopleListSignal: Observable<[InstagramUser]> {
        return favoritePeopleListSubject.asObservable()
    }
    
    private var favoritePeopleListSubject = PublishSubject<[InstagramUser]>()
    
    public func addFavorite(user: InstagramUser) {
        if !favoritePeopleList.contains(user) {
            favoritePeopleList.append(user)
            favoritePeopleListSubject.onNext(favoritePeopleList)
            fileManager.saveFavoritePeopleList(favoritePeopleList)
            
            FlurryManager.logEvent(.AddFavoritePeople)
        }
    }
    
    public func removeFavorite(user: InstagramUser) {
        if favoritePeopleList.contains(user) {
            favoritePeopleList.removeAtIndex(favoritePeopleList.indexOf(user)!)
            favoritePeopleListSubject.onNext(favoritePeopleList)
            fileManager.saveFavoritePeopleList(favoritePeopleList)
        }
    }
    
    public func moveFavorite(user: InstagramUser, toIndex index: Int) {
        if favoritePeopleList.contains(user) {
            favoritePeopleList.removeAtIndex(favoritePeopleList.indexOf(user)!)
            favoritePeopleList.insert(user, atIndex: index)
            favoritePeopleListSubject.onNext(favoritePeopleList)
            fileManager.saveFavoritePeopleList(favoritePeopleList)
        }
    }
    
    public func loadFavoritePeople() {
        favoritePeopleList = fileManager.loadFavoritePeopleList()
        favoritePeopleListSubject.onNext(favoritePeopleList)
        
        if BundleManager.sharedManager.isMainApp == false {
            return
        }
        
        loadFavoritePeopleFromServer(
            { [weak self] users in
                guard let this = self else { return }
                this.favoritePeopleListSubject.onNext(this.favoritePeopleList)
            },
            onError: { error in
                print(error)
        })
    }
    
    public func loadFavoritePeopleFromServer(onNext: ([InstagramUser] -> Void)? = nil, onError: (NSError -> Void)? = nil) {
        let db = CloudkitManager.sharedManager.mainPrivateDB
        let recordID = CKRecordID(recordName: "FavoritePeople")
        
        db.fetchRecordWithID(recordID) { fetchedRecord, error in
            if let err = error {
                onError?(err)
                return
            }
            
            guard let record = fetchedRecord, data = record["data"] as? NSData else {
                onError?(PeopleManager.commonError)
                return
            }
            
            guard let list = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? [InstagramUser] else {
                onError?(PeopleManager.commonError)
                return
            }
            
            for user in list {
                if self.favoritePeopleList.contains(user) == false {
                    self.favoritePeopleList.append(user)
                }
            }
            
            self.fileManager.saveFavoritePeopleList(self.favoritePeopleList)
            
            onNext?(list)
        }
    }
    
    public func rx_saveFavoritePeople() -> Observable<Void> {
        ShareFileManager.sharedManager.saveFavoritePeopleList(favoritePeopleList)
        return rx_saveFavoritePeopleToServer()
    }
    
    private func saveFavoritePeopleToServer(onNext: (Void -> Void)? = nil, onError: (NSError -> Void)? = nil) {
        let db = CloudkitManager.sharedManager.mainPrivateDB
        let recordID = CKRecordID(recordName: "FavoritePeople")
        
        let data = NSKeyedArchiver.archivedDataWithRootObject(self.favoritePeopleList)
        
        db.fetchRecordWithID(recordID) { fetchedRecord, error in
            let record: CKRecord
            
            if let r = fetchedRecord {
                record = r
            } else {
                record = CKRecord(recordType: "KVStore", recordID: recordID)
                
            }
            
            record["data"] = data
            db.saveRecord(record) { savedRecord, error in
                if let err = error {
                    onError?(err)
                    return
                }
                
                onNext?()
            }
        }
    }
    
    private func rx_saveFavoritePeopleToServer() -> Observable<Void> {
        return Observable<Void>.create { [weak self] (observer) -> Disposable in
            
            guard let this = self else {
                observer.onError(PeopleManager.commonError)
                return NopDisposable.instance
            }
            
            this.saveFavoritePeopleToServer(
                { () in
                    observer.onNext()
                    observer.onCompleted()
                },
                onError: { (error) in
                    observer.onError(error)
            })
            
            return NopDisposable.instance
        }
    }
    
    public func isFavorite(user: InstagramUser) -> Bool {
        return favoritePeopleList.contains(user)
    }
}
