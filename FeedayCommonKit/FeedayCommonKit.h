//
//  FeedayCommonKit.h
//  FeedayCommonKit
//
//  Created by 陈圣晗 on 16/4/14.
//  Copyright © 2016年 Feeday. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for FeedayCommonKit.
FOUNDATION_EXPORT double FeedayCommonKitVersionNumber;

//! Project version string for FeedayCommonKit.
FOUNDATION_EXPORT const unsigned char FeedayCommonKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <FeedayCommonKit/PublicHeader.h>

#import "InstagramAppMonitor.h"