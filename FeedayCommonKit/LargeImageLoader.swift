//
//  LargeImageLoader.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/6/6.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import SDWebImage

public class LargeImageLoader: NSObject {
    
    public static let sharedLoader = LargeImageLoader()
    
    public var targetWidth: CGFloat = 100
    
    private var downloader: SDWebImageDownloader {
        if _downloader == nil {
            _downloader = SDWebImageDownloader.init()
            _downloader?.maxConcurrentDownloads = 1
            _downloader?.shouldDecompressImages = false
        }
        
        return _downloader!
    }
    
    private var _downloader: SDWebImageDownloader?
    
    public func loadImage(url: NSURL?, complete: (image: UIImage?) -> ()) {
        guard let url = url else {return}
        
        let mgr = SDWebImageManager.sharedManager()
        let key = mgr.cacheKeyForURL(url)
        mgr.imageCache.removeImageForKey(key) { [weak self] in
            guard let this = self else {
                LargeImageLoader.callBlockInMainThread {
                    complete(image: nil)
                }
                return
            }
            
            this.downloader.downloadImageWithURL(
                url,
                options: .LowPriority,
                progress: nil
            ) { (image, data, error, success) in
                if image == nil {
                    LargeImageLoader.callBlockInMainThread {
                        complete(image: nil)
                    }
                    return
                }
                
                let smallImage = LargeImageLoader.resizeImage(image, newWidth: this.targetWidth)
                let mgr = SDWebImageManager.sharedManager()
                mgr.saveImageToCache(smallImage, forURL: url)
                
                LargeImageLoader.callBlockInMainThread {
                    complete(image: smallImage)
                }
            }
        }
    }
    
    private static func callBlockInMainThread(block: () -> ()) {
        dispatch_async(dispatch_get_main_queue(), block)
    }
    
    private static func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight))
        image.drawInRect(CGRectMake(0, 0, newWidth, newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}
