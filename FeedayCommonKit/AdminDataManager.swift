//
//  AdminDataManager.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/15.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import InstagramKit
import RxSwift
import CloudKit

public class AdminDataManager: NSObject {
    
    public static let sharedManager = AdminDataManager()
    
    // MARK: People
    
    public var featuredPeopleListVar: Variable<[InstagramUser]> = Variable([])
    public var featuredPeopleList: [InstagramUser] {
        return featuredPeopleListVar.value
    }
    
    public func rx_loadFeaturedPeopleList() -> Observable<Bool> {
        return Observable<Bool>.create { (subscriber) -> Disposable in
            let db = CloudkitManager.sharedManager.mainPublicDB
            let recordID = CKRecordID(recordName: "FeaturedPeople")
            
            db.fetchRecordWithID(recordID) { fetchedRecord, error in
                if let err = error {
                    subscriber.onError(err)
                    return
                }
                
                guard let record = fetchedRecord, data = record["data"] as? NSData else {
                    subscriber.onError(NSError.init(domain: "Admin", code: -1, userInfo: [NSLocalizedDescriptionKey: "Data Issue"]))
                    return
                }
                
                guard let list = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? [InstagramUser] else {
                    subscriber.onError(NSError.init(domain: "Admin", code: -1, userInfo: [NSLocalizedDescriptionKey: "Data Issue"]))
                    return
                }
                
                self.featuredPeopleListVar.value = list
                subscriber.onNext(true)
                subscriber.onCompleted()
            }
            
            return NopDisposable.instance
        }
    }
    
    public func rx_saveFeaturedPeopleList() -> Observable<Bool> {
        return Observable<Bool>.create { (subscriber) -> Disposable in
            let db = CloudkitManager.sharedManager.mainPublicDB
            let recordID = CKRecordID(recordName: "FeaturedPeople")
            
            let data = NSKeyedArchiver.archivedDataWithRootObject(self.featuredPeopleList)
            
            
            db.fetchRecordWithID(recordID) { fetchedRecord, error in
                var record = fetchedRecord
                if record == nil {
                    record = CKRecord(recordType: "KVStore", recordID: recordID)
                }
                
                guard record != nil else {
                    subscriber.onError(NSError.init(domain: "Admin", code: -1, userInfo: [NSLocalizedDescriptionKey: "Data Issue"]))
                    return
                }
                
                record!["data"] = data
                db.saveRecord(record!) { savedRecord, error in
                    if let err = error {
                        subscriber.onError(err)
                        return
                    }
                    
                    subscriber.onNext(true)
                    subscriber.onCompleted()
                }
            }
            
            return NopDisposable.instance
        }
    }
    
    // MARK: Tags
    
    public var featuredTagsListVar: Variable<[InstagramTag]> = Variable([])
    public var featuredTagsList: [InstagramTag] {
        return featuredTagsListVar.value
    }
    
    public func rx_loadFeaturedTagsList() -> Observable<Bool> {
        return Observable<Bool>.create { (subscriber) -> Disposable in
            let db = CloudkitManager.sharedManager.mainPublicDB
            let recordID = CKRecordID(recordName: "FeaturedTags")
            
            db.fetchRecordWithID(recordID) { fetchedRecord, error in
                if let err = error {
                    subscriber.onError(err)
                    return
                }
                
                guard let record = fetchedRecord, data = record["data"] as? NSData else {
                    subscriber.onError(NSError.init(domain: "Admin", code: -1, userInfo: [NSLocalizedDescriptionKey: "Data Issue"]))
                    return
                }
                
                guard let list = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? [InstagramTag] else {
                    subscriber.onError(NSError.init(domain: "Admin", code: -1, userInfo: [NSLocalizedDescriptionKey: "Data Issue"]))
                    return
                }
                
                self.featuredTagsListVar.value = list
                subscriber.onNext(true)
                subscriber.onCompleted()
            }
            
            return NopDisposable.instance
        }
    }
    
    public func rx_saveFeaturedTagsList() -> Observable<Bool> {
        return Observable<Bool>.create { (subscriber) -> Disposable in
            let db = CloudkitManager.sharedManager.mainPublicDB
            let recordID = CKRecordID(recordName: "FeaturedTags")
            
            let data = NSKeyedArchiver.archivedDataWithRootObject(self.featuredTagsList)
            
            
            db.fetchRecordWithID(recordID) { fetchedRecord, error in
                var record = fetchedRecord
                if record == nil {
                    record = CKRecord(recordType: "KVStore", recordID: recordID)
                }
                
                guard record != nil else {
                    subscriber.onError(NSError.init(domain: "Admin", code: -1, userInfo: [NSLocalizedDescriptionKey: "Data Issue"]))
                    return
                }
                
                record!["data"] = data
                db.saveRecord(record!) { savedRecord, error in
                    if let err = error {
                        subscriber.onError(err)
                        return
                    }
                    
                    subscriber.onNext(true)
                    subscriber.onCompleted()
                }
            }
            
            return NopDisposable.instance
        }
    }
}
