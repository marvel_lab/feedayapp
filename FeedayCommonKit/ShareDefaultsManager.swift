//
//  ShareDefaultsManager.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/4/14.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit

import RxSwift
import InstagramKit

public class ShareDefaultsManager: NSObject, Rxable {
    
    public enum WidgetSegment: Int {
        case Feed = 0
        case Favorite = 1
        case Tag = 2
        
        public var segmentName: String {
            switch self {
            case .Feed:
                return "Feed"
            case .Favorite:
                return "People"
            case .Tag:
                return "Hashtag"
            }
        }
    }
    
    public enum BlockType: Int {
        case Mono = 0
        case Asymmetric = 1
        case Grid = 2
        
        static func allRawValues() -> [Int] {
            return [0, 1, 2]
        }
    }
    
    public static let sharedManager = ShareDefaultsManager()
    
    let sharedDefaults = NSUserDefaults(suiteName: "group.Feeday.him")
    
    public var lastTimeClearWidgetsCache: Double {
        get {
            return self.sharedDefaults?.doubleForKey("Last_Time_Open_Widgets") ?? 0
        }
        set {
            self.sharedDefaults?.setDouble(newValue, forKey: "Last_Time_Open_Widgets")
            self.sharedDefaults?.synchronize()
        }
    }
    
    public var initedWidgetVersion: String {
        get {
            return self.sharedDefaults?.stringForKey("Inited_Widget_Version") ?? ""
        }
        set {
            self.sharedDefaults?.setObject(newValue, forKey: "Inited_Widget_Version")
            self.sharedDefaults?.synchronize()
        }
    }
    
    public var blockType: BlockType {
        get {
            var typeInt = sharedDefaults?.integerForKey("Block_Type") ?? 0
            let allRawValues = BlockType.allRawValues()
            if !allRawValues.contains(typeInt) {
                typeInt = 0
                self.blockType = .Mono
            }
            
            if let type = BlockType.init(rawValue: typeInt) {
                return type
            }
            
            return .Mono
        }
        set {
            let typeInt = newValue.rawValue
            sharedDefaults?.setInteger(typeInt, forKey: "Block_Type")
            sharedDefaults?.synchronize()
            blockTypeChangeSignal.onNext(newValue)
        }
    }
    
    public var blockTypeChangeSignal = PublishSubject<BlockType>()
    
   
    
    public var isLoggedIn: Bool {
        get {
            return self.sharedDefaults?.boolForKey("LoggedIn_Want") ?? false
        }
        set {
            self.sharedDefaults?.setBool(newValue, forKey: "LoggedIn_Want")
            self.sharedDefaults?.synchronize()
        }
    }
    
    public var accessToken: String {
        get {
            return self.sharedDefaults?.stringForKey("accessToken") ?? ""
        }
        set {
            self.sharedDefaults?.setObject(newValue, forKey: "accessToken")
            self.sharedDefaults?.synchronize()
        }
    }

    private var _currentUserIdKey = "Current_User_Id"
    public var currentUserId: String {
        get {
            return self.sharedDefaults?.stringForKey(_currentUserIdKey) ?? ""
        }
        set {
            self.sharedDefaults?.setObject(newValue, forKey: _currentUserIdKey)
            self.sharedDefaults?.synchronize()
        }
    }
    
    public var hideName: Bool {
        get {
            return self.sharedDefaults?.boolForKey("Setting_Hidename") ?? false
        }
        set {
            self.sharedDefaults?.setBool(newValue, forKey: "Setting_Hidename")
            self.sharedDefaults?.synchronize()
        }
    }
    
    public var widgetSelectedSegment: WidgetSegment {
        get {
            let int = self.sharedDefaults?.integerForKey("Widget_Segment_Selected") ?? 0
            return WidgetSegment.init(rawValue: int) ?? .Feed
        }
        set {
            self.sharedDefaults?.setInteger(newValue.rawValue, forKey: "Widget_Segment_Selected")
            self.sharedDefaults?.synchronize()
        }
    }
    
    // MARK: - Premium
    
    public var isPremium: Bool {
        get {
            return _isPremium() || _isBlockUnlocked() || _isHidenameUnlocked()
        }
    }
    
    public var rx_premiumUnlocked = PublishSubject<Bool>()
    
    public func unlockPremium(pushToCloud: Bool = true) {
        self.sharedDefaults?.setBool(true, forKey: "IAP_Premium")
        self.sharedDefaults?.synchronize()
        
        rx_premiumUnlocked.onNext(true)
        
        if pushToCloud {
            updatePremiumStateFromCloud()
        }
    }
    
    public func updatePremiumStateFromCloud() {
        // load premium state from cloud
        let signal = CloudkitManager.sharedManager.rx_loadPremium().replay(2)
        
        // load premium state from local
        let premiumInLocal = self.isPremium
        
        // if is not premium on cloud, but premium in local, change cloud state
        signal
            .filter { (premiumOnCloud) -> Bool in
                return premiumOnCloud == false && premiumInLocal == true
            }
            .flatMap { _ in
                return CloudkitManager.sharedManager.rx_savePremium(true)
            }
            .subscribe()
            .addDisposableTo(disposeBag)
        
        // if premium on cloud, not premium in local, change local state
        signal
            .filter { (premiumOnCloud) -> Bool in
                return premiumOnCloud == true && premiumInLocal == false
            }
            .subscribeNext { [weak self] (_) in
                self?.unlockPremium(false)
            }
            .addDisposableTo(disposeBag)
        
        signal.connect().addDisposableTo(disposeBag)
    }
    
    
    // MARK: - Cookies
    
    public var sharedCookieData: NSData? {
        get {
            return self.sharedDefaults?.objectForKey("Shared_Cookies") as? NSData
        }
        set {
            self.sharedDefaults?.setObject(newValue, forKey: "Shared_Cookies")
            self.sharedDefaults?.synchronize()
        }
    }
    
    // MARK: - Feed
    
    public func feedPeopleID(forLine line: Int = 0) -> String {
        let peopleList = PeopleManager.sharedManager.feedPeopleList
        let idList = peopleList.map { (user) -> String in
            return user.Id
        }
        
        guard line < idList.count else {
            return ""
        }
        
        return idList[line]
    }
    
    // MARK: - People
    
    public func peopleID(forLine line: Int = 0) -> String {
        let peopleList = PeopleManager.sharedManager.favoritePeopleList
        let idList = peopleList.map { (user) -> String in
            return user.Id
        }
        
        guard line < idList.count else {
            return ""
        }
        
        return idList[line]
    }
    
    // MARK: - Hashtag
    
    public func hashtag(forLine line: Int = 0) -> String {
        let tagList = TagsManager.sharedManager.tagList
        let nameList = tagList.map { (tag) -> String in
            return tag.name
        }
        
        guard line < tagList.count else { return "" }
        
        return nameList[line]
    }
    
    // MARK: - Send Photo
    
    public func rx_saveSendPhoto(photoInfo: PhotoInfo) -> Observable<Void> {
        return Observable<Void>.create { (observer) -> Disposable in
            self.sharedDefaults?.setObject(
                NSKeyedArchiver.archivedDataWithRootObject(photoInfo.media),
                forKey: "Send_Photo_Media"
            )
            
            if let imageData = UIImagePNGRepresentation(photoInfo.image) {
                self.sharedDefaults?.setObject(imageData, forKey: "Send_Photo_Image")
            }
            
            self.sharedDefaults?.setInteger(photoInfo.type.rawValue, forKey: "Send_Photo_Type")
            
            self.sharedDefaults?.synchronize()
            
            observer.onNext()
            observer.onCompleted()
            
            return NopDisposable.instance
        }
    }
    
    public func rx_loadSendPhoto() -> Observable<PhotoInfo> {
        return Observable<PhotoInfo>.create { (observer) -> Disposable in
            
            var media = InstagramMedia()
            if let data = self.sharedDefaults?.objectForKey("Send_Photo_Media") as? NSData {
                if let m = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? InstagramMedia {
                    media = m
                }
            }
            
            var image = UIImage()
            if let data = self.sharedDefaults?.objectForKey("Send_Photo_Image") as? NSData {
                if let img = UIImage.init(data: data) {
                    image = img
                }
            }
            
            let idx = self.sharedDefaults?.integerForKey("Send_Photo_Type") ?? 0
            let type = FeedayConstant.SendPhotoType(rawValue: idx) ?? FeedayConstant.SendPhotoType.Email
            
            observer.onNext((media: media, image: image, type: type))
            
            return NopDisposable.instance
        }
    }
    
    public func clearSendPhoto() {
        sharedDefaults?.removeObjectForKey("Send_Photo_Image")
        sharedDefaults?.removeObjectForKey("Send_Photo_URL")
        sharedDefaults?.removeObjectForKey("Send_Photo_Type")
        sharedDefaults?.removeObjectForKey("Send_Photo_Text")
        sharedDefaults?.synchronize()
    }
    
    // MARK: - Private
    
    private func _hashtagKey(forLine line: Int = 0) -> String {
        let key1 = "hashtag"
        let key2 = "hashtag2"
        let key3 = "hashtag3"
        
        let match = [
            0 : key1,
            1 : key2,
            2 : key3
        ]
        
        let key = match[line] ?? key1
        return key
    }
    
    private func _peopleIDKey(forLine line: Int = 0) -> String {
        let key1 = "people"
        let key2 = "people2"
        let key3 = "people3"
        
        let match = [
            0 : key1,
            1 : key2,
            2 : key3
        ]
        
        let key = match[line] ?? key1
        return key
    }
    
    private func _peopleNameKey(forLine line: Int = 0) -> String {
        let key1 = "people_name"
        let key2 = "people_name2"
        let key3 = "people_name3"
        
        let match = [
            0 : key1,
            1 : key2,
            2 : key3
        ]
        
        let key = match[line] ?? key1
        return key
    }
    
    private func _isPremium() -> Bool {
        return self.sharedDefaults?.boolForKey("IAP_Premium") ?? false
    }
    
    private func _isBlockUnlocked() -> Bool {
        return self.sharedDefaults?.boolForKey("blocksActivated") ?? false
    }
    
    private func _isHidenameUnlocked() -> Bool {
        return self.sharedDefaults?.boolForKey("IAP_Hidename") ?? false
    }
}
