//
// Created by Shenghan Chen on 15/11/5.
// Copyright (c) 2015 Nils Kasseckert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InstagramAppMonitor : NSObject

+ (nonnull NSUserDefaults *)getGroupUserDefaults;

+ (void)setIsInstagramInstalled:(BOOL)isInstagramInstalled;

+ (BOOL)getIsInstagramInstalled;

@end