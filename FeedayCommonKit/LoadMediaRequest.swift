import UIKit
import InstagramKit
import RxSwift

public class LoadMediaRequest: NSObject {
    
    public func rx_loadOne(forUser user: InstagramUser) -> Observable<InstagramMedia> {
        if user.Id.isEmpty || user.Id == "0" {
            return Observable<InstagramMedia>.just(InstagramMedia())
        }
        
        let signal = Observable<InstagramMedia>.create { (observer) -> Disposable in
            let engine = InstagramEngine.sharedEngine()
            engine.accessToken = ShareDefaultsManager.sharedManager.accessToken
            
            engine.getMediaForUser(
                user.Id,
                withSuccess: { (medias, pageInfo) in
                    observer.onNext(medias.first ?? InstagramMedia())
                    observer.onCompleted()
                },
                failure: { (error, code) in
                    observer.onError(error)
            })
            
            return NopDisposable.instance
        }
        
        return signal
    }
    
    public func rx_loadOne(forTag tag: InstagramTag) -> Observable<InstagramMedia> {
        if tag.name.isEmpty {
            return Observable<InstagramMedia>.just(InstagramMedia())
        }
        
        let signal = Observable<InstagramMedia>.create { (observer) -> Disposable in
            let engine = InstagramEngine.sharedEngine()
            engine.accessToken = ShareDefaultsManager.sharedManager.accessToken
            
            engine.getMediaWithTagName(
                tag.name,
                withSuccess: { (medias, pageInfo) in
                    observer.onNext(medias.first ?? InstagramMedia())
                    observer.onCompleted()
                },
                failure: { (error, code) in
                    observer.onError(error)
            })
            
            return NopDisposable.instance
        }
        
        return signal
    }
    
    public func rx_loadMultiple(forUser user: InstagramUser, num: Int = 9) -> Observable<[InstagramMedia]> {
        if user.Id.isEmpty || user.Id == "0" {
            return Observable<[InstagramMedia]>.just(LoadMediaRequest.fillMedias([], num: num))
        }
        
        let signal = Observable<[InstagramMedia]>.create { (observer) -> Disposable in
            let engine = InstagramEngine.sharedEngine()
            engine.accessToken = ShareDefaultsManager.sharedManager.accessToken
            
            engine.getMediaForUser(
                user.Id,
                withSuccess: { (medias, pageInfo) in
                    observer.onNext(LoadMediaRequest.fillMedias(medias, num: num))
                    observer.onCompleted()
                },
                failure: { (error, code) in
                    observer.onError(error)
            })
            
            return NopDisposable.instance
        }
        
        return signal
    }
    
    public func rx_loadMultiple(forTag tag: InstagramTag, num: Int = 9) -> Observable<[InstagramMedia]> {
         if tag.name.isEmpty {
            return Observable<[InstagramMedia]>.just(LoadMediaRequest.fillMedias([], num: num))
        }
        
        let signal = Observable<[InstagramMedia]>.create { (observer) -> Disposable in
            let engine = InstagramEngine.sharedEngine()
            engine.accessToken = ShareDefaultsManager.sharedManager.accessToken
            
            engine.getMediaWithTagName(
                tag.name,
                withSuccess: { (medias, pageInfo) in
                    let arr = medias.sort({ (obj1, obj2) -> Bool in
                        switch obj1.createdDate.compare(obj2.createdDate) {
                        case .OrderedDescending:
                            return true
                        default:
                            return false
                        }
                    })
                    
                    observer.onNext(LoadMediaRequest.fillMedias(arr, num: num))
                    observer.onCompleted()
                },
                failure: { (error, code) in
                    observer.onError(error)
            })
            
            return NopDisposable.instance
        }
        
        return signal
    }
    
    private static func fillMedias(medias: [InstagramMedia], num: Int = 9) -> [InstagramMedia] {
        var arr: [InstagramMedia] = []
        var count: Int = 0
        for media in medias {
            arr.append(media)
            count += 1
            
            if count == 9 {
                break
            }
        }
        
        while arr.count < 9 {
            arr.append(InstagramMedia())
        }
        
        return arr
    }
}
