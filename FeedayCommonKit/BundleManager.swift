//
//  BundleManager.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/8/7.
//  Copyright © 2016年 Feeday. All rights reserved.
//

public class BundleManager: NSObject {

    public static let sharedManager = BundleManager()
    
    public var isMainApp: Bool {
        return bundleID == "de.fiorearcangelo.Feeday"
    }
    
    public var isTodayExt: Bool {
        return bundleID == "de.fiorearcangelo.Feeday.Feeday-Widget"
    }
    
    public var isWatchExt: Bool {
        return bundleID == "de.fiorearcangelo.Feeday.watchkitextension"
    }
    
    public var bundleID: String {
        return NSBundle.mainBundle().bundleIdentifier ?? ""
    }
    
}
