//
//  ShareFileManager.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/5.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import InstagramKit

public class ShareFileManager: NSObject {
    
    public static let sharedManager = ShareFileManager()
    
    public var applicationDocumentsDirectory: NSURL? {
        return NSFileManager.defaultManager().containerURLForSecurityApplicationGroupIdentifier("group.Feeday.him")
    }
    
    // MARK: - Featured People
    
    public var featuredPeopleListFileURL: NSURL? {
        return applicationDocumentsDirectory?.URLByAppendingPathComponent("Featured-People.plist")
    }
    
    public func loadFeatruedPeopleList() -> [InstagramUser] {
        return loadPeopleList(featuredPeopleListFileURL)
    }
    
    public func saveFeaturedPeopleList(list: [InstagramUser]) -> Bool {
        return savePeopleList(list, url: featuredPeopleListFileURL)
    }
    
    // MARK: - Featured Tags
    
    public var featuredTagsListFileURL: NSURL? {
        return applicationDocumentsDirectory?.URLByAppendingPathComponent("Featured-Tags.plist")
    }
    
    public func loadFeatruedTagsList() -> [InstagramTag] {
        return loadTagList(featuredTagsListFileURL)
    }
    
    public func saveFeaturedTagsList(list: [InstagramTag]) -> Bool {
        return saveTagList(list, url: featuredTagsListFileURL)
    }
    
    // MARK: - Feed
    
    public var feedPeopleListFileURL: NSURL? {
        return applicationDocumentsDirectory?.URLByAppendingPathComponent("Feed-People.plist")
    }
    
    public func loadFeedPeopleList() -> [InstagramUser] {
        return loadPeopleList(feedPeopleListFileURL)
    }

    public func saveFeedPeopleList(list: [InstagramUser]) -> Bool {
        return savePeopleList(list, url: feedPeopleListFileURL)
    }
    
    // MARK: - Favorite
    
    public var favoritePeopleListFileURL: NSURL? {
        return applicationDocumentsDirectory?.URLByAppendingPathComponent("Favorite-People.plist")
    }
    
    public func loadFavoritePeopleList() -> [InstagramUser] {
        return loadPeopleList(favoritePeopleListFileURL)
    }
    
    public func saveFavoritePeopleList(list: [InstagramUser]) -> Bool {
        return savePeopleList(list, url: favoritePeopleListFileURL)
    }
    
    // MARK: - Tags
    
    public var tagListFileURL: NSURL? {
        return applicationDocumentsDirectory?.URLByAppendingPathComponent("Tags.plist")
    }
    
    public func loadTagList(url: NSURL? = nil) -> [InstagramTag] {
        if let url = url ?? tagListFileURL {
            if let data = NSData(contentsOfURL: url) {
                if let list = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? [InstagramTag] {
                    return list
                }
            }
        }
        
        return []
    }
    
    public func saveTagList(list: [InstagramTag], url: NSURL? = nil) -> Bool {
        let data = NSKeyedArchiver.archivedDataWithRootObject(list)
        if let url = url ?? tagListFileURL {
            let flag = data.writeToURL(url, atomically: true)
            return flag
        }
        
        return false
    }
    
    // MARK: - People
    
    private func loadPeopleList(url: NSURL?) -> [InstagramUser] {
        if let url = url {
            if let data = NSData(contentsOfURL: url) {
                if let list = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? [InstagramUser] {
                    return list
                }
            }
        }
        
        return []
    }
    
    private func savePeopleList(list: [InstagramUser], url: NSURL?) -> Bool {
        let data = NSKeyedArchiver.archivedDataWithRootObject(list)
        if let url = url {
            let flag = data.writeToURL(url, atomically: true)
            return flag
        }
        
        return false
    }
}
