//
//  MonoViewModel.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/30.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import Foundation

import RxSwift
import InstagramKit
import SDWebImage

import FeedayCommonKit

class MonoViewModel: NSObject, Rxable {
    
    var widgetsViewModel = WidgetsViewModel()
    
    var currentMedia: InstagramMedia?
    
    var imagePrefether = SDWebImagePrefetcher.init(imageManager: SDWebImageManager.sharedManager())
    
    private let feedLoader: FeedLoader
    private let favoriteLoader: FavoriteLoader
    private let tagLoader: TagLoader
    
    private var currentLoader: DataLoader {
        switch widgetsViewModel.segmentState {
        case .Feed:
            return feedLoader
        case .Favorites:
            return favoriteLoader
        case .Tags:
            return tagLoader
        }
    }
    
    override init() {
        imagePrefether.options = [.RetryFailed, .ProgressiveDownload, .LowPriority]
        imagePrefether.maxConcurrentDownloads = 3
        
        feedLoader = FeedLoader(imagePrefether: imagePrefether)
        favoriteLoader = FavoriteLoader(imagePrefether: imagePrefether)
        tagLoader = TagLoader(imagePrefether: imagePrefether)
    }
    
    var rx_page: Variable<Int> {
        return currentLoader.rx_page
    }
    
    func rx_loadData() -> Observable<InstagramMedia> {
        return currentLoader
            .rx_loadData()
            .doOnNext { [weak self] (media) in
                self?.currentMedia = media
            }
            .doOnError { [weak self] (_) in
                self?.currentMedia = nil
            }
    }
    
    func hasPrevious() -> Bool {
        return currentLoader.hasPrevious()
    }
    
    func hasMore() -> Bool {
        return currentLoader.hasMore()
    }
    
    func usernameForPrivateAccount() -> String {
        let page = rx_page.value
        var user: InstagramUser?
        switch widgetsViewModel.segmentState {
        case .Feed:
            user = PeopleManager.sharedManager.feedPeopleList[page]
        case .Favorites:
            user = PeopleManager.sharedManager.favoritePeopleList[page]
        default:
            break
        }
        
        if let user = user {
            return user.fullName ?? user.username
        }
        
        return ""
    }
    
    func profileURLForPrivateAccount() -> NSURL? {
        let page = rx_page.value
        var user: InstagramUser?
        switch widgetsViewModel.segmentState {
        case .Feed:
            user = PeopleManager.sharedManager.feedPeopleList[page]
        case .Favorites:
            user = PeopleManager.sharedManager.favoritePeopleList[page]
        default:
            break
        }
        
        return user?.profilePictureURL
    }
}

private protocol DataLoader {
    var rx_page: Variable<Int> { get }
    var imagePrefether: SDWebImagePrefetcher { get set }
    
    func rx_loadData() -> Observable<InstagramMedia>
    func hasPrevious() -> Bool
    func hasMore() -> Bool
}

private class FeedLoader: NSObject, DataLoader, Rxable {

    init(imagePrefether: SDWebImagePrefetcher) {
        self.imagePrefether = imagePrefether
    }
    
    var imagePrefether: SDWebImagePrefetcher
    
    var rx_page: Variable<Int> = Variable(0)
    
    var dataList: [InstagramUser] {
        var list = PeopleManager.sharedManager.feedPeopleList
        
        // free user can only browse 10
        if ShareDefaultsManager.sharedManager.isPremium == false {
            while list.count > 10 {
                list.removeLast()
            }
        }
        return list
    }
    
    private func rx_loadFeed(forUser user: InstagramUser) -> Observable<InstagramMedia> {
        if let media = CacheManager.sharedManager.loadMonoFeedMedia(forUser: user) {
            return Observable<InstagramMedia>.just(media)
        }
        
        return LoadMediaRequest().rx_loadOne(forUser: user).doOnNext { [weak self] (media) in
            CacheManager.sharedManager.cacheMonoFeedMedia(media)
            self?.imagePrefether.prefetchURLs([media.standardResolutionImageURL])
        }
    }
    
    func rx_loadData() -> Observable<InstagramMedia> {
        let list = dataList
        
        if rx_page.value < 0 {
            rx_page.value = 0
        }
        
        if rx_page.value >= list.count {
            rx_page.value = list.count - 1
        }
        
        let page = rx_page.value
        let user = list[page]
        
        let rx_request = rx_loadFeed(forUser: user)
        
        var rx_nextRequest: Observable<InstagramMedia>?
        if page < list.count - 1 {
            let nextUser = list[page + 1]
            rx_nextRequest = rx_loadFeed(forUser: nextUser)
        }
        
        // always pre-load next page
        return rx_request.doOnNext { [weak self] (_) in
            guard let this = self else { return }
            rx_nextRequest?.subscribe().addDisposableTo(this.disposeBag)
        }
    }
    
    func hasPrevious() -> Bool {
        return rx_page.value > 0
    }
    
    func hasMore() -> Bool {
        return rx_page.value < dataList.count - 1
    }
}

private class FavoriteLoader: NSObject, DataLoader, Rxable {

    init(imagePrefether: SDWebImagePrefetcher) {
        self.imagePrefether = imagePrefether
    }
    
    var imagePrefether: SDWebImagePrefetcher
    
    var rx_page: Variable<Int> = Variable(0)
    
    var dataList: [InstagramUser] {
        return PeopleManager.sharedManager.favoritePeopleList
    }
    
    private func rx_loadFavorite(forUser user: InstagramUser) -> Observable<InstagramMedia> {
        if let media = CacheManager.sharedManager.loadMonoFavoriteMedia(forUser: user) {
            return Observable<InstagramMedia>.just(media)
        }
        
        return LoadMediaRequest().rx_loadOne(forUser: user).doOnNext { [weak self] (media) in
            CacheManager.sharedManager.cacheMonoFavoriteMedia(media)
            self?.imagePrefether.prefetchURLs([media.standardResolutionImageURL])
        }
    }
    
    func rx_loadData() -> Observable<InstagramMedia> {
        
        let list = dataList
        
        if rx_page.value < 0 {
            rx_page.value = 0
        }
        
        if rx_page.value >= list.count {
            rx_page.value = list.count - 1
        }
        
        let page = rx_page.value
        let user = list[page]
        
        let rx_request = rx_loadFavorite(forUser: user)
        
        var rx_nextRequest: Observable<InstagramMedia>?
        if page < list.count - 1 {
            let nextUser = list[page + 1]
            rx_nextRequest = rx_loadFavorite(forUser: nextUser)
        }
        
        // always pre-load next page
        return rx_request.doOnNext { [weak self] (_) in
            guard let this = self else { return }
            rx_nextRequest?.subscribe().addDisposableTo(this.disposeBag)
        }
    }
    
    func hasPrevious() -> Bool {
        return rx_page.value > 0
    }
    
    func hasMore() -> Bool {
        return rx_page.value < PeopleManager.sharedManager.favoritePeopleList.count - 1
    }
}

private class TagLoader: NSObject, DataLoader, Rxable {

    init(imagePrefether: SDWebImagePrefetcher) {
        self.imagePrefether = imagePrefether
    }
    
    var imagePrefether: SDWebImagePrefetcher
    
    var rx_page: Variable<Int> = Variable(0)
    
    var dataList: [InstagramTag] {
        return TagsManager.sharedManager.tagList
    }
    
    private func rx_loadTag(forTag tag: InstagramTag) -> Observable<InstagramMedia> {
        // if has cache, return cache then load next tag
        if let media = CacheManager.sharedManager.loadMonoTagMedia(forTag: tag) {
            return Observable<InstagramMedia>.just(media)
        }
        
        // if no cache, load current tag then next tag
        return LoadMediaRequest().rx_loadOne(forTag: tag).doOnNext { [weak self] (media) in
            CacheManager.sharedManager.cacheMonoTagMedia(media, withTag: tag)
            self?.imagePrefether.prefetchURLs([media.standardResolutionImageURL])
        }
    }
    
    func rx_loadData() -> Observable<InstagramMedia> {
        
        let list = dataList
        
        if rx_page.value < 0 {
            rx_page.value = 0
        }
        
        if rx_page.value >= list.count {
            rx_page.value = list.count - 1
        }
        
        let page = rx_page.value
        let tag = list[page]
        
        let rx_request = rx_loadTag(forTag: tag)
        
        // request for next tag
        var rx_nextRequest: Observable<InstagramMedia>? = nil
        if page < list.count - 1 {
            let nextTag = list[page + 1]
            rx_nextRequest = rx_loadTag(forTag: nextTag)
        }
        
        // always pre-load next page
        return rx_request.doOnNext { [weak self] (_) in
            guard let this = self else { return }
            rx_nextRequest?.subscribe().addDisposableTo(this.disposeBag)
        }
    }
    
    func hasPrevious() -> Bool {
        return rx_page.value > 0
    }
    
    func hasMore() -> Bool {
        return rx_page.value < TagsManager.sharedManager.tagList.count - 1
    }
}
