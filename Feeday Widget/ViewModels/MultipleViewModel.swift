//
//  MultipleViewModel.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/8/9.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import Foundation

import RxSwift
import InstagramKit
import SDWebImage

import FeedayCommonKit

private protocol DataLoader {
    var rx_page: Variable<Int> { get }
    var imagePrefether: SDWebImagePrefetcher { get set }
    
    func rx_loadData() -> Observable<[InstagramMedia]>
    func hasPrevious() -> Bool
    func hasMore() -> Bool
    
    func prefetchImages(fromMedias medias: [InstagramMedia])
}

extension DataLoader {
    func prefetchImages(fromMedias medias: [InstagramMedia]) {
        let urls = medias.map { (media) -> NSURL in
            return media.thumbnailURL
        }
        imagePrefether.prefetchURLs(urls)
    }
}

private class FeedLoader: NSObject, Rxable, DataLoader {
    
    init(imagePrefether: SDWebImagePrefetcher, cacheMethod: ([InstagramMedia] -> Void)?) {
        self.imagePrefether = imagePrefether
        self.cacheMethod = cacheMethod
    }
    
    var imagePrefether: SDWebImagePrefetcher
    var cacheMethod: ([InstagramMedia] -> Void)?
    
    var rx_page: Variable<Int> = Variable(0)
    
    func rx_loadData() -> Observable<[InstagramMedia]> {
        
        let list = dataList
        
        if rx_page.value < 0 {
            rx_page.value = 0
        }
        
        if rx_page.value >= list.count {
            rx_page.value = list.count - 1
        }
        
        let page = rx_page.value
        let user = list[page]
        
        let rx_request = rx_loadFeed(forUser: user)
        
        var rx_nextRequest: Observable<[InstagramMedia]>? = nil
        if hasMore() {
            let nextUser = list[page + 1]
            rx_nextRequest = rx_loadFeed(forUser: nextUser)
        }
        
        // always pre-load next page
        let sig = rx_request.doOnNext { [weak self] (_) in
            guard let this = self else { return }
            rx_nextRequest?.subscribe().addDisposableTo(this.disposeBag)
        }
        
        return sig
    }
    
    func hasPrevious() -> Bool {
        return rx_page.value > 0
    }
    
    func hasMore() -> Bool {
        return rx_page.value < dataList.count - 1
    }
    
    private var dataList: [InstagramUser] {
        var list = PeopleManager.sharedManager.feedPeopleList
        
        // free user can only browse 10
        if ShareDefaultsManager.sharedManager.isPremium == false {
            while list.count > 10 {
                list.removeLast()
            }
        }
        return list
    }
    
    private func rx_loadFeed(forUser user: InstagramUser) -> Observable<[InstagramMedia]> {
        // load from cache first
        if let medias = CacheManager.sharedManager.loadGridFeedMedia(forUser: user, num: 9) {
            return Observable<[InstagramMedia]>.just(medias)
        }
        
        // if no cache, load from network, then cache it
        let request = LoadMediaRequest()
        let sig = request
            .rx_loadMultiple(forUser: user, num: 9)
            .doOnNext { [weak self] (medias) in
                self?.cacheMethod?(medias)
                self?.prefetchImages(fromMedias: medias)
        }
        return sig
    }
}

private class FavoriteLoader: NSObject, Rxable, DataLoader {

    init(imagePrefether: SDWebImagePrefetcher, cacheMethod: ([InstagramMedia] -> Void)?) {
        self.imagePrefether = imagePrefether
        self.cacheMethod = cacheMethod
    }
    
    var imagePrefether: SDWebImagePrefetcher
    var cacheMethod: ([InstagramMedia] -> Void)?
    
    var rx_page: Variable<Int> = Variable(0)
    
    func rx_loadData() -> Observable<[InstagramMedia]> {
        
        let list = PeopleManager.sharedManager.favoritePeopleList
        
        if rx_page.value < 0 {
            rx_page.value = 0
        }
        
        if rx_page.value >= list.count {
            rx_page.value = list.count - 1
        }
        
        let page = rx_page.value
        let user = list[page]
        
        let rx_request = rx_loadFavorite(forUser: user)
        
        var rx_nextRequest: Observable<[InstagramMedia]>? = nil
        if hasMore() {
            let nextUser = list[page + 1]
            rx_nextRequest = rx_loadFavorite(forUser: nextUser)
        }
        
        // always pre-load next page
        return rx_request.doOnNext { [weak self] (_) in
            guard let this = self else { return }
            rx_nextRequest?.subscribe().addDisposableTo(this.disposeBag)
        }
    }
    
    func hasPrevious() -> Bool {
        return rx_page.value > 0
    }
    
    func hasMore() -> Bool {
        return rx_page.value < dataList.count - 1
    }
    
    private var dataList: [InstagramUser] {
        return PeopleManager.sharedManager.favoritePeopleList
    }
    
    private func rx_loadFavorite(forUser user: InstagramUser) -> Observable<[InstagramMedia]> {
        // load from cache first
        if let medias = CacheManager.sharedManager.loadGridFavoriteMedia(forUser: user, num: 9) {
            return Observable<[InstagramMedia]>.just(medias)
        }
        
        // if no cache, load from network, then cache it
        let request = LoadMediaRequest()
        return request
            .rx_loadMultiple(forUser: user, num: 9)
            .doOnNext { [weak self] (medias) in
                self?.cacheMethod?(medias)
                self?.prefetchImages(fromMedias: medias)
        }
    }
}

private class TagLoader: NSObject, Rxable, DataLoader {

    init(imagePrefether: SDWebImagePrefetcher, cacheMethod: ((media: [InstagramMedia], tag: InstagramTag) -> Void)?) {
        self.imagePrefether = imagePrefether
        self.cacheMethod = cacheMethod
    }
    
    var imagePrefether: SDWebImagePrefetcher
    var cacheMethod: ((media: [InstagramMedia], tag: InstagramTag) -> Void)?
    
    var rx_page: Variable<Int> = Variable(0)
    
    func rx_loadData() -> Observable<[InstagramMedia]> {
        
        let list = TagsManager.sharedManager.tagList
        
        if rx_page.value < 0 {
            rx_page.value = 0
        }
        
        if rx_page.value >= list.count {
            rx_page.value = list.count - 1
        }
        
        let page = rx_page.value
        let tag = list[page]
        
        let rx_request = rx_loadTag(forTag: tag)
        
        // build request for next tag
        var rx_nextRequest: Observable<[InstagramMedia]>? = nil
        if hasMore() {
            let nextTag = list[page + 1]
            rx_nextRequest = rx_loadTag(forTag: nextTag)
        }
        
        // always pre-load next page
        return rx_request.doOnNext { [weak self] (_) in
            guard let this = self else { return }
            rx_nextRequest?.subscribe().addDisposableTo(this.disposeBag)
        }
    }
    
    func hasPrevious() -> Bool {
        return rx_page.value > 0
    }
    
    func hasMore() -> Bool {
        return rx_page.value < dataList.count - 1
    }
    
    private var dataList: [InstagramTag] {
        return TagsManager.sharedManager.tagList
    }
    
    private func rx_loadTag(forTag tag:InstagramTag) -> Observable<[InstagramMedia]> {
        // if has cache, return cache then load next tag
        if let medias = CacheManager.sharedManager.loadGridTagMedia(forTag: tag, num: 9) {
            return Observable<[InstagramMedia]>.just(medias)
        }
        
        // if no cache, load then cache it
        let request = LoadMediaRequest()
        return request
            .rx_loadMultiple(forTag: tag, num: 9)
            .doOnNext { [weak self] (medias) in
                self?.cacheMethod?(media: medias, tag: tag)
                self?.prefetchImages(fromMedias: medias)
        }
    }
}

class MultipleViewModel: NSObject, Rxable {
    
    var widgetsViewModel = WidgetsViewModel()
    
    var imagePrefether = SDWebImagePrefetcher.init(imageManager: SDWebImageManager.sharedManager())
    
    var currentMedias: [InstagramMedia] = []
    
    private var feedLoader: FeedLoader
    private var favoriteLoader: FavoriteLoader
    private var tagLoader: TagLoader
    
    private var currentLoader: DataLoader {
        switch widgetsViewModel.segmentState {
        case .Feed:
            return feedLoader
        case .Favorites:
            return favoriteLoader
        case .Tags:
            return tagLoader
        }
    }
    
    override init() {
        imagePrefether.options = [.RetryFailed, .ProgressiveDownload, .LowPriority]
        imagePrefether.maxConcurrentDownloads = 3
        
        feedLoader = FeedLoader(imagePrefether: imagePrefether, cacheMethod: nil)
        favoriteLoader = FavoriteLoader(imagePrefether: imagePrefether, cacheMethod: nil)
        tagLoader = TagLoader(imagePrefether: imagePrefether, cacheMethod: nil)
    }
    
    var rx_page: Variable<Int> {
        return currentLoader.rx_page
    }
    
    func rx_loadData() -> Observable<[InstagramMedia]> {
        return currentLoader
            .rx_loadData()
            .doOnNext { [weak self] (medias) in
                self?.currentMedias = medias
            }
            .doOnError { [weak self] (_) in
                self?.currentMedias = []
        }
    }
    
    func hasPrevious() -> Bool {
        return currentLoader.hasPrevious()
    }
    
    func hasMore() -> Bool {
        return currentLoader.hasMore()
    }
    
    func mediaForRow(row: Int) -> InstagramMedia? {
        let medias = currentMedias
        guard medias.count > 0 && row < medias.count else { return nil }
        let media = medias[row]
        return media
    }
    
    func usernameForPrivateAccount() -> String {
        let page = rx_page.value
        var user: InstagramUser?
        switch widgetsViewModel.segmentState {
        case .Feed:
            user = PeopleManager.sharedManager.feedPeopleList[page]
        case .Favorites:
            user = PeopleManager.sharedManager.favoritePeopleList[page]
        default:
            break
        }
        
        if let user = user {
            return user.fullName ?? user.username
        }
        
        return ""
    }
    
    func profileURLForPrivateAccount() -> NSURL? {
        let page = rx_page.value
        var user: InstagramUser?
        switch widgetsViewModel.segmentState {
        case .Feed:
            user = PeopleManager.sharedManager.feedPeopleList[page]
        case .Favorites:
            user = PeopleManager.sharedManager.favoritePeopleList[page]
        default:
            break
        }
        
        return user?.profilePictureURL
    }
}

class GridViewModel: MultipleViewModel {
    
    override init() {
        super.init()
        
        let cacheFeed = CacheManager.sharedManager.cacheGridFeedMedias
        let cacheFavorite = CacheManager.sharedManager.cacheGridFavoriteMedias
        let cacheTag = CacheManager.sharedManager.cacheGridTagMedias
        
        feedLoader = FeedLoader(imagePrefether: imagePrefether, cacheMethod: cacheFeed)
        favoriteLoader = FavoriteLoader(imagePrefether: imagePrefether, cacheMethod: cacheFavorite)
        tagLoader = TagLoader(imagePrefether: imagePrefether, cacheMethod: cacheTag)
    }
}

class AsymmetricViewModel: GridViewModel {}

