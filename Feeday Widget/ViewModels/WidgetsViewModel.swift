//
//  WidgetsViewModel.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/29.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit

import RxSwift
import Alamofire

import FeedayCommonKit

class WidgetsViewModel: NSObject {
    
    var segmentState: SegmentState = .Feed {
        didSet {
            switch segmentState {
            case .Feed:
                ShareDefaultsManager.sharedManager.widgetSelectedSegment = .Feed
            case .Favorites:
                ShareDefaultsManager.sharedManager.widgetSelectedSegment = .Favorite
            case .Tags:
                ShareDefaultsManager.sharedManager.widgetSelectedSegment = .Tag
            }
        }
    }
    
    var rx_reachabilityStatus: Variable<NetworkReachabilityManager.NetworkReachabilityStatus> = Variable(.Unknown)
    
    var reachabilityManager = NetworkReachabilityManager.init(host: "www.instagram.com")
    
    var displayStyle: DisplayStyle = .Empty
    
    override init() {
        super.init()
        switch ShareDefaultsManager.sharedManager.widgetSelectedSegment {
        case .Feed:
            segmentState = .Feed
        case .Favorite:
            segmentState = .Favorites
        case .Tag:
            segmentState = .Tags
        }
    }
    
    func initReachability() {
        reachabilityManager?.listener = { status in
            self.rx_reachabilityStatus.value = status
            self.updateDisplayStyle()
        }
        
        reachabilityManager?.startListening()
    }
    
    func updateDisplayStyle() {
        // Check Login
        if ShareDefaultsManager.sharedManager.isLoggedIn == false {
            displayStyle = .Empty
            return
        }
        
        // Check State
        if (segmentState == .Feed && PeopleManager.sharedManager.feedPeopleList.count == 0) ||
            (segmentState == .Favorites && PeopleManager.sharedManager.favoritePeopleList.count == 0) ||
            (segmentState == .Tags && TagsManager.sharedManager.tagList.count == 0)
        {
            displayStyle = .Empty
            return
        }
        
        let mgr = ShareDefaultsManager.sharedManager
        
        switch mgr.blockType {
        case .Mono:
            displayStyle = .Mono
        case .Grid:
            displayStyle = .Grid
        case .Asymmetric:
            displayStyle = .Asymmetric
        }
    }
}

enum SegmentState: Int {
    case Feed = 0
    case Favorites = 1
    case Tags = 2
}

enum DisplayStyle: Int {
    case Empty = 0
    case Mono = 1
    case Asymmetric = 2
    case Grid = 3
}