//
//  CacheManager.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/30.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import RxSwift
import InstagramKit

import FeedayCommonKit

// MARK: - CachePair

class CachePair: NSObject, NSCoding {
    var key = ""
    var media = InstagramMedia()
    
    init(key: String, media: InstagramMedia) {
        self.key = key
        self.media = media
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(key, forKey: "key")
        aCoder.encodeObject(media, forKey: "media")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.key = aDecoder.decodeObjectForKey("key") as? String ?? ""
        self.media = aDecoder.decodeObjectForKey("media") as? InstagramMedia ?? InstagramMedia()
    }
}

// MARK: - CacheManager Common

class CacheManager: NSObject, Rxable {
    
    static let sharedManager = CacheManager()
    
    let compacity: Int = 1000
    
    var fileURL: NSURL? {
        return NSFileManager.defaultManager().containerURLForSecurityApplicationGroupIdentifier("group.Feeday.him")?.URLByAppendingPathComponent("Widgets-Cache.plist")
    }
    
    var plist: [CachePair] = []
    
    override init() {
        super.init()
        loadPlist()
        //clearCache()
    }
    
    func clearCache() {
        plist.removeAll()
        savePlist()
    }
    
    func loadPlist() {
        if let url = fileURL {
            if let data = NSData(contentsOfURL: url) {
                if let list = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? [CachePair] {
                    plist = list
                }
            }
        }
    }
    
    func savePlist() -> Bool {
        if let url = fileURL {
            let data = NSKeyedArchiver.archivedDataWithRootObject(plist)
            let flag = data.writeToURL(url, atomically: true)
            return flag
        }
        
        return false
    }
    
    func loadMedia(forKey key: String) -> InstagramMedia? {
        for pair in plist {
            if pair.key == key {
                return pair.media
            }
        }
        
        return nil
    }
    
    func saveMedia(media: InstagramMedia,forKey key: String) {
        rx_saveMedia(media, forKey: key).subscribe().addDisposableTo(disposeBag)
    }
    
    func rx_saveMedia(media: InstagramMedia,forKey key: String) -> Observable<Bool> {
        return Observable<Bool>.create { [weak self] (observer) -> Disposable in
            guard let this = self else {
                observer.onNext(false)
                return NopDisposable.instance
            }
            
            for pair in this.plist {
                if pair.key == key {
                    this.plist.removeAtIndex(this.plist.indexOf(pair)!)
                }
            }
            
            let pair = CachePair(key: key, media: media)
            
            this.plist.append(pair)
            
            while this.plist.count >= this.compacity {
                this.plist.removeFirst()
            }
            
            let flag = this.savePlist()
            observer.onNext(flag)
            
            return NopDisposable.instance
        }
    }
}

// MARK: - Mono

extension CacheManager {
    
    func cacheMonoFeedMedia(media: InstagramMedia) {
        saveMedia(media, forKey: "CacheFeedMedia.Mono.\(media.user.Id)")
    }
    
    func cacheMonoFavoriteMedia(media: InstagramMedia) {
        saveMedia(media, forKey: "CacheFavoriteMedia.Mono.\(media.user.Id)")
    }
    
    func cacheMonoTagMedia(media: InstagramMedia, withTag tag: InstagramTag) {
        saveMedia(media, forKey: "CacheTagMedia.Mono.\(tag.name)")
    }
    
    func loadMonoFeedMedia(forUser user: InstagramUser) -> InstagramMedia? {
        return loadMedia(forKey: "CacheFeedMedia.Mono.\(user.Id)")
    }
    
    func loadMonoFavoriteMedia(forUser user: InstagramUser) -> InstagramMedia? {
        return loadMedia(forKey: "CacheFavoriteMedia.Mono.\(user.Id)")
    }
    
    func loadMonoTagMedia(forTag tag: InstagramTag) -> InstagramMedia? {
        return loadMedia(forKey: "CacheTagMedia.Mono.\(tag.name)")
    }
}

// MARK: - Grid

extension CacheManager {
    
    func cacheGridFeedMedias(medias: [InstagramMedia]) {
        for (idx, media) in medias.enumerate() {
            saveMedia(media, forKey: "CacheFeedMedia.Grid.\(idx).\(media.user.Id)")
        }
    }
    
    func loadGridFeedMedia(forUser user: InstagramUser, num: Int = 9) -> [InstagramMedia]? {
        return loadMedias(forKey: { idx in "CacheFeedMedia.Grid.\(idx).\(user.Id)" }, num: num)
    }
    
    func cacheGridFavoriteMedias(medias: [InstagramMedia]) {
        for (idx, media) in medias.enumerate() {
            saveMedia(media, forKey: "CacheFavoriteMedia.Grid.\(idx).\(media.user.Id)")
        }
    }
    
    func loadGridFavoriteMedia(forUser user: InstagramUser, num: Int = 9) -> [InstagramMedia]? {
        return loadMedias(forKey: { idx in "CacheFavoriteMedia.Grid.\(idx).\(user.Id)" }, num: num)
    }
    
    func cacheGridTagMedias(medias: [InstagramMedia], forTag tag: InstagramTag) {
        for (idx, media) in medias.enumerate() {
            saveMedia(media, forKey: "CacheTagMedia.Grid.\(idx).\(tag.name)")
        }
    }
    
    func loadGridTagMedia(forTag tag: InstagramTag, num: Int = 9) -> [InstagramMedia]? {
        return loadMedias(forKey: { idx in "CacheTagMedia.Grid.\(idx).\(tag.name)" }, num: num)
    }
    
    func loadMedias(forKey key: (Int -> String), num: Int) -> [InstagramMedia]? {
        var arr: [InstagramMedia] = []
        for idx in 0..<num {
            if let media = loadMedia(forKey: key(idx)) {
                arr.append(media)
            }
        }
        
        // no cache
        if arr.count == 0 {
            return nil
        }
        
        // has cache, but not full
        while arr.count < num {
            arr.append(InstagramMedia())
        }
        
        return arr
    }
}
