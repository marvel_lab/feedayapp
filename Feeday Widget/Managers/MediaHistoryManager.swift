//
//  MediaHistoryManager.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/8/5.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import RxSwift
import InstagramKit

import FeedayCommonKit

class MediaHistoryManager: NSObject, Rxable {
    
    static let sharedManager = MediaHistoryManager()
    
    let compacity: Int = 200
    
    var fileURL: NSURL? {
        return NSFileManager.defaultManager().containerURLForSecurityApplicationGroupIdentifier("group.Feeday.him")?.URLByAppendingPathComponent("Widgets-History.plist")
    }
    
    var plist: [String] = []
    
    override init() {
        super.init()
        loadPlist()
    }
    
    func clearHistory() {
        plist.removeAll()
        savePlist()
    }
    
    func loadPlist() {
        if let url = fileURL {
            if let data = NSData(contentsOfURL: url) {
                if let list = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? [String] {
                    plist = list
                }
            }
        }
    }
    
    func savePlist() -> Bool {
        if let url = fileURL {
            let data = NSKeyedArchiver.archivedDataWithRootObject(plist)
            let flag = data.writeToURL(url, atomically: true)
            return flag
        }
        
        return false
    }
    
    func isMediaShowed(mediaID: String) -> Bool {
        return plist.indexOf(mediaID) != nil
    }
    
    func saveMediaHistory(mediaID: String) {
        if let index = plist.indexOf(mediaID) {
            plist.removeAtIndex(index)
        }
        
        plist.append(mediaID)
        
        while plist.count >= compacity {
            plist.removeFirst()
        }
        
        savePlist()
    }
}
