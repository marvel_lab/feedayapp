//
//  WidgetEventHub.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/8/18.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import RxSwift

class WidgetEventHub: NSObject {
    
    static let sharedHub = WidgetEventHub()
    
    var monoSendPhotoVCDismissed: PublishSubject<Void> = PublishSubject()
    
}
