//
//  UIViewController+OpenInstagram.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/8/9.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit

import InstagramKit

import FeedayCommonKit

protocol OpenInstagram {
    
    func openInstagramMedia(media: InstagramMedia)
    
    func openInstagramUser(user: InstagramUser)
}

extension OpenInstagram where Self: UIViewController {
    
    func openInstagramMedia(media: InstagramMedia) {
        if InstagramAppMonitor.getIsInstagramInstalled() {
            if let url = media.appLink {
                extensionContext?.openURL(url, completionHandler: nil)
                return
            }
        }
        
        if let url = NSURL.init(string: media.link) {
            extensionContext?.openURL(url, completionHandler: nil)
        }
    }
    
    func openInstagramUser(user: InstagramUser) {
        if InstagramAppMonitor.getIsInstagramInstalled() {
            if let url = user.appLink {
                extensionContext?.openURL(url, completionHandler: nil)
                return
            }
        }
        
        if let url = user.webLink {
            extensionContext?.openURL(url, completionHandler: nil)
        }
    }
}
