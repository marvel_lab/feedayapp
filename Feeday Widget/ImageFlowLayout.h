//
//  ImageFlowLayout.h
//  Feeday
//
//  Created by Nils Kasseckert on 24.05.15.
//  Copyright (c) 2015 Nils Kasseckert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageFlowLayout : UICollectionViewFlowLayout

@end
