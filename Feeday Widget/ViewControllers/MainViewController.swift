import UIKit
import NotificationCenter

import SDWebImage
import RxSwift

import FeedayCommonKit
import FeedayUIKit

class MainViewController: UIViewController, NCWidgetProviding, Rxable {
    
    let initialSize = CGSize(width: 375, height: 310)
    let monoStyleSize = CGSize(width: 375, height: 468)
    let gridStyleSize = CGSize(width: 375, height: 473)
    let asymmetricStyleSize = CGSize(width: 375, height: 328)
    
    let emptyContainerSize: CGSize = CGSizeMake(350, 225)
    let monoContainerSize: CGSize = CGSizeMake(350, 400)
    let gridContainerSize: CGSize = CGSizeMake(355, 405)
    let asymmetricContainerSize: CGSize = CGSizeMake(355, 260)
    
    @IBOutlet weak var feedButton: UIButton!
    @IBOutlet weak var favoritesButton: UIButton!
    @IBOutlet weak var tagsButton: UIButton!
    
    @IBOutlet weak var navigationContainerView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var containerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var containerViewWidth: NSLayoutConstraint!
    
    private var contentViewController: UIViewController?
    private var compactViewController: MainCompactViewController?
    
    private var viewModel = WidgetsViewModel()
}

// MARK: - Life Cycle

extension MainViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOSApplicationExtension 10.0, *) {
            extensionContext?.widgetLargestAvailableDisplayMode = .Expanded
        }
        
        /*
        // uncomment follow lines to debug cache
        SDWebImageManager.sharedManager().imageCache.clearMemory()
        SDWebImageManager.sharedManager().imageCache.clearDisk()
        */
        
        FlurryManager.startSession()
        FlurryManager.logUserIdIfLoggedIn()
        
        PeopleManager.sharedManager.loadFeedPeople()
        PeopleManager.sharedManager.loadFavoritePeople()
        TagsManager.sharedManager.loadTags()
        
        initSetting()
        initUI()
        initObserveReachability()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        updateUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        SDWebImageManager.sharedManager().imageCache.clearMemory()
    }
    
    func widgetMarginInsetsForProposedMarginInsets(defaultMarginInsets: UIEdgeInsets) -> UIEdgeInsets {
        return UIEdgeInsetsZero
    }
    
    @available(iOSApplicationExtension 10.0, *)
    func widgetActiveDisplayModeDidChange(activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
        preferredContentSize = maxSize
        updateUI()
    }
}

// MARK: - Non Private

extension MainViewController {
    
    @IBAction func segmentButtonPressed(sender: UIButton) {
        guard let segment = SegmentState(rawValue: sender.tag) else { return }
        viewModel.segmentState = segment
        
        switch segment {
        case .Feed:
            FlurryManager.logExtEvent(.TapFeedInWidget)
        case .Favorites:
            FlurryManager.logExtEvent(.TapFavoriteInWidget)
        case .Tags:
            FlurryManager.logExtEvent(.TapTagsInWidget)
        }
        
        updateUI()
    }
}

// MARK: - Private

extension MainViewController {
    
    private func initSetting() {
        // avoid memory issue
        SDWebImageManager.sharedManager().imageDownloader.shouldDecompressImages = false
        SDWebImageManager.sharedManager().imageDownloader.maxConcurrentDownloads = 3
    }
    
    private func initUI() {
        view.backgroundColor = UIColor.init(red: 1, green: 1, blue: 1, alpha: 0.01)
        view.bounds.size = initialSize
        
        updateUI()
    }
    
    private func initObserveReachability() {
        viewModel.initReachability()
        
        // if there is network
        // if previous time open widgets is 10 mins before now
        // clear cache
        if viewModel.reachabilityManager?.isReachable == true {
            let lastTime = ShareDefaultsManager.sharedManager.lastTimeClearWidgetsCache
            let now = NSDate().timeIntervalSince1970
            if now - lastTime >= 600 {
                CacheManager.sharedManager.clearCache()
                ShareDefaultsManager.sharedManager.lastTimeClearWidgetsCache = now
            }
        }
    }
    
    private func updateButtons() {
        let buttons = [
            feedButton,
            favoritesButton,
            tagsButton
        ]
        buttons.forEach { (button) in
            button.alpha = 0.3
            
            if button.tag == viewModel.segmentState.rawValue {
                button.alpha = 1
            }
        }
    }
    
    private func reloadContentVC() {
        if let vc = contentViewController {
            vc.view.removeFromSuperview()
            vc.removeFromParentViewController()
            contentViewController = nil
        }
        
        viewModel.updateDisplayStyle()
        switch viewModel.displayStyle {
        case .Empty:
            updateContentSize(initialSize, containerSize: emptyContainerSize)
            
            if let vc = EmptyStateViewController.instantiateViewController() {
                vc.viewModel = viewModel
                addContentVC(vc)
            }
        case .Mono:
            updateContentSize(monoStyleSize, containerSize: monoContainerSize)
            
            if let vc = MonoViewController.instantiateViewController() {
                vc.viewModel.widgetsViewModel = viewModel
                addContentVC(vc)
            }
        case .Grid:
            updateContentSize(gridStyleSize, containerSize: gridContainerSize)
            
            if let vc = GridViewController.instantiateViewController() {
                vc.viewModel.widgetsViewModel = viewModel
                addContentVC(vc)
            }
        case .Asymmetric:
            updateContentSize(asymmetricStyleSize, containerSize: asymmetricContainerSize)
        
            if let vc = AsymmetricViewController.instantiateViewController() {
                vc.viewModel.widgetsViewModel = viewModel
                addContentVC(vc)
            }
        }
    }
    
    private func addContentVC(vc: UIViewController) {
        contentViewController = vc
        addChildViewController(vc)
        containerView.addSubview(vc.view)
        vc.view.frame = containerView.bounds
        vc.didMoveToParentViewController(self)
    }
    
    private func updateUI() {
        updateButtons()
        reloadContentVC()
        
        if #available(iOSApplicationExtension 10.0, *) {
            guard let activeDisplayMode = extensionContext?.widgetActiveDisplayMode else {
                return
            }
            
            switch activeDisplayMode {
            case .Compact:
                // Reset transform scale, because the height is always 110 points
                view.transform = CGAffineTransformMakeScale(1, 1)
                view.frame.size = preferredContentSize
                view.frame.origin = CGPointZero
                view.layoutSubviews()
                
                // Hide expanded content
                navigationContainerView.hidden = true
                containerView.hidden = true
                
                // Create compact view controller if not created
                if compactViewController == nil {
                    compactViewController = MainCompactViewController.instantiateViewController()
                }
                
                // Add compact view controller into main view controller
                if let vc = compactViewController {
                    vc.reachabilityManager = viewModel.reachabilityManager
                    addChildViewController(vc)
                    view.addSubview(vc.view)
                    vc.view.frame = view.bounds
                    vc.didMoveToParentViewController(self)
                }
            case .Expanded:
                // Show expanded content
                navigationContainerView.hidden = false
                containerView.hidden = false
                
                // Remove compact view controller from main view controller
                if let vc = compactViewController {
                    vc.view.removeFromSuperview()
                    vc.removeFromParentViewController()
                }
            }
        }
    }
    
    private func updateContentSize(viewSize: CGSize, containerSize: CGSize) {
        view.bounds.size = viewSize
        
        if let screenBounds = view.superview?.bounds {
            let designWidth: CGFloat = 375
            let screenWidth = screenBounds.width
            let factor = screenWidth / designWidth
            
            view.transform = CGAffineTransformMakeScale(factor, factor)
            view.frame.origin = CGPointZero
        }
        
        preferredContentSize = view.frame.size
        
        containerViewWidth.constant = containerSize.width
        containerViewHeight.constant = containerSize.height
        view.layoutSubviews()
    }
}

protocol ContentViewControllerProtocol {
    var viewModel: WidgetsViewModel { get set }
}
