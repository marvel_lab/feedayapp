//
//  GridViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/8/7.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit

import RxSwift
import InstagramKit
import SDWebImage

import FeedayUIKit
import FeedayCommonKit

private let reuseID: String = "cell"

class GridViewCollectionViewCell: UICollectionViewCell, Rxable {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mediaImageView: UIImageView!
    @IBOutlet weak var shadowView: UIImageView!
    
    weak var viewModel: GridViewModel?
    
    var rx_mediaDidSet = PublishSubject<Void>()
    
    var media = InstagramMedia() {
        didSet {
            rx_mediaDidSet.onNext()
            updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.feeday_roundCorner(8)
        
        shadowView.hidden = true
    }
    
    private func updateUI() {
        shadowView.hidden = ShareDefaultsManager.sharedManager.hideName
        
        nameLabel.text = media.user.username
        nameLabel.hidden = ShareDefaultsManager.sharedManager.hideName
        
        let mediaID = media.Id
        if mediaID.isEmpty == true {
            // if private account or error
            nameLabel.text = viewModel?.usernameForPrivateAccount()
            mediaImageView.image = nil
            return
        }
        
        mediaImageView.image = nil
        let url = media.thumbnailURL
        SDWebImageManager
            .sharedManager()
            .rx_loadImage(url, options: [.HighPriority, .ProgressiveDownload], timeout: 5, retry: -1)
            .takeUntil(rx_mediaDidSet)
            .observeOn(MainScheduler.instance)
            .subscribeNext { [weak self] (image) in
                self?.mediaImageView?.image = image
            }
            .addDisposableTo(disposeBag)
    }
}

class GridViewController: UIViewController, Rxable, OpenInstagram {
    
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var offlineView: UIView!
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var premiumButton:  UIButton!
    
    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var rightLabel: UILabel!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var viewModel = GridViewModel()
}

extension GridViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
        loadData()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
}

extension GridViewController {
    
    @IBAction func nextButtonPressed(sender: AnyObject) {
        viewModel.rx_page.value += 1
        loadData()
    }
    
    @IBAction func previousButtonPressed(sender: AnyObject) {
        viewModel.rx_page.value -= 1
        loadData()
    }
    
    @IBAction func premiumButtonPressed(sender: AnyObject) {
        if let url = NSURL.init(string: FeedayConstant.LinkType.InAppPurchase.rawValue) {
            extensionContext?.openURL(url, completionHandler: nil)
        }
    }
}

extension GridViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseID, forIndexPath: indexPath) as! GridViewCollectionViewCell
        
        cell.viewModel = viewModel
        
        if let media = viewModel.mediaForRow(indexPath.row) {
            cell.media = media
        } else {
            cell.media = InstagramMedia()
        }
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        collectionView.deselectItemAtIndexPath(indexPath, animated: true)
        
        if let media = viewModel.mediaForRow(indexPath.row) {
            openInstagramMedia(media)
        }
    }
}

extension GridViewController {
    
    private func initUI() {
        view.backgroundColor = UIColor.clearColor()
        
        viewModel
            .widgetsViewModel
            .rx_reachabilityStatus
            .asObservable()
            .subscribeNext {
                [weak self] (status) in
                self?.offlineView.hidden = (self?.viewModel.widgetsViewModel.reachabilityManager?.isReachable == true ?? false)
            }
            .addDisposableTo(disposeBag)
    }
    
    private func updateUI(medias: [InstagramMedia]) {
        loadingView.hidden = true
        previousButton.enabled = false
        nextButton.enabled = false
        premiumButton.enabled = false
        
        leftLabel.text = ""
        rightLabel.text = ""
        
        
        if viewModel.hasPrevious() {
            previousButton.enabled = true
            leftLabel.text = "Previous"
        }
        if viewModel.hasMore() {
            nextButton.enabled = true
            rightLabel.text = "Next"
        } else {
            if ShareDefaultsManager.sharedManager.isPremium == false {
                premiumButton.enabled = true
                rightLabel.text = "Premium"
            }
        }
        
        collectionView.reloadData()
    }
    
    private func loadData() {
        loadingView.hidden = false
        previousButton.enabled = false
        nextButton.enabled = false
        premiumButton.enabled = false
        
        leftLabel.text = ""
        rightLabel.text = ""
        
        viewModel
            .rx_loadData()
            .observeOn(MainScheduler.instance)
            .doOnError { [weak self] (_) in
                self?.updateUI([])
            }
            .subscribeNext { [weak self] (medias) in
                self?.updateUI(medias)
            }
            .addDisposableTo(disposeBag)
    }
}

extension GridViewController: Instantiatable {
    static func storyboardID() -> String {
        return "GridViewController"
    }
    
    static func storyboardName() -> String {
        return "GridViewController"
    }
}
