//
//  MainCompactViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/9/15.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit

import RxSwift
import InstagramKit
import SDWebImage
import Alamofire

import FeedayCommonKit
import FeedayUIKit

class MainCompactViewController: UIViewController, Rxable, OpenInstagram {

    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var likesCountLabel: UILabel!
    @IBOutlet weak var commentsCountLabel: UILabel!
    
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var emptyContentView: UIView!
    
    @IBOutlet weak var imageLoadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var dataLoadingIndicator: UIActivityIndicatorView!
    
    var reachabilityManager: NetworkReachabilityManager?
    
    private var media: InstagramMedia?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        dataLoadingIndicator.hidden = false
        dataLoadingIndicator.startAnimating()
        
        rx_loadData()
            .observeOn(MainScheduler.instance)
            .subscribe { [weak self] (event) in
                guard let this = self else { return }
                this.dataLoadingIndicator.stopAnimating()
                
                switch event {
                case let .Next(media):
                    this.media = media
                    this.updateUI(media)
                case .Error(_):
                    this.media = nil
                    this.updateUI()
                default:
                    break
                }
            }
            .addDisposableTo(disposeBag)
    }
    
    @IBAction func mainImageButtonPressed() {
        if let media = media {
            openInstagramMedia(media)
        }
    }

    @IBAction func emptyViewButtonPressed() {
        let urlString = "feedayios://home"
        if let url = NSURL.init(string: urlString) {
            extensionContext?.openURL(url, completionHandler: nil)
        }
    }
    
    private func rx_loadData() -> Observable<InstagramMedia?> {
        guard ShareDefaultsManager.sharedManager.isLoggedIn == true else {
            return Observable<InstagramMedia?>.just(nil)
        }
        
        guard let user = PeopleManager.sharedManager.feedPeopleList.first else {
            return Observable<InstagramMedia?>.just(nil)
        }
        
        if let media = CacheManager.sharedManager.loadMonoFeedMedia(forUser: user) {
            return Observable<InstagramMedia?>.just(media)
        }
        
        return LoadMediaRequest()
            .rx_loadOne(forUser: user)
            .doOnNext { (media) in
                CacheManager.sharedManager.cacheMonoFeedMedia(media)
            }
            .map { (media) -> InstagramMedia? in
                return media
        }
    }
    
    private func updateUI(media: InstagramMedia? = nil) {
        mainContentView.hidden = true
        emptyContentView.hidden = true
        
        mainImageView.layer.masksToBounds = true
        mainImageView.layer.cornerRadius = 9.4
        
        // If no feed setup or network is off
        guard let media = media where reachabilityManager?.isReachable == true else {
            emptyContentView.hidden = false
            
            mainImageView.image = nil
            usernameLabel.text = ""
            likesCountLabel.text = ""
            commentsCountLabel.text = ""
            return
        }

        mainContentView.hidden = false
        
        imageLoadingIndicator.hidden = false
        imageLoadingIndicator.startAnimating()
        
        let url = media.thumbnailURL
        SDWebImageManager
            .sharedManager()
            .rx_loadImage(url, options: [.HighPriority, .ProgressiveDownload, .RetryFailed], timeout: 5, retry: -1)
            .observeOn(MainScheduler.instance)
            .doOnCompleted { [weak self] in
                self?.imageLoadingIndicator.stopAnimating()
            }
            .doOnError { [weak self] (_) in
                self?.imageLoadingIndicator.stopAnimating()
            }
            .subscribeNext { [weak self] (image) in
                self?.mainImageView?.image = image
            }
            .addDisposableTo(disposeBag)
        
        usernameLabel.text = media.user.fullName
        if usernameLabel.text?.isEmpty == true {
            usernameLabel.text = media.user.username
        }
        
        let numberFormatter = NSNumberFormatter.init()
        numberFormatter.numberStyle = .DecimalStyle
        numberFormatter.decimalSeparator = ","
        
        switch media.likesCount {
        case 0:
            likesCountLabel.text = "no like"
        case 1:
            likesCountLabel.text = "1 like"
        default:
            likesCountLabel.text = (numberFormatter.stringFromNumber(NSNumber.init(long: media.likesCount)) ?? "0") + " likes"
        }
        
        switch media.commentsCount {
        case 0:
            commentsCountLabel.text = "no comment"
        case 1:
            commentsCountLabel.text = "1 comment"
        default:
            commentsCountLabel.text = (numberFormatter.stringFromNumber(NSNumber.init(long: media.commentsCount)) ?? "0") + " comments"
        }
    }
}

extension MainCompactViewController: Instantiatable {
    
    static func storyboardID() -> String {
        return "MainCompactViewController"
    }
    
    static func storyboardName() -> String {
        return "MainCompactViewController"
    }
}
