//
//  EmptyStateViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/29.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit

import FeedayCommonKit
import FeedayUIKit

class EmptyStateViewController: UIViewController, ContentViewControllerProtocol {

    @IBOutlet weak var emptyImageView: UIImageView!
    @IBOutlet weak var hintLabel: UILabel!
    
    var viewModel = WidgetsViewModel()
}

extension EmptyStateViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
    }
    
}

extension EmptyStateViewController {

    @IBAction func mainButtonPressed() {
        var linkType = FeedayConstant.LinkType.App
        
        if ShareDefaultsManager.sharedManager.isLoggedIn {
            switch viewModel.segmentState {
            case .Feed:
                linkType = .AddPeople
            case .Favorites:
                linkType = .AddFavorite
            case .Tags:
                linkType = .AddHashtag
            }
        }
        
        if let url = NSURL.init(string: linkType.rawValue) {
            extensionContext?.openURL(url, completionHandler: nil)
        }
    }
}

extension EmptyStateViewController {
    
    private func initUI() {
        emptyImageView.feeday_roundCorner(7)
        updateUI()
    }
    
    private func updateUI() {
        // check login state
        if ShareDefaultsManager.sharedManager.isLoggedIn == false {
            hintLabel.text = "Connect Instagram"
            showImage("Widget-Connect-Instagram")
            return
        }
        
        switch viewModel.segmentState {
        case .Feed:
            hintLabel.text = "Add Feed"
            showImage("Widget-Add-People")
        case .Favorites:
            hintLabel.text = "Add Favorites"
            showImage("Widget-Add-Favorites")
        case .Tags:
            hintLabel.text = "Add Tags"
            showImage("Widget-Add-Tags")
        }
    }
    
    private func showImage(imageName: String = "") {
        if let path = NSBundle.mainBundle().pathForResource(imageName, ofType: "pic") {
            if let image = UIImage.init(contentsOfFile: path) {
                emptyImageView.image = image
            }
        }
    }
}


// MARK: - Instantiatable

extension EmptyStateViewController: Instantiatable {

    static func storyboardName() -> String {
        return "EmptyState"
    }
    
    static func storyboardID() -> String {
        return "EmptyStateViewController"
    }

}
