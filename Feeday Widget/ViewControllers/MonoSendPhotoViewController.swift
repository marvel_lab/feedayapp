//
//  MonoSendPhotoViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/8/18.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit

import InstagramKit
import SDWebImage
import RxSwift

import FeedayUIKit
import FeedayCommonKit

class MonoSendPhotoViewController: UIViewController, Rxable {
    
    var media: InstagramMedia?
}

extension MonoSendPhotoViewController {
    
    @IBAction func cancelButtonPressed() {
        WidgetEventHub.sharedHub.monoSendPhotoVCDismissed.onNext()
    }
    
    @IBAction func sendEmailButtonPressed() {
        sendPhoto(FeedayConstant.SendPhotoType.Email)
    }
    
    @IBAction func sendMessageButtonPressed() {
        sendPhoto(FeedayConstant.SendPhotoType.Message)
    }
    
    @IBAction func sendWhatsAppButtonPressed() {
        sendPhoto(FeedayConstant.SendPhotoType.WhatsApp)
    }
    
    @IBAction func sendMessengerPressed() {
        sendPhoto(FeedayConstant.SendPhotoType.Messenger)
    }
}

extension MonoSendPhotoViewController {
    
    private func sendPhoto(type: FeedayConstant.SendPhotoType) {
        guard let media = media else { return }
        
        SDWebImageManager.sharedManager()
            .downloadImageWithURL(
                media.standardResolutionImageURL,
                options: [.RetryFailed],
                progress: nil
            ) {
                [weak self] (image, error, cacheType, success, url) in
                guard let this = self else { return }
                guard error == nil && success == true && image != nil && url != nil else { return }
                
                ShareDefaultsManager.sharedManager
                    .rx_saveSendPhoto((media: media, image: image, type: type))
                    .observeOn(MainScheduler.instance)
                    .subscribeNext {
                        this.extensionContext?.openURL(
                            NSURL.init(string: FeedayConstant.LinkType.SendPhoto.rawValue)!,
                            completionHandler: nil)
                    }
                    .addDisposableTo(this.disposeBag)
        }
    }
}

extension MonoSendPhotoViewController: Instantiatable {
    
    static func storyboardName() -> String {
        return "MonoViewController"
    }
    
    static func storyboardID() -> String {
        return "MonoSendPhotoViewController"
    }
}