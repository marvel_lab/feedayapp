//
//  MonoViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/30.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit

import RxSwift
import InstagramKit
import SDWebImage

import FeedayUIKit
import FeedayCommonKit

class MonoViewController: UIViewController, Rxable, OpenInstagram {
    
    @IBOutlet weak var usernameLabel:      UILabel!
    @IBOutlet weak var likesCountLabel:    UILabel!
    @IBOutlet weak var commentsCountLabel: UILabel!
    
    @IBOutlet weak var mainImageView:   UIImageView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var dotView: UIView!
    
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var offlineView: UIView!
    
    @IBOutlet weak var showMoreButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var premiumButton:  UIButton!
    
    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var rightLabel: UILabel!
    
    @IBOutlet weak var mainRoundView: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    var viewModel = MonoViewModel()
    
    private var rx_mediaDidSet = PublishSubject<Void>()
    
    private var sendPhotoVC: MonoSendPhotoViewController?
}

extension MonoViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        loadData()
    }
}

extension MonoViewController {
    
    @IBAction func showMoreButtonPressed(sender: AnyObject) {
        viewModel.rx_page.value += 1
        loadData()
    }
    
    @IBAction func previousButtonPressed(sender: AnyObject) {
        viewModel.rx_page.value -= 1
        loadData()
    }
    
    @IBAction func premiumButtonPressed(sender: AnyObject) {
        // if has no next post and position is 1st: the button is Add More
        if !viewModel.hasMore() && viewModel.rx_page.value == 0 {
            switch viewModel.widgetsViewModel.segmentState {
            case .Feed, .Favorites:
                if let url = NSURL.init(string: FeedayConstant.LinkType.AddPeople.rawValue) {
                    extensionContext?.openURL(url, completionHandler: nil)
                }
            case .Tags:
                if let url = NSURL.init(string: FeedayConstant.LinkType.AddHashtag.rawValue) {
                    extensionContext?.openURL(url, completionHandler: nil)
                }
            }
            return
        }

        // other condition
        if let url = NSURL.init(string: FeedayConstant.LinkType.InAppPurchase.rawValue) {
            extensionContext?.openURL(url, completionHandler: nil)
        }
    }
    
    @IBAction func profileButtonPressed(sender: AnyObject) {
        if let user = viewModel.currentMedia?.user {
            openInstagramUser(user)
        }
    }
    
    @IBAction func mainImageButtonPressed(sender: AnyObject) {
        if let media = viewModel.currentMedia {
            openInstagramMedia(media)
        }
    }
    
    @IBAction func sendPhotoButtonPressed(sender: AnyObject) {
        guard let media = viewModel.currentMedia else { return }
        guard let vc = MonoSendPhotoViewController.instantiateViewController() else { return }
        
        bottomView.hidden = true
        
        sendPhotoVC = vc
        vc.media = media
        addChildViewController(vc)
        mainRoundView.addSubview(vc.view)
        vc.view.frame = mainRoundView.bounds
        vc.didMoveToParentViewController(self)
    }
}

extension MonoViewController {
    
    private func initUI() {
        view.backgroundColor = UIColor.clearColor()
        
        mainRoundView.feeday_roundCorner(12)
        avatarImageView.feeday_roundCorner(20)
        dotView.feeday_roundCorner(5)
        
        viewModel
            .widgetsViewModel
            .rx_reachabilityStatus
            .asObservable()
            .subscribeNext {
                [weak self] (status) in
                self?.offlineView.hidden = (self?.viewModel.widgetsViewModel.reachabilityManager?.isReachable == true ?? false)
            }
            .addDisposableTo(disposeBag)
        
        WidgetEventHub.sharedHub
            .monoSendPhotoVCDismissed
            .subscribeNext { [weak self] in
                self?.sendPhotoVC?.view.removeFromSuperview()
                self?.sendPhotoVC?.removeFromParentViewController()
                self?.sendPhotoVC = nil
                
                self?.bottomView.hidden = false
            }
            .addDisposableTo(disposeBag)
    }
    
    private func updateUI(media: InstagramMedia) {
        rx_mediaDidSet.onNext()
        
        loadingView.hidden = true
        previousButton.hidden = true
        showMoreButton.hidden = true
        premiumButton.hidden = true
        
        leftLabel.text = ""
        rightLabel.text = ""
        
        usernameLabel.hidden = ShareDefaultsManager.sharedManager.hideName

        if let profileURL = media.user.profilePictureURL {
            avatarImageView.sd_setImageWithURL(profileURL, placeholderImage: nil, options: [.RetryFailed, .ProgressiveDownload])
        } else {
            avatarImageView.image = nil
        }
        
        let mediaID = media.Id
        dotView.hidden = MediaHistoryManager.sharedManager.isMediaShowed(mediaID)
        
        mainImageView.image = nil
        
        // If media is video, it doesn't have `standardResolutionImageURL`
        let url = media.isVideo == true ? media.thumbnailURL : media.standardResolutionImageURL
        SDWebImageManager
            .sharedManager()
            .rx_loadImage(url, options: [.HighPriority, .ProgressiveDownload, .RetryFailed], timeout: 5, retry: -1)
            .takeUntil(rx_mediaDidSet)
            .observeOn(MainScheduler.instance)
            .doOnCompleted {
                MediaHistoryManager.sharedManager.saveMediaHistory(mediaID)
            }
            .doOnError { (error) in
                print(error)
            }
            .subscribeNext { [weak self] (image) in
                self?.mainImageView?.image = image
            }
            .addDisposableTo(disposeBag)
        
        usernameLabel.text = media.user.fullName
        if usernameLabel.text?.isEmpty == true {
            usernameLabel.text = media.user.username
        }
        
        let numberFormatter = NSNumberFormatter.init()
        numberFormatter.numberStyle = .DecimalStyle
        numberFormatter.decimalSeparator = ","
        
        switch media.likesCount {
        case 0:
            likesCountLabel.text = "no like"
        case 1:
            likesCountLabel.text = "1 like"
        default:
            likesCountLabel.text = (numberFormatter.stringFromNumber(NSNumber.init(long: media.likesCount)) ?? "0") + " likes"
        }
        
        switch media.commentsCount {
        case 0:
            commentsCountLabel.text = "no comment"
        case 1:
            commentsCountLabel.text = "1 comment"
        default:
            commentsCountLabel.text = (numberFormatter.stringFromNumber(NSNumber.init(long: media.commentsCount)) ?? "0") + " comments"
        }
        
        if viewModel.hasPrevious() {
            previousButton.hidden = false
            leftLabel.text = "Previous"
        }

        updateRightBottomButton()

        // if private account
        if usernameLabel.text == nil || usernameLabel.text?.isEmpty == true {
            usernameLabel.text = viewModel.usernameForPrivateAccount()
            
            if let url = viewModel.profileURLForPrivateAccount() {
                avatarImageView.sd_setImageWithURL(url, placeholderImage: nil, options: [.RetryFailed, .ProgressiveDownload])
            }
        }
    }

    private func updateRightBottomButton() {
        // Premium User
        if ShareDefaultsManager.sharedManager.isPremium {
            if viewModel.hasMore() {
                showMoreButton.hidden = false
                rightLabel.text = "Next"
            }

            return
        }

        // Free User
        // for People & Favorite
        if [SegmentState.Feed, SegmentState.Favorites].contains(viewModel.widgetsViewModel.segmentState) {
            // if has no next post and position is 1st: the button is Add More
            if !viewModel.hasMore() && viewModel.rx_page.value == 0 {
                premiumButton.hidden = false
                rightLabel.text = "Add more people"
                return
            }

            // if has next post and position is less than 10th: the button is Next
            if viewModel.hasMore() && viewModel.rx_page.value < 9 {
                showMoreButton.hidden = false
                rightLabel.text = "Next"
                return
            }
        }

        // for Tags
        if viewModel.widgetsViewModel.segmentState == SegmentState.Tags {
            // if has no next post and position is 1st: the button is Add More
            if !viewModel.hasMore() && viewModel.rx_page.value == 0 {
                premiumButton.hidden = false
                rightLabel.text = "Add more tags"
                return
            }
        }

        // other condition: the button is Premium
        premiumButton.hidden = false
        rightLabel.text = "Premium"
    }

    private func loadData() {
        loadingView.hidden = false
        previousButton.hidden = true
        showMoreButton.hidden = true
        premiumButton.hidden = true
        
        leftLabel.text = ""
        rightLabel.text = ""
        
        usernameLabel.hidden = ShareDefaultsManager.sharedManager.hideName
        
        viewModel
            .rx_loadData()
            .observeOn(MainScheduler.instance)
            .subscribe { [weak self] (event) in
                guard let this = self else { return }
                
                switch event {
                case let .Next(media):
                    this.updateUI(media)
                case .Error(_):
                    this.updateUI(InstagramMedia())
                default:
                    break
                }
            }
            .addDisposableTo(disposeBag)
    }
}

extension MonoViewController: Instantiatable {
    static func storyboardID() -> String {
        return "MonoViewController"
    }
    
    static func storyboardName() -> String {
        return "MonoViewController"
    }
}
