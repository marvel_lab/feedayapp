//
//  AsymmetricViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/8/9.
//  Copyright © 2016年 Feeday. All rights reserved.
//


import UIKit

import RxSwift
import InstagramKit
import SDWebImage

import FeedayUIKit
import FeedayCommonKit

private let reuseID: String = "cell"

class AsymmetricViewCell: UIView, Rxable {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mediaImageView: UIImageView!
    @IBOutlet weak var shadowView: UIView!
    
    @IBInspectable var useStandardResolution: Bool = false
    
    weak var viewModel: AsymmetricViewModel?
    
    var rx_mediaDidSet = PublishSubject<Void>()
    
    var media = InstagramMedia() {
        didSet {
            rx_mediaDidSet.onNext()
            updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        shadowView.hidden = true
    }
    
    private func updateUI() {
        shadowView.hidden = ShareDefaultsManager.sharedManager.hideName
        
        nameLabel.text = media.user.username
        nameLabel.hidden = ShareDefaultsManager.sharedManager.hideName
        
        let mediaID = media.Id
        if mediaID.isEmpty == true {
            // if private account or error
            nameLabel.text = viewModel?.usernameForPrivateAccount()
            mediaImageView.image = nil
            return
        }
        
        mediaImageView.image = nil
        
        var url = media.thumbnailURL
        if media.isVideo == false && useStandardResolution == true {
            url = media.standardResolutionImageURL
        }
        
        SDWebImageManager
            .sharedManager()
            .rx_loadImage(url, options: [.HighPriority, .ProgressiveDownload], timeout: 5, retry: -1)
            .takeUntil(rx_mediaDidSet)
            .observeOn(MainScheduler.instance)
            .subscribeNext { [weak self] (image) in
                self?.mediaImageView?.image = image
            }
            .addDisposableTo(disposeBag)
    }
}

class AsymmetricViewController: UIViewController, Rxable, OpenInstagram {
    
    
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var offlineView: UIView!
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var premiumButton:  UIButton!
    
    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var rightLabel: UILabel!
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var cell1: AsymmetricViewCell!
    @IBOutlet weak var cell2: AsymmetricViewCell!
    @IBOutlet weak var cell3: AsymmetricViewCell!
    @IBOutlet weak var cell4: AsymmetricViewCell!
    @IBOutlet weak var cell5: AsymmetricViewCell!
    
    var viewModel = AsymmetricViewModel()
    
    private var cells: [AsymmetricViewCell] {
        return [
            cell1,
            cell2,
            cell3,
            cell4,
            cell5
        ]
    }
}

extension AsymmetricViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
        loadData()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
}

extension AsymmetricViewController {
    
    @IBAction func nextButtonPressed(sender: AnyObject) {
        viewModel.rx_page.value += 1
        loadData()
    }
    
    @IBAction func previousButtonPressed(sender: AnyObject) {
        viewModel.rx_page.value -= 1
        loadData()
    }
    
    @IBAction func premiumButtonPressed(sender: AnyObject) {
        if let url = NSURL.init(string: FeedayConstant.LinkType.InAppPurchase.rawValue) {
            extensionContext?.openURL(url, completionHandler: nil)
        }
    }
    
    @IBAction func cellButtonPressed(sender: UIButton) {
        var idx = sender.tag
        idx = max(0, idx)
        idx = min(idx, 4)
        
        if let media = viewModel.mediaForRow(idx) {
            openInstagramMedia(media)
        }
    }
}

extension AsymmetricViewController {
    
    private func initUI() {
        view.backgroundColor = UIColor.clearColor()
        
        mainView.feeday_roundCorner(8)
        
        viewModel
            .widgetsViewModel
            .rx_reachabilityStatus
            .asObservable()
            .subscribeNext {
                [weak self] (status) in
                self?.offlineView.hidden = (self?.viewModel.widgetsViewModel.reachabilityManager?.isReachable == true ?? false)
            }
            .addDisposableTo(disposeBag)
    }
    
    private func updateUI(medias: [InstagramMedia]) {
        loadingView.hidden = true
        previousButton.enabled = false
        nextButton.enabled = false
        premiumButton.enabled = false
        
        leftLabel.text = ""
        rightLabel.text = ""
        
        
        if viewModel.hasPrevious() {
            previousButton.enabled = true
            leftLabel.text = "Previous"
        }
        if viewModel.hasMore() {
            nextButton.enabled = true
            rightLabel.text = "Next"
        } else {
            if ShareDefaultsManager.sharedManager.isPremium == false {
                premiumButton.enabled = true
                rightLabel.text = "Premium"
            }
        }
        
        updateCells()
    }
    
    private func loadData() {
        loadingView.hidden = false
        previousButton.enabled = false
        nextButton.enabled = false
        premiumButton.enabled = false
        
        leftLabel.text = ""
        rightLabel.text = ""
        
        viewModel
            .rx_loadData()
            .observeOn(MainScheduler.instance)
            .doOnError { [weak self] (_) in
                self?.updateUI([])
            }
            .subscribeNext {
                [weak self] (medias) in
                self?.updateUI(medias)
            }
            .addDisposableTo(disposeBag)
    }
    
    private func updateCells() {
        for (idx, cell) in cells.enumerate() {
            cell.viewModel = viewModel
            if let media = viewModel.mediaForRow(idx) {
                cell.media = media
            } else {
                cell.media = InstagramMedia()
            }
        }
    }
}

extension AsymmetricViewController: Instantiatable {
    static func storyboardID() -> String {
        return "AsymmetricViewController"
    }
    
    static func storyboardName() -> String {
        return "AsymmetricViewController"
    }
}
