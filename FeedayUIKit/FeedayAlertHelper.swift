//
//  FeedayAlertHelper.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/4/17.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit

public class FeedayAlertHelper: NSObject {
    
    public static func showAlert(
        inViewController vc:UIViewController,
                         title: String = "",
                         message: String = "",
                         okTitle: String = "OK",
                         cancelTitle: String = "Cancel",
                         showCancel: Bool = false,
                         okHandler:((action: UIAlertAction) -> ())? = nil,
                         cancelHandler:((action: UIAlertAction) -> ())? = nil
        ) {
        dispatch_async(dispatch_get_main_queue()) { 
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: okTitle, style: UIAlertActionStyle.Cancel, handler: okHandler)
            alert.addAction(okAction)
            
            if showCancel {
                let cancelAction = UIAlertAction(title: cancelTitle, style: UIAlertActionStyle.Default, handler: cancelHandler)
                alert.addAction(cancelAction)
            }
            
            vc.presentViewController(alert, animated: true, completion: { [weak alert] in
                alert?.view.tintColor = UIColor(red: 255/255.0, green: 19/255.0, blue: 83/255.0, alpha: 1)
                })
            alert.view.tintColor = UIColor(red: 255/255.0, green: 19/255.0, blue: 83/255.0, alpha: 1)
        }
    }
}
