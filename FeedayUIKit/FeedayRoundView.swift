//
//  FeedayRoundView.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/5/2.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit

@IBDesignable public class FeedayRoundView: UIView {
    
    @IBInspectable var cornerRadius: Int = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    override public func prepareForInterfaceBuilder() {
        self.updateUI()
    }
    
    private func commonInit() {
        self.updateUI()
    }
    
    private func updateUI() {
        self.clipsToBounds = true
        
        self.layer.masksToBounds = true
        self.layer.cornerRadius = CGFloat(self.cornerRadius)
        
    }
    
    public override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        
        self.updateUI()
    }
}
