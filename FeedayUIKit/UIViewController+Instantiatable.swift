import UIKit

public protocol Instantiatable {
    associatedtype T: UIViewController
    static func instantiateViewController() -> T?
    static func instantiateViewControllerWithNavigationController() -> UINavigationController?
    
    static func storyboardName() -> String
    static func storyboardID() -> String
    static func storyboardIDForNavigationController() -> String?
}

public extension Instantiatable where Self: UIViewController {
    public static func instantiateViewController() -> Self? {
        let sb = UIStoryboard(name: self.storyboardName(), bundle: NSBundle(forClass: Self.self))
        return sb.instantiateViewControllerWithIdentifier(self.storyboardID()) as? Self
    }
    
    public static func instantiateViewControllerWithNavigationController() -> UINavigationController? {
        if let navID = self.storyboardIDForNavigationController() {
            let sb = UIStoryboard(name: self.storyboardName(), bundle: NSBundle(forClass: Self.self))
            return sb.instantiateViewControllerWithIdentifier(navID) as? UINavigationController
        }
        
        if let vc = self.instantiateViewController() {
            return UINavigationController(rootViewController: vc)
        }
        
        return nil
    }
    
    public static func storyboardIDForNavigationController() -> String? {
        return nil
    }
}