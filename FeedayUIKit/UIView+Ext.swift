//
//  UIView+Ext.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/26.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit

public extension UIView {
    
    public func feeday_roundCorner(radius: CGFloat = 10.5) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
}