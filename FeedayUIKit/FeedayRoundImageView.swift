//
//  FeedayRoundImageView.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/3/25.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit

@IBDesignable public class FeedayRoundImageView: UIImageView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        updateUI()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        updateUI()
    }
    
    override public func prepareForInterfaceBuilder() {
        updateUI()
    }
    
    public func updateUI() {
        clipsToBounds = true
        layer.masksToBounds = true
        
        layoutIfNeeded()
        let width = bounds.width
        let radius = width / 2.0
        layer.cornerRadius = radius
    }
    
}
