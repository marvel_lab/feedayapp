//
//  FeedayRoundButton.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/3/23.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit

@IBDesignable public class FeedayRoundButton: UIButton {

    @IBInspectable public var cornerRadius: CGFloat = 0 {
        didSet {
            self.updateUI()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    override public func prepareForInterfaceBuilder() {
        self.updateUI()
    }
    
    private func commonInit() {
        self.updateUI()
    }
    
    private func updateUI() {
        self.clipsToBounds = true
        
        self.layer.masksToBounds = true
        self.layer.cornerRadius = self.cornerRadius
    }
    
}
