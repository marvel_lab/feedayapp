import RxSwift
import MBProgressHUD

extension ObservableType {
    
    func showHUDWhileExecuting(view: UIView) -> RxSwift.Observable<Self.E> {
        
        dispatch_async(dispatch_get_main_queue()) {
            MBProgressHUD.showHUDAddedTo(view, animated: true)
        }
        
        return self.doOn { (_) in
            dispatch_async(dispatch_get_main_queue()) {
                MBProgressHUD.hideAllHUDsForView(view, animated: true)
            }
        }
    }
}