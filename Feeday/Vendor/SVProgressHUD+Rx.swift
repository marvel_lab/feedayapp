import RxSwift
import SVProgressHUD

extension ObservableType {
    
    func showHUDWhileExecuting() -> RxSwift.Observable<Self.E> {
        SVProgressHUD.show()
        return self.doOn { (_) in
            SVProgressHUD.dismiss()
        }
    }
}