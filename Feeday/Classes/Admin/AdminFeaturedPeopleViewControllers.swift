//
//  AdminFeaturedPeopleViewControllers.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/15.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import Social

import RxSwift
import RxCocoa
import InstagramKit

import FeedayUIKit
import FeedayCommonKit

class AdminFeaturedPeopleCell: UICollectionViewCell {
    var model = InstagramUser() {
        didSet {
            updateUI()
        }
    }
    
    @IBOutlet weak var avatarImageView: FeedayRoundImageView!
    
    @IBOutlet weak var usernameLabel: UILabel!
    
    private func updateUI() {
        usernameLabel.text = model.username
        avatarImageView.updateUI()
        avatarImageView.sd_setImageWithURL(model.profilePictureURL)
    }
}

class AdminFeaturedPeopleCollectionViewController: UICollectionViewController, RAReorderableLayoutDelegate {
    
    private var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AdminDataManager
            .sharedManager
            .featuredPeopleListVar
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribeNext { (_) in
                self.collectionView?.reloadData()
            }
            .addDisposableTo(disposeBag)
        
        AdminDataManager
            .sharedManager
            .rx_loadFeaturedPeopleList()
            .showHUDWhileExecuting()
            .observeOn(MainScheduler.instance)
            .subscribe { (event) in
                if let err = event.error as? NSError {
                    let msg = err.localizedDescription
                    FeedayAlertHelper.showAlert(inViewController: self, message: msg)
                }
                
                self.collectionView?.reloadData()
            }
            .addDisposableTo(disposeBag)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        collectionView?.reloadData()
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return AdminDataManager.sharedManager.featuredPeopleList.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("AdminFeaturedPeopleCell", forIndexPath: indexPath) as! AdminFeaturedPeopleCell
        
        let list = AdminDataManager.sharedManager.featuredPeopleList
        
        let row = indexPath.row
        if row < list.count {
            cell.model = list[row]
        }
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, atIndexPath: NSIndexPath, didMoveToIndexPath toIndexPath: NSIndexPath) {
        let atIndex = atIndexPath.row
        let toIndex = toIndexPath.row
        
        var list = AdminDataManager.sharedManager.featuredPeopleList
        
        let user = list[atIndex]
        list.removeAtIndex(list.indexOf(user)!)
        list.insert(user, atIndex: toIndex)
        AdminDataManager.sharedManager.featuredPeopleListVar.value = list
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        collectionView.deselectItemAtIndexPath(indexPath, animated: true)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) { (action) -> Void in }
        
        let removeAction = UIAlertAction(title: "Remove", style: UIAlertActionStyle.Default) { action -> () in
            var list = AdminDataManager.sharedManager.featuredPeopleList
            let user = list[indexPath.row]
            list.removeAtIndex(list.indexOf(user)!)
            AdminDataManager.sharedManager.featuredPeopleListVar.value = list
        }
        
        let updateProfileAction = UIAlertAction(title: "Update Profile", style: UIAlertActionStyle.Default) { [weak self] action -> () in
            guard let this = self else { return }
            
            var list = AdminDataManager.sharedManager.featuredPeopleList
            let user = list[indexPath.row]
            
            this.rx_updateProfile(user)
                .showHUDWhileExecuting()
                .subscribe { [weak self] (event) in
                    if let _ = event.element {
                        self?.collectionView?.reloadData()
                    }
                }
                .addDisposableTo(this.disposeBag)
        }
        
        let tweetAction = UIAlertAction.init(title: "Tweet", style: .Default) { [weak self] (action) in
            guard let this = self else { return }
            
            var list = AdminDataManager.sharedManager.featuredPeopleList
            let user = list[indexPath.row]
            let username = user.username
            
            let alertTwitterUnavailable = {
                FeedayAlertHelper.showAlert(inViewController: this, message: "Please log in your twitter account in device setting")
            }
            
            guard SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter) else {
                alertTwitterUnavailable()
                return
            }
            
            guard let vc = SLComposeViewController.init(forServiceType: SLServiceTypeTwitter) else {
                alertTwitterUnavailable()
                return
            }
            
            vc.setInitialText("New entry in Trending 💥@\(username)💥on #Feedayapp this week get.feedayapp.com")
            this.presentViewController(vc, animated: true, completion: nil)
        }
        
        let sheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        sheet.addAction(cancelAction)
        sheet.addAction(updateProfileAction)
        sheet.addAction(tweetAction)
        sheet.addAction(removeAction)
        
        // For iPad
        if let cell = collectionView.cellForItemAtIndexPath(indexPath) {
            sheet.popoverPresentationController?.sourceView = cell
            sheet.popoverPresentationController?.sourceRect = CGRect.init(
                x: cell.bounds.width / 2,
                y: cell.bounds.height / 2,
                width: 1,
                height: 1
            )
        } else {
            sheet.popoverPresentationController?.sourceView = collectionView
        }
        
        presentViewController(sheet, animated: true, completion: nil)
    }
    
    private func rx_updateProfile(user: InstagramUser) -> Observable<InstagramUser> {
        let engine = InstagramEngine.sharedEngineWithToken()
        let signal = engine
            .rx_getUserDetails(user.Id)
            .doOn { [weak self] (event) in
                guard let this = self else { return }
                
                switch event {
                case let .Error(err as NSError):
                    FeedayAlertHelper.showAlert(inViewController: this, message: err.localizedDescription)
                case let .Next(newUser):
                    var list = AdminDataManager.sharedManager.featuredPeopleList
                    guard let idx = list.indexOf(user) else { break }
                    list[idx] = newUser
                    AdminDataManager.sharedManager.featuredPeopleListVar.value = list
                default:
                    break
                }
        }
        return signal
    }
}

class AdminFeaturedPeopleViewController: UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    
    private var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let vc = UIStoryboard.init(name: "Admin", bundle: nil).instantiateViewControllerWithIdentifier("AdminFeaturedPeopleCollectionViewController") as? AdminFeaturedPeopleCollectionViewController {
            addChildViewController(vc)
            contentView.addSubview(vc.view)
            vc.view.frame = contentView.bounds
            vc.didMoveToParentViewController(self)
        }
    }
    
    @IBAction func randomButtonPressed() {
        AdminDataManager.sharedManager.featuredPeopleListVar.value = AdminDataManager.sharedManager.featuredPeopleList.shuffle()
    }
    
    @IBAction func doneButtonPressed() {
        AdminDataManager
            .sharedManager
            .rx_saveFeaturedPeopleList()
            .showHUDWhileExecuting()
            .observeOn(MainScheduler.instance)
            .subscribe { (event) in
                if let err = event.error as? NSError {
                    let msg = err.localizedDescription
                    FeedayAlertHelper.showAlert(inViewController: self, message: msg)
                } else {
                    self.navigationController?.popViewControllerAnimated(true)
                }
            }
            .addDisposableTo(disposeBag)
    }
    
    @IBAction func cancelButtonPressed() {
        navigationController?.popViewControllerAnimated(true)
    }
}

class AdminSearchFeaturedPeopleCell: UITableViewCell {
    var model = InstagramUser() {
        didSet {
            updateUI()
        }
    }
    
    @IBOutlet weak var avatarImageView: FeedayRoundImageView!
    
    @IBOutlet weak var usernameLabel: UILabel!
    
    private func updateUI() {
        usernameLabel.text = model.username
        avatarImageView.sd_setImageWithURL(model.profilePictureURL)
    }
}

class AdminSearchFeaturedPeopleViewController: UITableViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var disposeBag = DisposeBag()
    
    var list: [InstagramUser] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar
            .rx_searchButtonClicked
            .doOn { (event) in
                self.searchBar.resignFirstResponder()
            }
            .filter { (_) -> Bool in
                return self.searchBar.text?.characters.count > 0
            }
            .flatMap { self.searchSignal() }
            .observeOn(MainScheduler.instance)
            .subscribeNext { (users) in
                self.list = users
                self.tableView.reloadData()
            }
            .addDisposableTo(disposeBag)
    }
    
    private func searchSignal() -> Observable<[InstagramUser]> {
        return SearchPeopleRequest().rx_request(self.searchBar.text ?? "").showHUDWhileExecuting()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("AdminSearchFeaturedPeopleCell", forIndexPath: indexPath) as! AdminSearchFeaturedPeopleCell
        
        let row = indexPath.row
        if row < list.count {
            cell.model = list[row]
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let user = list[indexPath.row]
        var people = AdminDataManager.sharedManager.featuredPeopleList
        if !people.contains(user) {
            people.append(user)
            AdminDataManager.sharedManager.featuredPeopleListVar.value = people
            navigationController?.popViewControllerAnimated(true)
        }
    }
}
