//
//  AdminFeaturedTagsViewControllers.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/15.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import FeedayUIKit
import FeedayCommonKit
import InstagramKit

// MARK: - Container

class AdminFeaturedTagsViewController: UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    
    private var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let vc = UIStoryboard.init(name: "Admin", bundle: nil).instantiateViewControllerWithIdentifier("AdminFeaturedTagsCollectionViewController") as? AdminFeaturedTagsCollectionViewController {
            addChildViewController(vc)
            contentView.addSubview(vc.view)
            vc.view.frame = contentView.bounds
            vc.didMoveToParentViewController(self)
        }
    }
    
    @IBAction func randomButtonPressed() {
        AdminDataManager.sharedManager.featuredTagsListVar.value = AdminDataManager.sharedManager.featuredTagsList.shuffle()
    }
    
    @IBAction func doneButtonPressed() {
        AdminDataManager
            .sharedManager
            .rx_saveFeaturedTagsList()
            .showHUDWhileExecuting()
            .observeOn(MainScheduler.instance)
            .subscribe { (event) in
                if let err = event.error as? NSError {
                    let msg = err.localizedDescription
                    FeedayAlertHelper.showAlert(inViewController: self, message: msg)
                } else {
                    self.navigationController?.popViewControllerAnimated(true)
                }
            }
            .addDisposableTo(disposeBag)
    }
    
    @IBAction func cancelButtonPressed() {
        navigationController?.popViewControllerAnimated(true)
    }
}

// MARK: - Collection

class AdminFeaturedTagsCollectionViewController: UICollectionViewController, RAReorderableLayoutDelegate {
    
    private var disposeBag = DisposeBag()
    
    private var sizingCell: AddTagsCollectionViewCell?
    
    private let cellID = "AddTagsCollectionViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cellNib = UINib(nibName: cellID, bundle: nil)
        collectionView?.registerNib(cellNib, forCellWithReuseIdentifier: cellID)
        sizingCell = cellNib.instantiateWithOwner(nil, options: nil).first as? AddTagsCollectionViewCell
        
        AdminDataManager
            .sharedManager
            .featuredTagsListVar
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribeNext { (_) in
                self.collectionView?.reloadData()
            }
            .addDisposableTo(disposeBag)
        
        AdminDataManager
            .sharedManager
            .rx_loadFeaturedTagsList()
            .showHUDWhileExecuting()
            .observeOn(MainScheduler.instance)
            .subscribe { (event) in
                if let err = event.error as? NSError {
                    let msg = err.localizedDescription
                    FeedayAlertHelper.showAlert(inViewController: self, message: msg)
                }
                
                self.collectionView?.reloadData()
            }
            .addDisposableTo(disposeBag)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        collectionView?.reloadData()
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return AdminDataManager.sharedManager.featuredTagsList.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellID, forIndexPath: indexPath) as! AddTagsCollectionViewCell
        
        let list = AdminDataManager.sharedManager.featuredTagsList
        
        let row = indexPath.row
        if row < list.count {
            cell.model = list[row]
        }
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, atIndexPath: NSIndexPath, didMoveToIndexPath toIndexPath: NSIndexPath) {
        let atIndex = atIndexPath.row
        let toIndex = toIndexPath.row
        
        var list = AdminDataManager.sharedManager.featuredTagsList
        
        let tag = list[atIndex]
        list.removeAtIndex(list.indexOf(tag)!)
        list.insert(tag, atIndex: toIndex)
        AdminDataManager.sharedManager.featuredTagsListVar.value = list
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        collectionView.deselectItemAtIndexPath(indexPath, animated: true)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) { (action) -> Void in }
        
        let removeAction = UIAlertAction(title: "Remove", style: UIAlertActionStyle.Default) { action -> () in
            var list = AdminDataManager.sharedManager.featuredTagsList
            let tag = list[indexPath.row]
            list.removeAtIndex(list.indexOf(tag)!)
            AdminDataManager.sharedManager.featuredTagsListVar.value = list
        }
        
        let sheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        sheet.addAction(cancelAction)
        sheet.addAction(removeAction)
        
        // For iPad
        if let cell = collectionView.cellForItemAtIndexPath(indexPath) {
            sheet.popoverPresentationController?.sourceView = cell
            sheet.popoverPresentationController?.sourceRect = CGRect.init(
                x: cell.bounds.width / 2,
                y: cell.bounds.height / 2,
                width: 1,
                height: 1
            )
        } else {
            sheet.popoverPresentationController?.sourceView = collectionView
        }
        
        presentViewController(sheet, animated: true, completion: nil)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if let cell = sizingCell {
            let row = indexPath.row
            if row < AdminDataManager.sharedManager.featuredTagsList.count {
                let tag = AdminDataManager.sharedManager.featuredTagsList[row]
                cell.model = tag
                return cell.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)
            }
        }
        
        return CGSizeZero
    }
}

// MARK: - Search

class AdminSearchFeaturedTagsCell: UITableViewCell {
    var model = InstagramTag() {
        didSet {
            updateUI()
        }
    }
    
    @IBOutlet weak var nameLabel: UILabel!
    
    private func updateUI() {
        nameLabel.text = model.name
    }
}

class AdminSearchFeaturedTagsViewController: UITableViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var disposeBag = DisposeBag()
    
    var list: [InstagramTag] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar
            .rx_searchButtonClicked
            .doOn { (event) in
                self.searchBar.resignFirstResponder()
            }
            .filter { (_) -> Bool in
                return self.searchBar.text?.characters.count > 0
            }
            .flatMap { self.searchSignal() }
            .observeOn(MainScheduler.instance)
            .subscribeNext { (tags) in
                self.list = tags
                self.tableView.reloadData()
            }
            .addDisposableTo(disposeBag)
    }
    
    private func searchSignal() -> Observable<[InstagramTag]> {
        return SearchTagsRequest().rx_request(self.searchBar.text ?? "").showHUDWhileExecuting()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("AdminSearchFeaturedTagsCell", forIndexPath: indexPath) as! AdminSearchFeaturedTagsCell
        
        let row = indexPath.row
        if row < list.count {
            cell.model = list[row]
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let tag = list[indexPath.row]
        var tags = AdminDataManager.sharedManager.featuredTagsList
        if !tags.contains(tag) {
            tags.append(tag)
            AdminDataManager.sharedManager.featuredTagsListVar.value = tags
            navigationController?.popViewControllerAnimated(true)
        }
    }
}
