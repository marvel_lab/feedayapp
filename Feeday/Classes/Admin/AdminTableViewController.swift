//
//  AdminTableViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/15.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit

class AdminTableViewController: UITableViewController {

    @IBAction func closeButtonPressed() {
        navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
}
