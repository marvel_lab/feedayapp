//
//  AddContentViewModel.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/5.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import RxSwift
import InstagramKit
import FeedayCommonKit

class AddContentViewModel: NSObject
{
    var disposeBag = DisposeBag()

    var enterSearchSignal: Observable<()> {
        return _enterSearchSubject.asObservable()
    }

    var exitSearchSignal: Observable<()> {
        return _exitSearchSubject.asObservable()
    }

    var unfocusSearchSignal: Observable<()> {
        return _unfocusSearchSubject.asObservable()
    }

    var newContentAddedSignal: Observable<String> {
        return _newContentAddedSubject.asObservable()
    }

    private var _enterSearchSubject = PublishSubject<()>()

    private var _exitSearchSubject = PublishSubject<()>()

    private var _unfocusSearchSubject = PublishSubject<()>()

    private var _newContentAddedSubject = PublishSubject<String>()

    func enterSearch() {
        _enterSearchSubject.onNext(())
    }

    func exitSearch() {
        _exitSearchSubject.onNext(())
    }

    func unfocusSearch() {
        _unfocusSearchSubject.onNext(())
    }

    func newContentAdded(content: String) {
        _newContentAddedSubject.onNext(content)
    }
}

