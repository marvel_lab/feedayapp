//
//  SavingPreferenceHUDViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/11/24.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import FeedayUIKit

class SavingPreferenceHUDViewController: UIViewController {
    
}

extension SavingPreferenceHUDViewController: Instantiatable {
    
    static func storyboardName() -> String {
        return "AddFollowContent"
    }
    
    static func storyboardID() -> String {
        return "SavingPreferenceHUDViewController"
    }
}
