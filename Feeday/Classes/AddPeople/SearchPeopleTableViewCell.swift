//
//  SearchPeopleTableViewCell.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/5.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import FeedayUIKit
import InstagramKit

class SearchPeopleTableViewCell: UITableViewCell {
    
    var model = InstagramUser() {
        didSet {
            updateUI()
        }
    }

    @IBOutlet weak var avatarImageView: FeedayRoundImageView!

    @IBOutlet weak var usernameLabel: UILabel!
    
    private func updateUI() {
        usernameLabel.text = model.username
        avatarImageView.sd_setImageWithURL(model.profilePictureURL)
    }
}
