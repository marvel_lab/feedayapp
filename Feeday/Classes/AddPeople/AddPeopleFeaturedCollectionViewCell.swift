//
//  AddPeopleFeaturedCollectionViewCell.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/3.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import FeedayUIKit
import InstagramKit

class AddPeopleFeaturedCollectionViewCell: UICollectionViewCell {
    
    var model: InstagramUser? {
        didSet {
            guard let user = model else { return }
            updateUI(user)
        }
    }
    
    @IBOutlet weak var avatarImageView: FeedayRoundImageView!
    @IBOutlet weak var avatarMaskView: FeedayRoundImageView!
    @IBOutlet weak var plusView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!

    private func updateUI(user: InstagramUser) {
        avatarImageView.sd_setImageWithURL(user.profilePictureURL, placeholderImage: UIImage.init(named: "avatar"), options: .RetryFailed)
        nameLabel.text = user.username
        
        avatarMaskView.hidden = !user.isFeed()
        plusView.hidden = !avatarMaskView.hidden
    }
}