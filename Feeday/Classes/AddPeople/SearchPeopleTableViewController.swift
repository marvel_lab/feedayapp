//
//  SearchPeopleTableViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/5.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import RxSwift

import FeedayCommonKit
import FeedayUIKit

class SearchPeopleTableViewController: UITableViewController {

    var viewModel = AddPeopleViewModel()
    
    private var disposeBag = DisposeBag()
    
    private var peopleManager = PeopleManager.sharedManager
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel
            .searchPeopleResultSignal
            .subscribeNext { [weak self] (users) in
                self?.tableView.reloadData()
        }.addDisposableTo(disposeBag)
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.searchPeopleList.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("SearchPeopleTableViewCell", forIndexPath: indexPath) as! SearchPeopleTableViewCell

        let row = indexPath.row
        
        if row < viewModel.searchPeopleList.count {
            cell.model = viewModel.searchPeopleList[row]
        }

        return cell
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if let cell = tableView.cellForRowAtIndexPath(indexPath) as? SearchPeopleTableViewCell {
            let user = cell.model
            if !user.isFeed() {
                peopleManager.addFeedPeople(user)
                viewModel.newContentAdded(user.username)
            }
            viewModel.exitSearch()
        }
    }
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        viewModel.unfocusSearch()
    }
}

extension SearchPeopleTableViewController: Instantiatable {
    
    static func storyboardName() -> String {
        return "AddFollowContent"
    }
    
    static func storyboardID() -> String {
        return "SearchPeopleTableViewController"
    }
}
