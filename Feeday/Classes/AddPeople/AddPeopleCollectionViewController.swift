//
//  AddPeopleCollectionViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/6.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import RxSwift
import FeedayUIKit
import FeedayCommonKit
import InstagramKit

class AddPeopleCollectionViewController: UICollectionViewController, RAReorderableLayoutDelegate {
    
    var viewModel = AddPeopleViewModel()
    
    private var disposeBag = DisposeBag()
    
    private var peopleManager = PeopleManager.sharedManager
    
}

// MARK: - Life Cycle

extension AddPeopleCollectionViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        peopleManager
            .peopleListSignal
            .subscribeNext { [weak self] (users) in
                self?.collectionView?.reloadData()
            }
            .addDisposableTo(disposeBag)
        
        viewModel
            .stateChangeSignal
            .subscribeNext { [weak self] (_) in
                self?.collectionView?.reloadData()
            }
            .addDisposableTo(disposeBag)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        collectionView?.reloadData()
    }
    
}

// MARK: - UICollectionView

extension AddPeopleCollectionViewController {
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let peopleList = viewModel.state == .Feed ? peopleManager.feedPeopleList : peopleManager.favoritePeopleList
        
        return peopleList.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("AddPeopleCollectionViewCell", forIndexPath: indexPath) as! AddPeopleCollectionViewCell
        
        let item = indexPath.item
        
        let peopleList = viewModel.state == .Feed ? peopleManager.feedPeopleList : peopleManager.favoritePeopleList
        
        if item < peopleList.count {
            cell.model = peopleList[item]
            cell.onImageFailedLoad = { [weak self] (error) in
                guard let this = self else { return }
                guard error.code == 404 else { return }
                // if 404, update profile automatically
                this.viewModel
                    .rx_updateProfile(cell.model)
                    .subscribe { (event) in
                        if let _ = event.element {
                            this.collectionView?.reloadData()
                        }
                    }
                    .addDisposableTo(this.disposeBag)

            }
        } else {
            cell.reset()
        }
        
        return cell
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        collectionView.deselectItemAtIndexPath(indexPath, animated: true)
        
        let peopleList = viewModel.state == .Feed ? peopleManager.feedPeopleList : peopleManager.favoritePeopleList
        let movePeopleFunction = viewModel.state == .Feed ? peopleManager.moveFeedPeople : peopleManager.moveFavorite
        
        let item = indexPath.item
        guard item < peopleList.count else { return }
        let user = peopleList[item]
        
        let sheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: { (action) -> Void in
            
        })
        let openInstagramAction = UIAlertAction(title: "Open in Instagram", style: UIAlertActionStyle.Default) { action -> () in
            let username = user.username
            
            // Instagram installed
            if UIApplication.sharedApplication().canOpenURL(NSURL.init(string: "instagram://app")!) {
                let urlString = "instagram://user?username=\(username)"
                UIApplication.sharedApplication().openURL(NSURL.init(string: urlString)!)
                return
            }
            
            guard let url = NSURL.init(string: "https://www.instagram.com/\(username)/") else { return }
            UIApplication.sharedApplication().openURL(url)
        }
        let updateProfileAction = UIAlertAction(title: "Update Profile Image", style: UIAlertActionStyle.Default) { [weak self] action -> () in
            guard let this = self else { return }
            
            this.viewModel
                .rx_updateProfile(user)
                .showHUDWhileExecuting()
                .subscribe { [weak self] (event) in
                    if let _ = event.element {
                        self?.collectionView?.reloadData()
                    }
                }
                .addDisposableTo(this.disposeBag)
        }
        let moveUpAction = UIAlertAction(title: "Move Up", style: UIAlertActionStyle.Default) { action -> () in
            let currentIndex = item
            let toIndex = currentIndex - 1
            
            if toIndex < 0 {
                return
            }
            
            movePeopleFunction(user, toIndex: toIndex)
        }
        let moveDownAction = UIAlertAction(title: "Move Down", style: UIAlertActionStyle.Default) { action -> () in
            let currentIndex = item
            let toIndex = currentIndex + 1
            
            if toIndex >= peopleList.count {
                return
            }
            
            movePeopleFunction(user, toIndex: toIndex)
        }
        let addToFavoriteAction = UIAlertAction(title: "Add to Favorites", style: UIAlertActionStyle.Default) { [weak self] action -> () in
            // Non-Premium User only can add 1 favorite
            if self?.peopleManager.favoritePeopleList.count >= 1 &&
                ShareDefaultsManager.sharedManager.isPremium == false {
                
                if let vc = IAPPremiumViewController.instantiateViewController() {
                    self?.presentViewController(vc, animated: true, completion: nil)
                }
                
                return
            }
            
            self?.peopleManager.addFavorite(user)
        }
        let removeAction = UIAlertAction(title: "Remove", style: UIAlertActionStyle.Default) { [weak self] action -> () in
            self?.peopleManager.removeFeedPeople(user)
            
            if user.isFavorite() {
                self?.peopleManager.removeFavorite(user)
            }
        }
        let unfavoriteAction = UIAlertAction(title: "Unfavorite", style: UIAlertActionStyle.Default) { [weak self] action -> () in
            self?.peopleManager.removeFavorite(user)
        }
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            sheet.addAction(cancelAction)
        }
        
        // Add "Move Up" option if not 1st item
        if item > 0 {
            sheet.addAction(moveUpAction)
        }
        
        // Add "Move Down" option if not last item
        if item < peopleList.count - 1 {
            sheet.addAction(moveDownAction)
        }
        
        if viewModel.state == .Feed {
            // Show "Add to Favorites" if not favorited
            if user.isFavorite() == false {
                sheet.addAction(addToFavoriteAction)
            }

            sheet.addAction(openInstagramAction)
            
            if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
                sheet.addAction(updateProfileAction)
            }
            
            sheet.addAction(removeAction)
        }
        
        
        if viewModel.state == .Favorite {
            sheet.addAction(unfavoriteAction)
            
            sheet.addAction(openInstagramAction)
            
            if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
                sheet.addAction(updateProfileAction)
            }
        }
        
        
        sheet.popoverPresentationController?.sourceView = collectionView.cellForItemAtIndexPath(indexPath) ?? view
        sheet.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.Down

        dump(sheet.actions)
        
        sheet.view.tintColor = UIColor(red: 255/255.0, green: 19/255.0, blue: 83/255.0, alpha: 1)
        self.presentViewController(sheet, animated: true, completion: nil)
        sheet.view.tintColor = UIColor(red: 255/255.0, green: 19/255.0, blue: 83/255.0, alpha: 1)
    }
    
    func collectionView(collectionView: UICollectionView, atIndexPath: NSIndexPath, didMoveToIndexPath toIndexPath: NSIndexPath) {
        switch viewModel.state {
        case .Feed:
            guard atIndexPath.item < peopleManager.feedPeopleList.count else { return }
            let user = peopleManager.feedPeopleList[atIndexPath.item]
            peopleManager.moveFeedPeople(user, toIndex: toIndexPath.item)
        case .Favorite:
            guard atIndexPath.item < peopleManager.favoritePeopleList.count else { return }
            let user = peopleManager.favoritePeopleList[atIndexPath.item]
            peopleManager.moveFavorite(user, toIndex: toIndexPath.item)
        }
    }
}

// MARK: - Instantiatable

extension AddPeopleCollectionViewController: Instantiatable {
    
    static func storyboardName() -> String {
        return "AddFollowContent"
    }
    
    static func storyboardID() -> String {
        return "AddPeopleCollectionViewController"
    }
}
