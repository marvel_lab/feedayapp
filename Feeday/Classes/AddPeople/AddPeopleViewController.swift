//
//  AddPeopleViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/11.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MBProgressHUD
import InstagramKit

import FeedayUIKit
import FeedayCommonKit

class AddPeopleViewController: UIViewController {

    @IBOutlet weak var featuredContainer: UIView!
    @IBOutlet weak var mainContainer: UIView!
    @IBOutlet weak var listContainer: UIView!

    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var searchTitleView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchBarTrailing: NSLayoutConstraint!
    @IBOutlet weak var cancelSearchButton: UIButton!

    @IBOutlet weak var topLeftButton: UIButton!

    @IBOutlet weak var emptyHintLabel: UILabel!
    @IBOutlet weak var whiteShadow: UIImageView!

    @IBOutlet weak var feedButton: UIButton!
    @IBOutlet weak var favoriteButton: UIButton!
    
    @IBOutlet weak var toastLabel: UILabel!
    @IBOutlet weak var toastLabelHeight: NSLayoutConstraint!
    
    var viewModel = AddPeopleViewModel()
    
    private var featuredPeopleVC: AddPeopleFeaturedCollectionViewController?
    
    private var peopleVC: AddPeopleCollectionViewController?
    
    private weak var searchPeopleVC: SearchPeopleTableViewController?
    
    private let disposeBag = DisposeBag()
    
    private let peopleManager = PeopleManager.sharedManager
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.zz_scaleInFactorRelativeByHeight667()
        view.feeday_roundCorner()
        
        initSearch()
        initStateButton()
        initEmptyHint()
        initTopLeftButton()
        
        initPeopleVC()
        initFeaturedPeopleVC()

        observeNewContentAdded()
        
        peopleManager
            .rx_gotProfileURLFromPasteboard
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribeNext { [weak self] (_) in
                self?.checkPeopleAddedFromPasteboard()
            }
            .addDisposableTo(disposeBag)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        checkPeopleAddedFromPasteboard()
    }
}

// MARK: - Non Private

extension AddPeopleViewController {
    
    @IBAction func doneButtonPressed(sender: AnyObject) {
        let sigFeed = PeopleManager.sharedManager.rx_saveFeedPeople()
        let sigFavorite = PeopleManager.sharedManager.rx_saveFavoritePeople()
        let sig = Observable.combineLatest(sigFeed, sigFavorite) { (_, _) -> Void in }
        
        let hud = SavingPreferenceHUDViewController.instantiateViewController()
        
        if let hud = hud {
            addChildViewController(hud)
            view.addSubview(hud.view)
            hud.view.frame = view.bounds
            hud.didMoveToParentViewController(self)
        }
        
        sig
            .observeOn(MainScheduler.instance)
            .subscribe { [weak self] (event) in
                hud?.view.removeFromSuperview()
                hud?.removeFromParentViewController()
                self?.dismissViewControllerAnimated(true, completion: nil)
            }
            .addDisposableTo(disposeBag)
    }
    
    @IBAction func searchBarReturnPressed(sender: UITextField) {
        searchTextField.resignFirstResponder()
        
        MBProgressHUD.showHUDAddedTo(view, animated: true)
        
        viewModel
            .rx_searchPeople(sender.text ?? "")
            .doOn({ [weak self] (event) in
                guard let this = self else { return }
                MBProgressHUD.hideHUDForView(this.view, animated: true)
                })
            .subscribe()
            .addDisposableTo(disposeBag)
    }
    
    @IBAction func searchBarBeginEditing(sender: UITextField) {
        viewModel.enterSearch()
    }
    
    @IBAction func cancelSearchButtonPressed(sender: AnyObject) {
        viewModel.exitSearch()
    }
    
    @IBAction func feedButtonPressed(sender: AnyObject) {
        viewModel.state = .Feed
    }
    
    @IBAction func favoriteButtonPressed(sender: AnyObject) {
        viewModel.state = .Favorite
    }

    @IBAction func topLeftButtonPressed() {
        // TODO: revert chagnes
        dismissViewControllerAnimated(true, completion: nil)
    }
}

// MARK: - Private

extension AddPeopleViewController {
    
    private func initPeopleVC() {
        updatePeopleContainer()
        updateWhiteShadow()
        
        peopleManager
            .peopleListSignal
            .subscribeNext { [weak self] (users) in
                self?.updatePeopleContainer()
                self?.updateWhiteShadow()
            }
            .addDisposableTo(disposeBag)
        
        viewModel
            .stateChangeSignal
            .subscribeNext { [weak self] (_) in
                self?.updatePeopleContainer()
                self?.updateWhiteShadow()
            }
            .addDisposableTo(disposeBag)
        
        peopleVC = AddPeopleCollectionViewController.instantiateViewController()
        if let vc = peopleVC {
            vc.viewModel = viewModel
            addChildViewController(vc)
            listContainer.addSubview(vc.view)
            vc.view.frame = listContainer.bounds
            vc.didMoveToParentViewController(self)
        }
    }
    
    private func updatePeopleContainer() {
        let peopleList = viewModel.state == .Feed ? peopleManager.feedPeopleList : peopleManager.favoritePeopleList
        
        listContainer.hidden = (peopleList.count == 0)
    }
    
    private func initFeaturedPeopleVC() {
        featuredPeopleVC = AddPeopleFeaturedCollectionViewController.instantiateViewController()
        if let vc = featuredPeopleVC {
            vc.viewModel = viewModel
            addChildViewController(vc)
            featuredContainer.addSubview(vc.view)
            vc.view.frame = featuredContainer.bounds
            vc.didMoveToParentViewController(self)
        }
    }
    
    private func loadSearchVC() {
        unloadSearchVC()
        
        if let vc = SearchPeopleTableViewController.instantiateViewController() {
            searchPeopleVC = vc
            vc.viewModel = viewModel
            addChildViewController(vc)
            mainContainer.addSubview(vc.view)
            vc.view.frame = mainContainer.bounds
            vc.didMoveToParentViewController(self)
        }
    }
    
    private func unloadSearchVC() {
        searchPeopleVC?.view.removeFromSuperview()
        searchPeopleVC?.removeFromParentViewController()
    }
    
    private func initSearch() {
        let constantSearchTrailingCompact: CGFloat = 86
        let constantSearchTrailingFull: CGFloat = 16
        let duration = 0.3
        
        titleView.hidden = false
        searchTitleView.hidden = true
        
        cancelSearchButton.hidden = true
        searchBarTrailing.constant = constantSearchTrailingFull
        
        viewModel.enterSearchSignal.subscribeNext { [weak self] (_) in
            self?.titleView.hidden = true
            self?.searchTitleView.hidden = false
            self?.cancelSearchButton.hidden = false
            self?.loadSearchVC()
            
            self?.view.layoutIfNeeded()
            UIView.animateWithDuration(duration, animations: {
                self?.searchBarTrailing.constant = constantSearchTrailingCompact
                self?.view.layoutIfNeeded()
            })
            }.addDisposableTo(disposeBag)
        
        viewModel.exitSearchSignal.subscribeNext { [weak self] (_) in
            self?.searchTextField.resignFirstResponder()
            self?.searchTextField.text = ""
            self?.titleView.hidden = false
            self?.searchTitleView.hidden = true
            self?.cancelSearchButton.hidden = true
            self?.unloadSearchVC()
            
            self?.view.layoutIfNeeded()
            UIView.animateWithDuration(duration, animations: {
                self?.searchBarTrailing.constant = constantSearchTrailingFull
                self?.view.layoutIfNeeded()
            })
            }.addDisposableTo(disposeBag)
        
        viewModel.unfocusSearchSignal.subscribeNext { [weak self] (_) in
            self?.searchTextField.resignFirstResponder()
            }.addDisposableTo(disposeBag)
    }
    
    private func initStateButton() {
        viewModel
            .stateChangeSignal
            .subscribeNext { [weak self] (state) in
                self?.updateStateButton(state)
            }
            .addDisposableTo(disposeBag)
        
        updateStateButton()
    }
    
    private func updateStateButton(state: AddPeopleViewModel.AddPeopleState = .Feed) {
        let buttons = [
            feedButton,
            favoriteButton
        ]
        
        for btn in buttons {
            btn.alpha = 0.3
        }
        
        switch state {
        case .Feed:
            feedButton.alpha = 1
        case .Favorite:
            favoriteButton.alpha = 1
        }
    }
    
    private func initEmptyHint() {
        viewModel
            .stateChangeSignal
            .subscribeNext { [weak self] (state) in
                self?.updateEmptyHint(state)
            }
            .addDisposableTo(disposeBag)
        
        updateEmptyHint()
    }
    
    private func updateEmptyHint(state: AddPeopleViewModel.AddPeopleState = .Feed) {
        let isPremium = ShareDefaultsManager.sharedManager.isPremium
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.2
        paragraphStyle.alignment = .Left
        
        let freeHintFeed = [
            "• Add from Trending list, by searching, or copy",
            "  profile URL from Instagram",
            "• Their feed will always displayed in time order",
            "• Press & hold on a person to arrange"
            ]
            .joinWithSeparator("\n")
        
        let premiumHintFeed = [
            "• Add from Top Instagramers, by searching, or",
            "  copy profile URL from Instagram",
            "• Their feed will always displayed in time order",
            "• Press & hold on a person to arrange"
            ]
            .joinWithSeparator("\n")
        
        let freeHintFavorite = [
            "• Tap a person from “People” to add",
            "• You can add only 1 favorite",
            "• Press & hold on a person to arrange",
            "• Switching to Premium you can add unlimited",
            "   favorites"
            ]
            .joinWithSeparator("\n")
        
        let premiumHintFavorite = [
            "• Tap a person from “People” to add",
            "• Add unlimited favorites",
            "• Press & hold on a person to arrange"
            ]
            .joinWithSeparator("\n")
        
        let freeHint = NSMutableAttributedString(string: state == .Favorite ? freeHintFavorite: freeHintFeed)
        freeHint.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, freeHint.length))
        
        let premiumHint = NSMutableAttributedString(string: state == .Favorite ? premiumHintFavorite : premiumHintFeed)
        premiumHint.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, premiumHint.length))
        
        emptyHintLabel.attributedText = isPremium ? premiumHint : freeHint
    }
    
    private func checkPeopleAddedFromPasteboard() {
        if PeopleManager.sharedManager.gotProfileURLFromPasteboard == true {
            PeopleManager.sharedManager.gotProfileURLFromPasteboard = false
            showTopToast("Got Profile URL")
        }
    }

    private func observeNewContentAdded() {
        viewModel
            .newContentAddedSignal
            .observeOn(MainScheduler.instance)
            .subscribeNext { [weak self] content in
                if content.isEmpty == false {
                    self?.showTopToast(content + " added 👌")
                }
            }
            .addDisposableTo(disposeBag)
    }

    private func showTopToast(text: String) {
        let label = toastLabel
        let height: CGFloat = 35
        view.feeday_roundCorner(0)

        label.text = text
        toastLabelHeight.constant = 0
        view.layoutIfNeeded()
        label.hidden = false

        let duration = 0.5
        UIView.animateWithDuration(duration) {
            self.toastLabelHeight.constant = height
            self.view.layoutIfNeeded()

            dispatch_after(
                dispatch_time(DISPATCH_TIME_NOW, Int64(2 * NSEC_PER_SEC)),
                dispatch_get_main_queue()
            ) {
                UIView.animateWithDuration(
                    duration,
                    animations: {
                        self.toastLabelHeight.constant = 0
                        self.view.layoutIfNeeded()
                    },
                    completion: { (complete) in
                        label.hidden = true
                        self.view.feeday_roundCorner()
                    })
            }
        }
    }

    private func updateWhiteShadow() {
        whiteShadow.hidden = listContainer.hidden
    }

    private func initTopLeftButton() {
        topLeftButton.hidden = false
        topLeftButton.setTitle("Cancel", forState: UIControlState.Normal)
    }
}

// MARK: - Instantiatable

extension AddPeopleViewController: Instantiatable {
    
    static func storyboardName() -> String {
        return "AddFollowContent"
    }
    
    static func storyboardID() -> String {
        return "AddPeopleViewController"
    }
    
}
