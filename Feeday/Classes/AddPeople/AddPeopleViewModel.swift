//
//  AddPeopleViewModel.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/11.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import RxSwift
import InstagramKit
import FeedayCommonKit

class AddPeopleViewModel: AddContentViewModel, Rxable {
    
    // MARK: - State
    
    enum AddPeopleState: Int {
        case Feed = 1
        case Favorite = 2
    }
    
    var state: AddPeopleState = .Feed {
        didSet {
            _stateChangeSubject.onNext(state)
        }
    }
    
    var stateChangeSignal: Observable<AddPeopleState> {
        return _stateChangeSubject.asObservable()
    }
    
    private var _stateChangeSubject = PublishSubject<AddPeopleState>()
    
    // MARK: - Search
    
    var searchPeopleList = [InstagramUser]() {
        didSet {
            _serachPeopleResultSubject.onNext(searchPeopleList)
        }
    }
    
    var searchPeopleResultSignal: Observable<[InstagramUser]> {
        return _serachPeopleResultSubject.asObservable()
    }
    
    private var _serachPeopleResultSubject = PublishSubject<[InstagramUser]>()
    
    func rx_searchPeople(username: String = "") -> Observable<[InstagramUser]> {
        let request = SearchPeopleRequest()
        let signal = request
            .rx_request(username)
            .doOnNext { (users) in
                self.searchPeopleList = users
        }
        return signal
    }
    
    // MARK: - Update Profile
    
    func rx_updateProfile(user: InstagramUser) -> Observable<InstagramUser> {
        let engine = InstagramEngine.sharedEngineWithToken()
        let signal = engine
            .rx_getUserDetails(user.Id)
            .doOn { [weak self] (event) in
                guard let this = self else { return }
                
                switch event {
                case let .Error(err as NSError):
                    print(err.localizedDescription)
                case let .Next(newUser):
                    if this.state == .Feed {
                        if let idx = PeopleManager.sharedManager.feedPeopleList.indexOf(user) {
                            PeopleManager.sharedManager.feedPeopleList[idx] = newUser
                        }
                    }
                    
                    if this.state == .Favorite {
                        if let idx = PeopleManager.sharedManager.favoritePeopleList.indexOf(user) {
                            PeopleManager.sharedManager.favoritePeopleList[idx] = newUser
                        }
                    }
                default:
                    break
                }
        }
        return signal
    }
}
