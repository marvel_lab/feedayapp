//
//  AddPeopleCollectionViewCell.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/6.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import InstagramKit
import SDWebImage
import FeedayUIKit

class AddPeopleCollectionViewCell: UICollectionViewCell {
    
    typealias ImageFailedLoadBlock = (NSError) -> ()
    
    var model = InstagramUser() {
        didSet {
            updateUI()
        }
    }
    
    var onImageFailedLoad: ImageFailedLoadBlock?
    
    @IBOutlet weak var avatarImageView: FeedayRoundImageView!
    
    @IBOutlet weak var favoriteImageView: UIImageView!
    
    @IBOutlet weak var usernameLabel: UILabel!
    
    func reset() {
        model = InstagramUser()
        onImageFailedLoad = nil
    }
    
    private func updateUI() {
        usernameLabel.text = model.username
        
        avatarImageView.image = UIImage.init(named: "avatar")
        SDWebImageManager
            .sharedManager()
            .downloadImageWithURL(
                model.profilePictureURL,
                options:  [.RetryFailed, .HighPriority, .ProgressiveDownload],
                progress: nil
            ) { [weak self] (image, error, cacheType, complete, url) in
                if image != nil {
                    self?.avatarImageView?.image = image
                    return
                }
                
                if error != nil {
                    self?.onImageFailedLoad?(error)
                }
        }
        
        favoriteImageView.hidden = !model.isFavorite()
    }
}
