//
//  AddPeopleFeaturedCollectionViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/3.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import RxSwift
import MBProgressHUD
import InstagramKit

import FeedayUIKit
import FeedayCommonKit

private let reuseIdentifier = "AddPeopleFeaturedCollectionViewCell"

class AddPeopleFeaturedCollectionViewController: UICollectionViewController, Rxable {

    var viewModel = AddPeopleViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        initData()
    }
}

// MARK: - Init

extension AddPeopleFeaturedCollectionViewController {
    
    private func initData() {
        PeopleManager
            .sharedManager
            .rx_featuredPeopleList
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribeNext { [weak self] (_) in
                self?.collectionView?.reloadData()
            }
            .addDisposableTo(disposeBag)
        
        PeopleManager
            .sharedManager
            .feedPeopleListSignal
            .observeOn(MainScheduler.instance)
            .subscribeNext { [weak self] (_) in
                self?.collectionView?.reloadData()
            }
            .addDisposableTo(disposeBag)
        
        PeopleManager.sharedManager.loadFeaturedPeopleList()
    }

}

// MARK: - UICollectionView

extension AddPeopleFeaturedCollectionViewController {
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return PeopleManager.sharedManager.featuredPeopleList.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! AddPeopleFeaturedCollectionViewCell
        cell.model = userForIndexPath(indexPath)
        return cell
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        collectionView.deselectItemAtIndexPath(indexPath, animated: true)
        
        guard let user = userForIndexPath(indexPath) else { return }
        if !user.isFeed() {
            PeopleManager.sharedManager.addFeedPeople(user)
            collectionView.reloadData()
            viewModel.newContentAdded(user.username)
        }
    }
    
    func userForIndexPath(indexPath: NSIndexPath) -> InstagramUser? {
        let list = PeopleManager.sharedManager.featuredPeopleList
        if indexPath.row < list.count {
            return list[indexPath.row]
        }
        
        return nil
    }
}

// MARK: - Instantiatable

extension AddPeopleFeaturedCollectionViewController: Instantiatable {

    static func storyboardName() -> String {
        return "AddFollowContent"
    }
    
    static func storyboardID() -> String {
        return "AddPeopleFeaturedCollectionViewController"
    }
}
