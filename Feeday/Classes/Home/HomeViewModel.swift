//
//  HomeViewModel.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/6/27.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import Foundation

import RxSwift

import FeedayCommonKit

class HomeViewModel: NSObject, Rxable {
    
    override init() {
        super.init()
        
        initTimer()
    }
    
    var bigHintContent: String {
        let count = PeopleManager.sharedManager.feedPeopleList.count + TagsManager.sharedManager.tagList.count
        
        if count == 0 {
            return "Let’s start"
        } else {
            return rx_time.value
        }
    }
    
    var welcomeHintContent: String {
        let calendar = NSCalendar.currentCalendar()
        let hour = calendar.component(NSCalendarUnit.Hour, fromDate: NSDate())
        var str = ""
        switch hour {
        case 0..<12:
            str = "Morning"
        case 12..<18:
            str = "Good afternoon"
        case 18...24:
            str = "Good evening"
        default: break
        }
        
        return (str + ", " +  UserPlistManager.sharedManager().getUsernanme()).uppercaseString
    }
    
    var smallHintContent: String {
        return WelcomTextManager.sharedManager.loadText()
    }
    
    var rx_time: Variable<String> = Variable("")
    
    var rx_userContentChange: Observable<()> {
        let sig1 = PeopleManager.sharedManager.feedPeopleListSignal.map { (users) -> () in
            return ()
        }
        
        let sig2 = TagsManager.sharedManager.tagListSignal.map { (tags) -> () in
            return ()
        }
        
        let sig = Observable.of(sig1, sig2).merge()
        return sig
    }
    
    var isUserAdmin: Bool {
        let userID = UserPlistManager.sharedManager().getUserID()
        let limitedIDs = [
            "2108182012", // feedayapp
            "2227691205", // zzdjk6
            "2632216", // arcangelofiore
        ]
        
        return limitedIDs.contains(userID)
    }
    
    func initTimer() {
        updateTime()
        
        Observable<Int>
            .interval(30, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .subscribeNext { [weak self] (_) in
                self?.updateTime()
            }
            .addDisposableTo(disposeBag)
    }
    
    private func updateTime() {
        let date = NSDate()
        let formatter = NSDateFormatter()
        formatter.dateFormat = "HH:mm"
        let str = formatter.stringFromDate(date)
        rx_time.value = str
    }
}
