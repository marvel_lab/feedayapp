//
//  HomeTableViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/6/27.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit

import RxCocoa
import RxSwift
import FLAnimatedImage

import FeedayCommonKit
import FeedayUIKit

private let INDEX_PEOPLE_ROW  = 1
private let INDEX_HASHTAG_ROW = 2

class HomeTableViewController: UITableViewController, Rxable {
    
    @IBOutlet weak var addPeopleTitleLabel: UILabel!
    @IBOutlet weak var addHashtagTitleLabel: UILabel!
    
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    @IBOutlet weak var bigHintLabel: UILabel!
    @IBOutlet weak var smallHintLabel: UILabel!
    
    @IBOutlet weak var addPeopleImageView: FLAnimatedImageView!
    @IBOutlet weak var addTagsImageView: FLAnimatedImageView!
    
    var viewModel = HomeViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
        initObserveData()
        initObserveProfilePictureLoaded()

        if #available(iOS 9.0, *) {
            registerForPreviewingWithDelegate(self, sourceView: view)
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        updateUI()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if identifier == "Admin" {
            return viewModel.isUserAdmin
        }
        
        return true
    }
}

extension HomeTableViewController: UIViewControllerPreviewingDelegate {

    func previewingContext(previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        let indexPath = tableView.indexPathForRowAtPoint(location)

        if indexPath?.row == INDEX_PEOPLE_ROW {
            if let vc = AddPeopleViewController.instantiateViewController() {
                return vc
            }
        }

        if indexPath?.row == INDEX_HASHTAG_ROW {
            if let vc = AddTagsViewController.instantiateViewController() {
                return vc
            }
        }

        return nil
    }

    func previewingContext(previewingContext: UIViewControllerPreviewing, commitViewController viewControllerToCommit: UIViewController) {
        showViewController(viewControllerToCommit, sender: nil)
    }

}

// MARK: - Custom

extension HomeTableViewController {
    
    private func initObserveProfilePictureLoaded() {
        NSNotificationCenter
            .defaultCenter()
            .rx_notification(UserPlistManager.finishedLoadingProfilePictureNotificationName())
            .takeUntil(self.rx_deallocating)
            .subscribeNext { [weak self] (note) in
                self?.updateAvatarImageView()
            }
            .addDisposableTo(disposeBag)
    }
    
    private func initObserveData() {
        viewModel
            .rx_time
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribeNext { [weak self] (time) in
                self?.updateBigHint()
            }
            .addDisposableTo(disposeBag)
        
        viewModel
            .rx_userContentChange
            .observeOn(MainScheduler.instance)
            .subscribeNext { [weak self] (count) in
                self?.updateBigHint()
                self?.updateSmallHint()
            }
            .addDisposableTo(disposeBag)
    }
    
    private func initUI() {
        view.feeday_roundCorner()
        addPeopleImageView.feeday_roundCorner(7)
        addTagsImageView.feeday_roundCorner(7)
        avatarImageView.feeday_roundCorner(20)
    }
    
    private func updateUI() {
        updateWelcomeLabel()
        updateBigHint()
        updateSmallHint()
        updateAvatarImageView()
        updateGIFs()
    }
    
    private func updateGIFs() {
        let homeBG = PeopleManager.sharedManager.feedPeopleList.count > 0 ? "Home-People-BG2-V2" : "Home-People-BG-V2"
        let tagBG  = TagsManager.sharedManager.tagList.count > 0 ? "Home-Tags-BG2-V2" : "Home-Tags-BG-V2"
        
        if let path = NSBundle.mainBundle().pathForResource(homeBG, ofType: "gif") {
            let gifPeople = FLAnimatedImage(animatedGIFData: NSData(contentsOfFile: path))
            addPeopleImageView.animatedImage = gifPeople
        }
        
        if let path = NSBundle.mainBundle().pathForResource(tagBG, ofType: "gif") {
            let gifTags = FLAnimatedImage(animatedGIFData: NSData(contentsOfFile: path))
            addTagsImageView.animatedImage = gifTags
        }
    }
    
    private func updateBigHint() {
        bigHintLabel.text = viewModel.bigHintContent
    }
    
    private func updateSmallHint() {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.2
        paragraphStyle.alignment = .Left
        
        let smallHintText = NSMutableAttributedString(string: viewModel.smallHintContent)
        smallHintText.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, smallHintText.length))
        smallHintText.addAttribute(NSFontAttributeName, value: FeedayConstant.lightFont(18), range: NSMakeRange(0, smallHintText.length))
        smallHintLabel.attributedText = smallHintText
    }
    
    private func updateWelcomeLabel() {
        welcomeLabel.text = viewModel.welcomeHintContent
    }
    
    private func updateAvatarImageView() {        
        if let data =  UserPlistManager.sharedManager().getProfilePicture() {
            if let avatarImage = UIImage(data: data) {
                avatarImageView.image = avatarImage
            }
        } else {
             UserPlistManager.sharedManager().loadProfilePictureData()
        }
    }
}

// MARK: - Instantiatable

extension HomeTableViewController: Instantiatable {
    
    static func storyboardName() -> String {
        return "Home"
    }
    
    static func storyboardID() -> String {
        return "HomeTableViewController"
    }
}

// MARK: - IBAction

extension HomeTableViewController {
    
    @IBAction func addPeopleButtonPressed(sender: AnyObject) {
        FlurryManager.logEvent(.TapAddPeople)
    }
    
    @IBAction func addHashtagButtonPressed(sender: AnyObject) {
        FlurryManager.logEvent(.TapAddHashtag)
    }
}
