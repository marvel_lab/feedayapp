//
//  HomeContainerViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/6/27.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import MessageUI

import RxSwift
import RxCocoa
import InstagramKit
import FBSDKMessengerShareKit
import SDWebImage

import FeedayUIKit
import FeedayCommonKit

extension HomeContainerViewController: Instantiatable {
    
    static func storyboardName() -> String {
        return "Home"
    }
    
    static func storyboardID() -> String {
        return "HomeContainerViewController"
    }
}

class HomeContainerViewController: UIViewController {
    
    @IBOutlet weak var pageViewWrapper: UIView!
    
    @IBOutlet weak var pageControlWrapper: UIView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    private var pageVC: HomePageViewController?
    
    private var homeVC = HomeTableViewController.instantiateViewController()!
    
    private var settingVC = SettingContainerTableViewController.instantiateViewController()!
    
    private var disposeBag = DisposeBag()
    
    private var documentInteractionController: UIDocumentInteractionController?
    
    private var page: Int = 1 {
        didSet {
            pageControl.currentPage = page
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.zz_scaleInFactorRelativeByHeight667()
        
        initObservingLogout()
        initObserveDeepLink()
        
        loadContent()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        guard checkLoginState() == true else { return }

        if IAPIntroViewController.isWatched() == false && ShareDefaultsManager.sharedManager.isPremium == false {
            IAPIntroViewController.present(overViewController: self)
            return
        }

        RateViewController.presentIfNeed(uponViewController: self)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        let mgr = SDWebImageManager.sharedManager()
        
        mgr.imageCache.clearMemory()
        mgr.imageDownloader.cancelAllDownloads()
    }
    
    private func loadContent() {
        if let vc = HomePageViewController.instantiateViewController() {
            pageVC = vc
            
            addChildViewController(vc)
            pageViewWrapper.addSubview(vc.view)
            vc.view.frame = pageViewWrapper.bounds
            vc.didMoveToParentViewController(self)
            
            vc.setViewControllers([homeVC], direction: UIPageViewControllerNavigationDirection.Reverse, animated: true, completion: nil)
            vc.dataSource = self
            vc.delegate = self
            
            page = 1
        }
    }
    
    private func checkLoginState() -> Bool {
        if !ShareDefaultsManager.sharedManager.isLoggedIn {
            if let vc = StartViewController.instantiateViewController() {
                presentViewController(vc, animated: true, completion: nil)
                return false
            }
        }

        return true
    }
    
    private func initObservingLogout() {
        NSNotificationCenter
            .defaultCenter()
            .rx_notification(UserPlistManager.logoutNotificationName())
            .takeUntil(self.rx_deallocating)
            .subscribeNext { [weak self] (note) in
                self?.checkLoginState()
            }
            .addDisposableTo(disposeBag)
    }
    
    private func initObserveDeepLink() {
        DeepLinkManager.sharedManager.rx_linkType.asObservable()
            .observeOn(MainScheduler.instance)
            .subscribeNext { [weak self] (linkType) in
                guard let this = self else { return }
                DeepLinkManager.sharedManager.clearIntent()
                
                guard ShareDefaultsManager.sharedManager.isLoggedIn == true else {
                    return
                }
                
                switch linkType {
                case .AddPeople:
                    if let vc = AddPeopleViewController.instantiateViewController() {
                        this.presentViewController(vc, animated: true, completion: nil)
                    }
                case .AddFavorite:
                    if let vc = AddPeopleViewController.instantiateViewController() {
                        this.presentViewController(vc, animated: true) {
                            vc.viewModel.state = .Favorite
                        }
                    }
                case .AddHashtag:
                    if let vc = AddTagsViewController.instantiateViewController() {
                        this.presentViewController(vc, animated: true, completion: nil)
                    }
                case .InAppPurchase:
                    // open IAP in settings
                    if this.page != 0 {
                        if let vc = this.pageVC {
                            vc.setViewControllers([this.settingVC], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
                            this.page = 0
                        }
                    }
                    
                    if let vc = IAPPremiumViewController.instantiateViewController() {
                        this.presentViewController(vc, animated: true, completion: nil)
                    }
                case .Setting:
                    if this.page != 0 {
                        if let vc = this.pageVC {
                            vc.setViewControllers([this.settingVC], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
                            this.page = 0
                        }
                    }
                case .Home:
                    if this.page != 1 {
                        if let vc = this.pageVC {
                            vc.setViewControllers([this.homeVC], direction: UIPageViewControllerNavigationDirection.Reverse, animated: true, completion: nil)
                            this.page = 1
                        }
                    }
                case .SendPhoto:
                    ShareDefaultsManager.sharedManager
                        .rx_loadSendPhoto()
                        .subscribeNext { (media: InstagramMedia, image: UIImage, type: FeedayConstant.SendPhotoType) in
                            let photoInfo = (media: media, image: image, type: type)
                            switch type {
                            case .Email:
                                this.sendPhotoViaEmail(photoInfo)
                            case .Message:
                                this.sendPhotoViaMessage(photoInfo)
                            case .WhatsApp:
                                this.sendPhotoViaWhatsApp(photoInfo)
                            case .Messenger:
                                this.sendPhotoViaMessenger(photoInfo)
                            }
                        }
                        .addDisposableTo(this.disposeBag)
                case .Media:
                    let id = DeepLinkManager.sharedManager.paramsDict["id"] as? String ?? ""
                    let short_code = DeepLinkManager.sharedManager.paramsDict["short_code"] as? String ?? ""
                    
                    // Instagram installed
                    if UIApplication.sharedApplication().canOpenURL(NSURL.init(string: "instagram://app")!) {
                        let urlString = "instagram://media?id=\(id)"
                        if let url = NSURL(string: urlString) {
                            dispatch_async(dispatch_get_main_queue()) {
                                UIApplication.sharedApplication().openURL(url)
                            }
                        }
                        return
                    }
                    
                    // Instagram not installed
                    guard let url = NSURL(string: "https://www.instagram.com/p/\(short_code)") else { return }
                    dispatch_async(dispatch_get_main_queue()) {
                        UIApplication.sharedApplication().openURL(url)
                    }
                case .IMessageTutorial:
                    if let vc =
                        UIStoryboard(name: "Main", bundle: nil)
                            .instantiateViewControllerWithIdentifier("VideoViewController") as? VideoViewController {
                        vc.forMessage = true
                        this.presentViewController(vc, animated: true, completion: nil)
                    }
                    
                case .NotificationCenterTutorial:
                    if let vc =
                        UIStoryboard(name: "Main", bundle: nil)
                            .instantiateViewControllerWithIdentifier("VideoViewController") as? VideoViewController {
                        vc.forWidget = true
                        this.presentViewController(vc, animated: true, completion: nil)
                    }
                default:
                    break
                }
            }
            .addDisposableTo(disposeBag)
    }
    
    private func updateDeepLink() {
        DeepLinkManager.sharedManager.checkAndFireIntent()
    }
    
    private func sendPhotoViaEmail(photoInfo: PhotoInfo) {
        let vc = MFMailComposeViewController()
        vc.mailComposeDelegate = self
        vc.addAttachmentData(UIImageJPEGRepresentation(photoInfo.image, CGFloat(1.0))!, mimeType: "image/jpeg", fileName:  "photo.jpeg")
        vc.setSubject("Here's a photo I want share with you...")
        vc.setMessageBody("<html><body><p>\(photoInfo.media.sendPhotoTextLong)</p></body></html>", isHTML: true)
        presentViewController(vc, animated: true, completion: nil)
    }
    
    private func sendPhotoViaMessage(photoInfo: PhotoInfo) {
        
        let vc = MFMessageComposeViewController()
        vc.messageComposeDelegate = self
        
        if MFMessageComposeViewController.canSendText() {
            vc.body = photoInfo.media.sendPhotoTextLong
            
            if MFMessageComposeViewController.canSendAttachments() {
                vc.addAttachmentData(
                    UIImageJPEGRepresentation(photoInfo.image, CGFloat(1.0))!,
                    typeIdentifier: "public.jpeg",
                    filename:  "photo.jpeg"
                )
            }
        }
        
        presentViewController(vc, animated: true, completion: nil)
    }
    
    private func sendPhotoViaWhatsApp(photoInfo: PhotoInfo) {
        let text = photoInfo.media.sendPhotoTextShort
        let msg = "\(photoInfo.media.link), \(text)"
        let urlWhats = "whatsapp://send?text=\(msg)"
        
        guard let urlString = urlWhats.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet()) else { return }
        guard let url = NSURL.init(string: urlString) else { return }
        if UIApplication.sharedApplication().canOpenURL(url) {
            UIApplication.sharedApplication().fastOpenURL(url)
        } else {
            FeedayAlertHelper.showAlert(inViewController: self, message: "Seems you don't have this app installed")
        }
    }
    
    private func sendPhotoViaMessenger(photoInfo: PhotoInfo) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.001 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) {
            FBSDKMessengerSharer.shareImage(photoInfo.image, withOptions: nil)
        }
    }
}

extension HomeContainerViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if let vc = pageViewController.viewControllers?.first {
            if vc is HomeTableViewController {
                page = 1
            }
            if vc is SettingContainerTableViewController {
                page = 0
            }
        }
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        if viewController is SettingContainerTableViewController {
            return homeVC
        }
        
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        if viewController is HomeTableViewController {
            return settingVC
        }
        
        return nil
    }
}


extension HomeContainerViewController: UINavigationControllerDelegate {
    
}

extension HomeContainerViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}

extension HomeContainerViewController: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}
