//
//  HomePageViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/6/27.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit

import FeedayUIKit
import FeedayCommonKit

class HomePageViewController: UIPageViewController {}

extension HomePageViewController: Instantiatable {
    
    static func storyboardName() -> String {
        return "Home"
    }
    
    static func storyboardID() -> String {
        return "HomePageViewController"
    }
}