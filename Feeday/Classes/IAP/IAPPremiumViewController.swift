//
//  IAPPremiumViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/4/9.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import SVProgressHUD
import FLAnimatedImage
import FeedayCommonKit
import FeedayUIKit

class IAPPremiumViewController: UIViewController {

    // MARK: - IBOutlet
    @IBOutlet weak var gifImageView: FLAnimatedImageView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var buyButton: UIButton!

    @IBOutlet weak var originalPriceBoard: UIView!
    @IBOutlet weak var originalPriceLabel: UILabel!

    // MARK: - Static
    static func instantiateViewController() -> IAPPremiumViewController? {
        let storyboard = UIStoryboard(name: "InAppPurchase", bundle: nil)
        let identifier = String(IAPPremiumViewController.self)
        return storyboard.instantiateViewControllerWithIdentifier(identifier) as? IAPPremiumViewController
    }

    // MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()

        view.feeday_roundCorner()
        setupPrice()
        setupTopGIF()
        setupScale()
    }

    // MARK: - IBAction
    @IBAction func cancelButtonPressed(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func buyButtonPressed(sender: UIButton) {
        FlurryManager.logEvent(FlurryManager.FlurryEvent.TapBuyPremiumbutton)

        self.dismissViewControllerAnimated(true) { () -> Void in
            SVProgressHUD.show()
            IAPManager
                .sharedManager()
                .buyPremium { (err) in
                    SVProgressHUD.dismiss()

                    if err != nil {
                        let alertView = UIAlertView(title: "", message: "Buy Premium Failed", delegate: nil, cancelButtonTitle: "OK")
                        alertView.show()
                    }
            }
        }
    }

    // MARK: - Private
    private func setupTopGIF() {
        if let url = NSBundle.mainBundle().URLForResource("IAP-Top", withExtension: "gif"),
            data = NSData(contentsOfURL: url),
            image = FLAnimatedImage(animatedGIFData: data) {
            gifImageView.animatedImage = image
        }
    }

    private func setupScale() {
        view.zz_scaleInFactorRelativeByHeight667()
    }

    private func setupPrice() {
        let baseText = "Buy Premium"
        buyButton.setTitle(baseText, forState: .Normal)

        originalPriceBoard.alpha = 0

        IAPManager.sharedManager().fetchPrimiumPriceDetails { [weak self] (currentPrice, originalPrice, discount, error) in
            guard let `self` = self else { return }

            self.buyButton.setTitle(baseText + " " + currentPrice, forState: .Normal)

            self.originalPriceLabel.text = originalPrice
            self.originalPriceBoard.alpha = 1
        }
    }
}
