import UIKit
import RxSwift
import SVProgressHUD
import FLAnimatedImage
import FeedayCommonKit
import FeedayUIKit

private let PLIST_FILE_NAME = "IAPIntroViewControllerMark.plist"
private let PLIST_KEY       = "watched"

class IAPIntroViewController: UIViewController {

    // MARK: - IBOutlet
    @IBOutlet weak var gifImageView: FLAnimatedImageView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var priceBoard: UIView!
    @IBOutlet weak var priceBoardWidthConstraint: NSLayoutConstraint!

    @IBOutlet weak var originalPriceBoard: UIView!
    @IBOutlet weak var originalPriceLabel: UILabel!

    @IBOutlet weak var discountBoard: UIView!
    @IBOutlet weak var discountLabel: UILabel!

    // MARK: - Static
    static func instantiateViewController() -> IAPIntroViewController? {
        let storyboard = UIStoryboard(name: "InAppPurchase", bundle: nil)
        let identifier = String(IAPIntroViewController.self)
        return storyboard.instantiateViewControllerWithIdentifier(identifier) as? IAPIntroViewController
    }

    static func present(overViewController baseViewController: UIViewController) {
        if let vc = instantiateViewController() {
            baseViewController.presentViewController(vc, animated: true) {
                markWatched()
            }
        }

        return
    }

    static func isWatched() -> Bool {
        if let url = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first,
            let fileURL = url.URLByAppendingPathComponent(PLIST_FILE_NAME),
            let data = NSData(contentsOfURL: fileURL),
            let dict = NSKeyedUnarchiver.unarchiveObjectWithData(data),
            let watched = dict[PLIST_KEY] as? Bool {
            return watched
        }
        return false
    }

    static func markWatched() {
        let dict = [PLIST_KEY: true]
        let data = NSKeyedArchiver.archivedDataWithRootObject(dict)
        if let url = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first,
            let fileURL = url.URLByAppendingPathComponent(PLIST_FILE_NAME) {
            do {
                try data.writeToURL(fileURL, options: NSDataWritingOptions.AtomicWrite)
                print("IAPIntroViewController::markWatched OK")
            } catch {
                print("IAPIntroViewController::markWatched Error")
            }
        }
    }

    // MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()

        view.feeday_roundCorner()

        setupPrice()
        setupTopGIF()
        setupScale()
    }

    // MARK: - IBAction
    @IBAction func cancelButtonPressed(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func buyButtonPressed(sender: UIButton) {
        FlurryManager.logEvent(FlurryManager.FlurryEvent.TapBuyPremiumbutton)

        self.dismissViewControllerAnimated(true) { () -> Void in
            SVProgressHUD.show()
            IAPManager
                .sharedManager()
                .buyPremium { (err) in
                    SVProgressHUD.dismiss()

                    if err != nil {
                        let alertView = UIAlertView(title: "", message: "Buy Premium Failed", delegate: nil, cancelButtonTitle: "OK")
                        alertView.show()
                    }
            }
        }
    }

    // MARK: - Private
    private func setupTopGIF() {
        if let url = NSBundle.mainBundle().URLForResource("IAP-Top", withExtension: "gif"),
            data = NSData(contentsOfURL: url),
            image = FLAnimatedImage(animatedGIFData: data) {
            gifImageView.animatedImage = image
        }
    }

    private func setupScale() {
        view.zz_scaleInFactorRelativeByHeight667()
    }

    private func setupPrice() {
        priceBoard.feeday_roundCorner(4)
        priceLabel.text = ""

        originalPriceBoard.alpha = 0
        discountBoard.alpha = 0

        IAPManager.sharedManager().fetchPrimiumPriceDetails { [weak self] (currentPrice, originalPrice, discount, error) in
            guard let `self` = self else { return }

            self.priceLabel.text = currentPrice
            self.priceLabel.sizeToFit()
            self.priceBoardWidthConstraint.constant = self.priceLabel.bounds.width + 36

            self.originalPriceLabel.text = originalPrice
            self.originalPriceBoard.alpha = 1

            self.discountLabel.text = String.init(format: "*Save %@. This is a limited-time offer.", discount)
            self.discountBoard.alpha = 1
        }
    }
}
