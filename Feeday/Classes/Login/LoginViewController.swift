//
//  LoginViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/3.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import InstagramKit
import RxSwift
import RxCocoa
import FeedayCommonKit
import FeedayUIKit
import Flurry_iOS_SDK

class LoginViewController: UIViewController, Rxable {
    
    let userPlistManager = UserPlistManager.sharedManager()
    
    var viewModel = LoginViewModel()
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSLog("%@","LoginViewController::viewDidLoad")
        
        view.zz_scaleInFactorRelativeByHeight667()
        
        initWebView()
        initNavigationBar()
    }
    
    private func authenticationSuccess() {
        NSLog("%@","LoginViewController::authenticationSuccess")
        
        InstagramManager.sharedManager.storeCookieData()
        
        let engine = InstagramEngine.sharedEngine()
        engine.getSelfUserDetailsWithSuccess({ [weak self] (user) in
            guard let this = self else { return }
            
            if let token = engine.accessToken {
                UserPlistManager.sharedManager().saveUserData(user)
                
                let mgr = ShareDefaultsManager.sharedManager
                mgr.accessToken = token
                mgr.isLoggedIn = true
                mgr.currentUserId = user.Id

                Flurry.setUserID(user.Id)
                
                this.dismissViewControllerAnimated(true, completion: nil)
                this.viewModel.didLogin()
            }
            })
        { [weak self] (error, code) in
            guard let this = self else { return }
            
            let breif = "iOS " + UIDevice.currentDevice().systemVersion + ">>" + String(#line) + ":" + error.localizedDescription
            NSLog("%@","LoginViewController::loginError::Start >>")
            NSLog("%@", breif)
            NSLog("%@", error)
            for line in NSThread.callStackSymbols() {
                NSLog("%@", line)
            }
            dump(error)
            dump(NSThread.callStackSymbols())
            NSLog("%@","LoginViewController::loginError::End >>")
            
            FeedayAlertHelper.showAlert(
                inViewController: this,
                message: breif
            )
        }
    }
    
    private func initNavigationBar() {
        let attr = [
            NSFontAttributeName: FeedayConstant.regularFont(17)
        ]
        
        navigationController?.navigationBar.titleTextAttributes = attr
        
        navigationItem.rightBarButtonItem?.title = "Cancel"
        navigationItem.rightBarButtonItem?.setTitleTextAttributes(attr, forState: .Normal)
        navigationItem.rightBarButtonItem?
            .rx_tap
            .subscribeNext { [weak self] (_) in
                self?.dismissViewControllerAnimated(true, completion: nil)
            }
            .addDisposableTo(disposeBag)
        
        if let bgView = navigationController?.navigationBar {
            let gLayer = CAGradientLayer.init()
            gLayer.frame = bgView.layer.bounds
            gLayer.colors = [UIColor.f2StrongPinkColor().CGColor, UIColor.f2OrangeRedColor().CGColor]
            gLayer.locations = [0, 1]
            gLayer.startPoint = CGPointMake(0, 0.5)
            gLayer.endPoint = CGPointMake(1, 0.5)
            gLayer.cornerRadius = bgView.layer.cornerRadius
            bgView.layer.insertSublayer(gLayer, atIndex: 1)
            
            bgView
                .rx_observe(CGRect.self, "bounds")
                .subscribeNext { (rect) in
                    guard let rect = rect else { return }
                    bgView.layer.frame = rect
                    gLayer.frame = rect
                }
                .addDisposableTo(disposeBag)
        }
    }
    
    private func initWebView() {
        
        NSURLCache.sharedURLCache().removeAllCachedResponses()
        if let cookies = NSHTTPCookieStorage.sharedHTTPCookieStorage().cookies {
            for cookie in cookies {
                NSHTTPCookieStorage.sharedHTTPCookieStorage().deleteCookie(cookie)
            }
        }
        
        webView.scrollView.contentInset  = UIEdgeInsetsMake(-45, 0, 0, 0)
        webView.scrollView.scrollEnabled = false
        webView.delegate                 = self
        
        webView.loadRequest(NSURLRequest(URL: viewModel.authURL))
    }
}

extension LoginViewController: UIWebViewDelegate {
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        let engine = InstagramEngine.sharedEngine()
        if let url = request.URL {
            // do {
                if url.scheme == viewModel.callbackURL.scheme &&
                    url.host == viewModel.callbackURL.host {
                    
                    NSLog("Login Callback URL: %@", url.absoluteString ?? "")
                    
                    try! engine.receivedValidAccessTokenFromURL(url)
                    authenticationSuccess()
                    return false
                }
//            } catch let error as NSError {
//                let breif = "iOS " + UIDevice.currentDevice().systemVersion + ">>" + String(#line) + ":" + error.localizedDescription
//                NSLog("%@","LoginViewController::loginError::Start >>")
//                NSLog("%@", breif)
//                NSLog("%@", error)
//                for line in NSThread.callStackSymbols() {
//                    NSLog("%@", line)
//                }
//                dump(error)
//                dump(NSThread.callStackSymbols())
//                NSLog("%@","LoginViewController::loginError::End >>")
//                
//                FeedayAlertHelper.showAlert(
//                    inViewController: self,
//                    message: breif
//                )
//            }
        }
        
        return true
    }
}
