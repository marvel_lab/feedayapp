//
//  LoginViewModel.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/3.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit

import RxSwift

class LoginViewModel: NSObject {
    
    let clientID = "90f63f3240ff470bb580ca42bcda4c77"
    let callbackURLPath = "http://www.feedayapp.com"
    
    var callbackURL: NSURL {
        return NSURL.init(string: callbackURLPath)!
    }
    
    var authURL: NSURL {
        let path = "https://api.instagram.com/oauth/authorize/?client_id=\(clientID)&redirect_uri=\(callbackURLPath)&response_type=token&scope=public_content"
        return NSURL.init(string: path)!
    }
    
    var followURL: NSURL {
        return NSURL.init(string: "http://instagram.com/feedayapp")!
    }
    
    var isLoggedIn: Variable<Bool> = Variable(false)
    var isReadAfterLogin: Variable<Bool> = Variable(false)
    
    func didLogin() {
        isLoggedIn.value = true
    }
    
    func didReadAfterLogin() {
        isReadAfterLogin.value = true
    }
}
