//
//  LoginNavigationController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/26.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import FeedayUIKit

class LoginNavigationController: UINavigationController, UINavigationControllerDelegate {
    
    var viewModel = LoginViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
    }
    
    func navigationController(navigationController: UINavigationController, willShowViewController viewController: UIViewController, animated: Bool) {
        if let vc = viewController as? LoginViewController {
            vc.viewModel = viewModel
        }
    }
}

extension LoginNavigationController: Instantiatable {
    
    static func storyboardID() -> String {
        return "LoginNavigationController"
    }
    
    static func storyboardName() -> String {
        return "Login"
    }
    
}
