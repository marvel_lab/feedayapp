//
//  AfterLoginViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/26.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit

import MBProgressHUD
import RxSwift

import FeedayUIKit
import FeedayCommonKit

class AfterLoginViewController: UIViewController, Rxable {
    
    @IBOutlet weak var title1Label: UILabel!
    @IBOutlet weak var title2Label: UILabel!
    @IBOutlet weak var webview: UIWebView!
    @IBOutlet weak var mainButton: FeedayRoundButton!
    
    var viewModel = LoginViewModel()
    
    private var isLoadedOnce: Bool = false
}

// MARK: - Life Cycle

extension AfterLoginViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        initWebView()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        webview.delegate = self
        updateTitle()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        webview.delegate = nil
    }
}

// MARK: - IBAction

extension AfterLoginViewController {
    
    @IBAction func mainButtonPressed(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
        viewModel.didReadAfterLogin()
    }
}

// MARK: - Private

extension AfterLoginViewController {
    
    private func updateTitle() {
        let title1 = "Welcome\non feeday."
        let title2 = "Not miss a beat\nfollow us!"
        
        title1Label.emphasizeWord("feeday", inText: title1, withFont: FeedayConstant.boldFont(32))
        title2Label.emphasizeWord("follow us!", inText: title2, withFont: FeedayConstant.boldFont(32))
        
        title1Label.setLineHeight(40)
        title2Label.setLineHeight(40)
    }
    
    private func initWebView() {
        webview.scrollView.scrollEnabled = false
        webview.delegate = self
        
        webview.loadRequest(NSURLRequest(URL: viewModel.followURL))
    }
    
    private func initView() {
        view.zz_scaleInFactorRelativeByHeight667()
        view.feeday_roundCorner()
    }
}

private extension UILabel {
    
    func emphasizeWord(word: String, inText txt: String, withFont font: UIFont) {
        let str = NSMutableAttributedString.init(string: txt)
        let range = (txt as NSString).rangeOfString(word)
        str.addAttribute(NSFontAttributeName, value: font, range: range)
        attributedText = str
    }
    
    func setLineHeight(height: CGFloat) {
        guard let attrStr = attributedText else { return }
        
        let str = NSMutableAttributedString.init(attributedString: attrStr)
        
        let pStyle = NSMutableParagraphStyle()
        
        let txt = attrStr.string
        let range = (txt as NSString).rangeOfString(txt)
        
        pStyle.minimumLineHeight = height
        pStyle.maximumLineHeight = height
        
        pStyle.alignment = .Center
        
        str.addAttribute(NSParagraphStyleAttributeName, value: pStyle, range: range)
        
        attributedText = str
    }
}


// MARK: - UIWebViewDelegate

extension AfterLoginViewController: UIWebViewDelegate {
    
    func webViewDidStartLoad(webView: UIWebView) {
        guard isLoadedOnce == false else { return }
        
        MBProgressHUD.showHUDAddedTo(webview, animated: true)
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        isLoadedOnce = true
        
        MBProgressHUD.hideAllHUDsForView(webview, animated: true)
        
        webview.scrollView.contentInset  = UIEdgeInsetsMake(-45, 0, 0, 0)
        
        // remove links
        if let path = NSBundle.mainBundle().pathForResource("RemoveLinks", ofType: "js") {
            if let js = try? String.init(contentsOfFile: path) {
                webView.stringByEvaluatingJavaScriptFromString(js)
            }
        }
        
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
        MBProgressHUD.hideAllHUDsForView(webview, animated: true)
    }
}

// MARK: - Instantiatable

extension AfterLoginViewController: Instantiatable {
    static func storyboardName() -> String {
        return "Login"
    }
    
    static func storyboardID() -> String {
        return "AfterLoginViewController"
    }
}
