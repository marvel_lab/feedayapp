//
//  StartViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/3.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import RxSwift

import FeedayUIKit
import FeedayCommonKit

class StartViewController: UIViewController, Rxable {
    
    static let kShowLoginSegueID = "SHOW_LOGIN"
    
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet weak var loginButton: UIButton!
    
    var viewModel = LoginViewModel()
}

// MARK: - Life Cycle

extension StartViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        initObserveLogin()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        updateTitleLabel()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if DeepLinkManager.sharedManager.rx_linkType.value == .Login {
            presentLoginViewController()
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let vc = segue.destinationViewController as? LoginNavigationController {
            vc.viewModel = viewModel
        }
    }
    
    func presentLoginViewController() {
        if let vc = LoginNavigationController.instantiateViewController() {
            vc.viewModel = viewModel
            presentViewController(vc, animated: true, completion: {
                DeepLinkManager.sharedManager.rx_linkType.value = .App
            })
        }
    }
}

// MARK: - Private

extension StartViewController {
    
    private func initView() {
        view.zz_scaleInFactorRelativeByHeight667()
        view.feeday_roundCorner()
    }
    
    private func updateTitleLabel() {
        let txt = "Customize your\nfeed experience"
        let str = NSMutableAttributedString.init(string: txt)
        let range = (txt as NSString).rangeOfString("experience")
        str.addAttribute(NSFontAttributeName, value: FeedayConstant.semiBoldFont(32), range: range)
        titleLabel.attributedText = str
    }
    
    private func initObserveLogin() {
        viewModel.isLoggedIn.asObservable()
            .observeOn(MainScheduler.instance)
            .filter { (flag) -> Bool in
                return flag == true
            }
            .subscribeNext { [weak self] (flag) in
                guard let this = self else { return }
                
                // hide UI
                let blockView = UIView.init(frame: this.view.bounds)
                blockView.backgroundColor = UIColor.whiteColor()
                this.view.addSubview(blockView)
                
                if let vc = AfterLoginViewController.instantiateViewController() {
                    vc.viewModel = this.viewModel
                    this.presentViewController(vc, animated: true, completion: nil)
                }
                
            }
            .addDisposableTo(disposeBag)
        
        viewModel.isReadAfterLogin.asObservable()
            .observeOn(MainScheduler.instance)
            .filter { (flag) -> Bool in
                return flag == true
            }
            .subscribeNext { [weak self] (flag) in
                guard let this = self else { return }
                
                // hide UI
                let blockView = UIView.init(frame: this.view.bounds)
                blockView.backgroundColor = UIColor.blackColor()
                this.view.addSubview(blockView)
                
                this.dismissViewControllerAnimated(false, completion: nil)
            }
            .addDisposableTo(disposeBag)
        
        DeepLinkManager.sharedManager.rx_linkType.asObservable()
            .observeOn(MainScheduler.instance)
            .filter { (linkType) -> Bool in
                return linkType == .Login
            }
            .subscribeNext { [weak self] (_) in
                self?.presentLoginViewController()
            }
            .addDisposableTo(disposeBag)
    }
}

// MARK: - Instantiatable

extension StartViewController: Instantiatable {
    
    static func storyboardName() -> String {
        return "Login"
    }
    
    static func storyboardID() -> String {
        return "StartViewController"
    }
}
