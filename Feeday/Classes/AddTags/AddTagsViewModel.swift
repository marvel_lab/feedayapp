//
//  AddTagsViewModel.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/11.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import RxSwift
import InstagramKit
import FeedayCommonKit

class AddTagsViewModel: AddContentViewModel {
    
    // MARK: - Search
    
    var searchTagsList = [InstagramTag]() {
        didSet {
            _serachTagsSubject.onNext(searchTagsList)
        }
    }
    
    var searchTagsSignal: Observable<[InstagramTag]> {
        return _serachTagsSubject.asObservable()
    }
    
    private var _serachTagsSubject = PublishSubject<[InstagramTag]>()
    
    func rx_searchTags(searchText: String = "") -> Observable<[InstagramTag]> {
        let request = SearchTagsRequest()
        let signal = request
            .rx_request(searchText)
            .doOnNext { (tags) in
                self.searchTagsList = tags
        }
        return signal
    }
}