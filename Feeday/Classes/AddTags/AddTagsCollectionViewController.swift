//
//  AddPeopleCollectionViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/10.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import RxSwift
import FeedayUIKit
import FeedayCommonKit
import InstagramKit

class AddTagsCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, RAReorderableLayoutDelegate {
    
    var viewModel = AddTagsViewModel()
    
    private var disposeBag = DisposeBag()
    
    private var tagsManager = TagsManager.sharedManager
    
    private var sizingCell: AddTagsCollectionViewCell?
    
    private var cellID = "AddTagsCollectionViewCell"
    
    @IBOutlet weak var collectionLayout: UICollectionViewFlowLayout!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cellNib = UINib(nibName: cellID, bundle: nil)
        collectionView?.registerNib(cellNib, forCellWithReuseIdentifier: cellID)
        sizingCell = cellNib.instantiateWithOwner(nil, options: nil).first as? AddTagsCollectionViewCell
        
        tagsManager
            .tagListSignal
            .subscribeNext { [weak self] (_) in
                self?.collectionView?.reloadData()
            }
            .addDisposableTo(disposeBag)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        collectionView?.reloadData()
    }
    
    // MARK: UICollectionViewDataSource
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tagsManager.tagList.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellID, forIndexPath: indexPath) as! AddTagsCollectionViewCell
        
        let row = indexPath.row
        if row < tagsManager.tagList.count {
            let model = tagsManager.tagList[row]
            cell.model = model
        }
        
        return cell
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        collectionView.deselectItemAtIndexPath(indexPath, animated: true)
        
        let row = indexPath.row
        guard row < tagsManager.tagList.count else { return }
        
        let tag = tagsManager.tagList[row]
        
        let sheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        // Add "Move Up" option if not 1st item
        if row != 0 {
            sheet.addAction(
                UIAlertAction(title: "Move Up", style: UIAlertActionStyle.Default) { [weak self] action -> () in
                    guard let this = self else { return }
                    let currentIndex = indexPath.row
                    let toIndex = currentIndex - 1
                    
                    if toIndex < 0 {
                        return
                    }
                    
                    this.tagsManager.moveTag(tag, toIndex: toIndex)
                }
            )
        }
        
        // Add "Move Down" option if not last item
        if row != tagsManager.tagList.count - 1 {
            sheet.addAction(
                UIAlertAction(title: "Move Down", style: UIAlertActionStyle.Default) { [weak self] action -> () in
                    guard let this = self else { return }
                    let currentIndex = indexPath.row
                    let toIndex = currentIndex + 1
                    
                    if toIndex >= this.tagsManager.tagList.count {
                        return
                    }
                    
                    this.tagsManager.moveTag(tag, toIndex: toIndex)
                }
            )
        }
        
        sheet.addAction(
            UIAlertAction(title: "Remove", style: UIAlertActionStyle.Default) { [weak self] action -> () in
                self?.tagsManager.removeTag(tag)
            }
        )
        
        sheet.addAction(
            UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) { action -> () in
                
            }
        )
        
        sheet.popoverPresentationController?.sourceView = self.view
        sheet.popoverPresentationController?.sourceRect = collectionView.cellForItemAtIndexPath(indexPath)?.frame ?? CGRectZero
        
        sheet.view.tintColor = UIColor(red: 255/255.0, green: 19/255.0, blue: 83/255.0, alpha: 1)
        self.presentViewController(sheet, animated: true, completion: nil)
        sheet.view.tintColor = UIColor(red: 255/255.0, green: 19/255.0, blue: 83/255.0, alpha: 1)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if let cell = sizingCell {
            let row = indexPath.row
            if row < tagsManager.tagList.count {
                let tag = tagsManager.tagList[row]
                cell.model = tag
                return cell.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)
            }
        }
        
        return CGSizeZero
    }
    
    func collectionView(collectionView: UICollectionView, atIndexPath: NSIndexPath, didMoveToIndexPath toIndexPath: NSIndexPath) {
        guard atIndexPath.row < tagsManager.tagList.count else { return }
        let tag = tagsManager.tagList[atIndexPath.row]
        tagsManager.moveTag(tag, toIndex: toIndexPath.row)
    }

}

extension AddTagsCollectionViewController: Instantiatable {
    
    static func storyboardName() -> String {
        return "AddFollowContent"
    }
    
    static func storyboardID() -> String {
        return "AddTagsCollectionViewController"
    }
}
