//
//  AddTagsCollectionViewCell.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/10.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import FeedayUIKit
import InstagramKit
import FeedayCommonKit
import RxSwift

class AddTagsCollectionViewCell: UICollectionViewCell {
    
    enum Mode: Int {
        case Unknown = 0
        case FeaturedList = 1
    }
    
    var model = InstagramTag() {
        didSet {
            updateUI()
        }
    }
    
    var mode: Mode = .Unknown
    
    @IBOutlet weak var tagLabel: UILabel!
    @IBOutlet weak var blockView: UIView!
    @IBOutlet weak var bgView: UIView!
    
    private var gradientLayer: CAGradientLayer?
    
    private var disposeBag = DisposeBag()
    
    private func updateUI() {
        tagLabel.text = "#" + model.name ?? ""
        
        switch mode {
        case .FeaturedList:
            let flag = model.isCollected()            
            blockView.hidden = flag
            bgView.hidden = !flag
        default:
            blockView.hidden = true
            bgView.hidden = false
            break
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let gLayer = CAGradientLayer.init()
        gLayer.frame = bgView.layer.bounds
        gLayer.colors = [UIColor.f2StrongPinkColor().CGColor, UIColor.f2OrangeRedColor().CGColor]
        gLayer.locations = [0, 1]
        gLayer.startPoint = CGPointMake(0, 0.5)
        gLayer.endPoint = CGPointMake(1, 0.5)
        gLayer.cornerRadius = bgView.layer.cornerRadius
        bgView.layer.addSublayer(gLayer)
        self.gradientLayer = gLayer
        
        bgView
            .rx_observe(CGRect.self, "bounds")
            .subscribeNext { [weak self] (rect) in
                guard let this = self else { return }
                guard let rect = rect else { return }
                this.bgView.layer.frame = rect
                this.gradientLayer?.frame = rect
            }
            .addDisposableTo(disposeBag)
    }
}
