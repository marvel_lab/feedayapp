//
//  AddTagsViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/11.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import RxSwift
import MBProgressHUD

import FeedayUIKit
import FeedayCommonKit

class AddTagsViewController: UIViewController {

    @IBOutlet weak var featuredContainer: UIView!
    @IBOutlet weak var mainContainer: UIView!
    @IBOutlet weak var listContainer: UIView!

    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var searchTitleView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchBarTrailing: NSLayoutConstraint!
    @IBOutlet weak var cancelSearchButton: UIButton!

    @IBOutlet weak var topLeftButton: UIButton!

    @IBOutlet weak var emptyHintLabel: UILabel!
    @IBOutlet weak var whiteShadow: UIImageView!

    @IBOutlet weak var toastLabel: UILabel!
    @IBOutlet weak var toastLabelHeight: NSLayoutConstraint!

    var viewModel = AddTagsViewModel()

    private var featuredVC: AddTagsFeaturedCollectionViewController?
    
    private var listVC: AddTagsCollectionViewController?
    
    private weak var searchtVC: SearchTagsTableViewController?
    
    private let disposeBag = DisposeBag()
    
    private let tagsManager = TagsManager.sharedManager
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.zz_scaleInFactorRelativeByHeight667()
        view.feeday_roundCorner()
        
        initSearch()
        initEmptyHint()
        initTopLeftButton()
        
        initObservePremiumIntent()
        observeNewContentAdded()
        
        initListVC()
        initFeaturedVC()
    }
    
    @IBAction func doneButtonPressed(sender: AnyObject) {
        let hud = SavingPreferenceHUDViewController.instantiateViewController()
        
        if let hud = hud {
            addChildViewController(hud)
            view.addSubview(hud.view)
            hud.view.frame = view.bounds
            hud.didMoveToParentViewController(self)
        }
        
        TagsManager.sharedManager.rx_saveTags()
            .observeOn(MainScheduler.instance)
            .subscribe { [weak self] (event) in
                hud?.view.removeFromSuperview()
                hud?.removeFromParentViewController()
                self?.dismissViewControllerAnimated(true, completion: nil)
            }
            .addDisposableTo(disposeBag)
    }
    
    @IBAction func searchBarReturnPressed(sender: UITextField) {
        searchTextField.resignFirstResponder()
        
        MBProgressHUD.showHUDAddedTo(view, animated: true)
        
        viewModel
            .rx_searchTags(sender.text ?? "")
            .doOn({ [weak self] (event) in
                guard let this = self else { return }
                MBProgressHUD.hideHUDForView(this.view, animated: true)
                })
            .subscribe()
            .addDisposableTo(disposeBag)
    }
    
    @IBAction func searchBarBeginEditing(sender: UITextField) {
        viewModel.enterSearch()
    }
    
    @IBAction func cancelSearchButtonPressed(sender: AnyObject) {
        viewModel.exitSearch()
    }
    
    private func initListVC() {
        updateListContainer()
        updateWhiteShadow()
        
        tagsManager
            .tagListSignal
            .subscribeNext { [weak self] (_) in
                self?.updateListContainer()
                self?.updateWhiteShadow()
            }
            .addDisposableTo(disposeBag)
        
        listVC = AddTagsCollectionViewController.instantiateViewController()
        if let vc = listVC {
            vc.viewModel = viewModel
            addChildViewController(vc)
            listContainer.addSubview(vc.view)
            vc.view.frame = listContainer.bounds
            vc.didMoveToParentViewController(self)
        }
    }
    
    private func updateListContainer() {
        listContainer.hidden = (tagsManager.tagList.count == 0)
    }
    
    private func initObservePremiumIntent() {
        TagsManager.sharedManager
            .rx_intentToDoPremiumThings
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribeNext { [weak self] (flag) in
                guard flag == true else { return }
                
                TagsManager.sharedManager.rx_intentToDoPremiumThings.value = false
                
                if let vc = IAPPremiumViewController.instantiateViewController() {
                    self?.presentViewController(vc, animated: true, completion: nil)
                }
            }
            .addDisposableTo(disposeBag)
    }
    
    private func initFeaturedVC() {
        featuredVC = AddTagsFeaturedCollectionViewController.instantiateViewController()
        if let vc = featuredVC {
            vc.viewModel = viewModel
            addChildViewController(vc)
            featuredContainer.addSubview(vc.view)
            vc.view.frame = featuredContainer.bounds
            vc.didMoveToParentViewController(self)
            
            vc.rx_collectionViewWidth
                .asObservable()
                .observeOn(MainScheduler.instance)
                .subscribeNext { [weak self] (w) in
                    if let scrollView = self?.featuredContainer as? UIScrollView {
                        scrollView.contentSize.width = w + 1
                    }
                }
                .addDisposableTo(disposeBag)
        }
    }
    
    private func loadSearchVC() {
        unloadSearchVC()
        
        if let vc = SearchTagsTableViewController.instantiateViewController() {
            searchtVC = vc
            vc.viewModel = viewModel
            addChildViewController(vc)
            mainContainer.addSubview(vc.view)
            vc.view.frame = mainContainer.bounds
            vc.didMoveToParentViewController(self)
        }
    }
    
    private func unloadSearchVC() {
        searchtVC?.view.removeFromSuperview()
        searchtVC?.removeFromParentViewController()
    }
    
    private func initSearch() {
        let constantSearchTrailingCompact: CGFloat = 86
        let constantSearchTrailingFull: CGFloat = 16
        let duration = 0.3
        
        titleView.hidden = false
        searchTitleView.hidden = true
        
        cancelSearchButton.hidden = true
        searchBarTrailing.constant = constantSearchTrailingFull
        
        viewModel.enterSearchSignal.subscribeNext { [weak self] (_) in
            self?.titleView.hidden = true
            self?.searchTitleView.hidden = false
            self?.cancelSearchButton.hidden = false
            self?.loadSearchVC()
            
            self?.view.layoutIfNeeded()
            UIView.animateWithDuration(duration, animations: {
                self?.searchBarTrailing.constant = constantSearchTrailingCompact
                self?.view.layoutIfNeeded()
            })
            }.addDisposableTo(disposeBag)
        
        viewModel.exitSearchSignal.subscribeNext { [weak self] (_) in
            self?.searchTextField.resignFirstResponder()
            self?.searchTextField.text = ""
            self?.titleView.hidden = false
            self?.searchTitleView.hidden = true
            self?.cancelSearchButton.hidden = true
            self?.unloadSearchVC()
            
            self?.view.layoutIfNeeded()
            UIView.animateWithDuration(duration, animations: {
                self?.searchBarTrailing.constant = constantSearchTrailingFull
                self?.view.layoutIfNeeded()
            })
            }.addDisposableTo(disposeBag)
        
        viewModel.unfocusSearchSignal.subscribeNext { [weak self] (_) in
            self?.searchTextField.resignFirstResponder()
            }.addDisposableTo(disposeBag)
    }

    private func initEmptyHint() {
        let isPremium = ShareDefaultsManager.sharedManager.isPremium
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.2
        paragraphStyle.alignment = .Left
        
        let freeHint = NSMutableAttributedString(string: [
            "• Add 1 tag from Trending list or by searching",
            "• Feed will always displayed in time order",
            "• Switching to Premium you can add unlimited",
            "  tags"
            ]
            .joinWithSeparator("\n"))
        freeHint.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, freeHint.length))
        
        let premiumHint = NSMutableAttributedString(string: [
            "• Add unlimited tags from Trending list or by",
            "  searching",
            "• Feed will always displayed in time order",
            "• Press and hold to drag and arrange"
            ]
            .joinWithSeparator("\n"))
        premiumHint.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, premiumHint.length))
        
        emptyHintLabel.attributedText = isPremium ? premiumHint : freeHint
    }

    private func observeNewContentAdded() {
        viewModel
            .newContentAddedSignal
            .observeOn(MainScheduler.instance)
            .subscribeNext { [weak self] content in
                if content.isEmpty == false {
                    self?.showTopToast(content + " added 👌")
                }
            }
            .addDisposableTo(disposeBag)
    }

    private func showTopToast(text: String) {
        let label = toastLabel
        let height: CGFloat = 35
        view.feeday_roundCorner(0)

        label.text = text
        toastLabelHeight.constant = 0
        view.layoutIfNeeded()
        label.hidden = false

        let duration = 0.5
        UIView.animateWithDuration(duration) {
            self.toastLabelHeight.constant = height
            self.view.layoutIfNeeded()

            dispatch_after(
                dispatch_time(DISPATCH_TIME_NOW, Int64(2 * NSEC_PER_SEC)),
                dispatch_get_main_queue()
            ) {
                UIView.animateWithDuration(
                    duration,
                    animations: {
                        self.toastLabelHeight.constant = 0
                        self.view.layoutIfNeeded()
                    },
                    completion: { (complete) in
                        label.hidden = true
                        self.view.feeday_roundCorner()
                })
            }
        }
    }

    @IBAction func topLeftButtonPressed() {
        // TODO: revert chagnes
        dismissViewControllerAnimated(true, completion: nil)
    }

    private func updateWhiteShadow() {
        whiteShadow.hidden = listContainer.hidden
    }

    private func initTopLeftButton() {
        topLeftButton.hidden = false
        topLeftButton.setTitle("Cancel", forState: UIControlState.Normal)
    }
}

extension AddTagsViewController: Instantiatable {
    
    static func storyboardName() -> String {
        // TODO: change the name of storyboard
        return "AddFollowContent"
    }
    
    static func storyboardID() -> String {
        return "AddTagsViewController"
    }
    
}
