//
//  SearchTagsTableViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/10.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import RxSwift

import FeedayUIKit
import FeedayCommonKit

class SearchTagsTableViewController: UITableViewController {
    
    var viewModel = AddTagsViewModel()
    
    private var disposeBag = DisposeBag()
    
    private var tagsManager = TagsManager.sharedManager
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel
            .searchTagsSignal
            .subscribeNext { [weak self] (_) in
                self?.tableView.reloadData()
            }.addDisposableTo(disposeBag)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.searchTagsList.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("SearchTagsTableViewCell", forIndexPath: indexPath) as! SearchTagsTableViewCell
        
        let row = indexPath.row
        
        if row < viewModel.searchTagsList.count {
            cell.model = viewModel.searchTagsList[row]
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if let cell = tableView.cellForRowAtIndexPath(indexPath) as? SearchTagsTableViewCell {
            let tag = cell.model
            if !tag.isCollected() {
                tagsManager.addTag(tag)
                viewModel.newContentAdded("#" + tag.name)
            }
            viewModel.exitSearch()
        }
    }
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        viewModel.unfocusSearch()
    }
}

extension SearchTagsTableViewController: Instantiatable {
    
    static func storyboardName() -> String {
        return "AddFollowContent"
    }
    
    static func storyboardID() -> String {
        return "SearchTagsTableViewController"
    }
}
