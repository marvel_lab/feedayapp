//
//  AddTagsFeaturedCollectionViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/20.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import RxSwift
import MBProgressHUD
import InstagramKit

import FeedayCommonKit
import FeedayUIKit

private let reuseIdentifier = "AddTagsCollectionViewCell"

class AddTagsFeaturedCollectionViewController: UICollectionViewController, RAReorderableLayoutDelegate, Rxable {

    var viewModel = AddTagsViewModel()

    var rx_collectionViewWidth: Variable<CGFloat> = Variable(0)
    
    private var sizingCell: AddTagsCollectionViewCell?
    
    private var cellID = "AddTagsCollectionViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cellNib = UINib(nibName: cellID, bundle: nil)
        collectionView?.registerNib(cellNib, forCellWithReuseIdentifier: cellID)
        sizingCell = cellNib.instantiateWithOwner(nil, options: nil).first as? AddTagsCollectionViewCell
        
        initData()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        updateUI()
    }
}

// MARK: - Private

extension AddTagsFeaturedCollectionViewController {
    
    private func initData() {
        TagsManager
            .sharedManager
            .rx_featuredTagsList
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribeNext { [weak self] (_) in
                self?.updateUI()
            }
            .addDisposableTo(disposeBag)
        
        TagsManager
            .sharedManager
            .tagListSignal
            .observeOn(MainScheduler.instance)
            .subscribeNext { [weak self] (_) in
                self?.updateUI()
            }
            .addDisposableTo(disposeBag)
        
        var rx_loadFeaturedTags = TagsManager.sharedManager.rx_loadFeaturedTagsList().observeOn(MainScheduler.instance)
        if TagsManager.sharedManager.featuredTagsList.count == 0 {
            rx_loadFeaturedTags = rx_loadFeaturedTags.showHUDWhileExecuting(view)
        }
        
        rx_loadFeaturedTags
            .subscribeNext { [weak self] (_) in
                self?.updateUI()
            }
            .addDisposableTo(disposeBag)
    }
    
    private func updateUI() {
        var w: CGFloat = 0
        let gap: CGFloat = 7
        let offset: CGFloat = 16
        
        var maxW: CGFloat = 0
        
        let tagCount = TagsManager.sharedManager.featuredTagsList.count
        for i in 0..<tagCount {
            let indexPath = NSIndexPath.init(forRow: i, inSection: 0)
            let size = sizeForCellAtIndexPath(indexPath)
            let temp = size.width + gap
            w += temp
            
            if temp > maxW {
                maxW = temp
            }
        }
        
        let width = offset * 2 + w / 2 + maxW + 1
        
        view.bounds.size.width = width
        view.frame.origin = CGPointZero
        
        collectionView?.bounds.size.width = view.bounds.size.width
        collectionView?.frame.origin = CGPointZero
        collectionView?.reloadData()
        
        rx_collectionViewWidth.value = width
    }
    
    private func sizeForCellAtIndexPath(indexPath: NSIndexPath) -> CGSize {
        if let cell = sizingCell {
            if let tag = tagForIndexPath(indexPath) {
                cell.model = tag
                return cell.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)
            }
        }
        
        return CGSizeZero
    }
}

// MARK: - UICollectionView

extension AddTagsFeaturedCollectionViewController {
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return TagsManager.sharedManager.featuredTagsList.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! AddTagsCollectionViewCell
        if let tag = tagForIndexPath(indexPath) {
            cell.mode = AddTagsCollectionViewCell.Mode.FeaturedList
            cell.model = tag
        }
        return cell
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        collectionView.deselectItemAtIndexPath(indexPath, animated: true)
        
        guard let tag = tagForIndexPath(indexPath) else { return }
        
        if tag.isCollected() == false {
            TagsManager.sharedManager.addTag(tag)
            collectionView.reloadData()
            viewModel.newContentAdded("#" + tag.name)
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if let cell = sizingCell {
            if let tag = tagForIndexPath(indexPath) {
                cell.model = tag
                return cell.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)
            }
        }
        
        return CGSizeZero
    }
    
    func collectionView(collectionView: UICollectionView, allowMoveAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
    
    func tagForIndexPath(indexPath: NSIndexPath) -> InstagramTag? {
        let list = TagsManager.sharedManager.featuredTagsList
        if indexPath.row < list.count {
            return list[indexPath.row]
        }
        
        return nil
    }
}

// MARK: - Instantiatable

extension AddTagsFeaturedCollectionViewController: Instantiatable {
    
    static func storyboardName() -> String {
        return "AddFollowContent"
    }
    
    static func storyboardID() -> String {
        return "AddTagsFeaturedCollectionViewController"
    }
}
