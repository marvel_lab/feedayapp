//
//  SearchTagsTableViewCell.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/7/10.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import FeedayUIKit
import InstagramKit

class SearchTagsTableViewCell: UITableViewCell {
    
    var model = InstagramTag() {
        didSet {
            updateUI()
        }
    }

    @IBOutlet weak var nameLabel: UILabel!
    
    private func updateUI() {
        nameLabel.text = "#" + model.name
    }
}
