//
//  UIApplication+FastOpenURL.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/8/20.
//  Copyright © 2016年 Feeday. All rights reserved.
//

public extension UIApplication {
    
    /// An attempt at solving 'openUrl()' freeze problem
    public func fastOpenURL(url: NSURL) {
        dispatch_after(
            dispatch_time(DISPATCH_TIME_NOW, Int64(0.001 * Double(NSEC_PER_SEC))),
            dispatch_get_main_queue()
        ) {
            UIApplication.sharedApplication().openURL(url)
        }
    }
}

