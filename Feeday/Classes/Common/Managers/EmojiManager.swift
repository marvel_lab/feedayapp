//
//  EmojiManager.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/4/2.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit

class EmojiManager: NSObject {
    
    static let sharedManager = EmojiManager()
    
    func getRandom9Emoji() -> [String] {
        let all = EmojiHolder.allEmojis
        let allCount = all.count
        var select: [String] = []
        
        while select.count < 9 {
            let i = Int(arc4random_uniform(UInt32(allCount)))
            let el = all[i]
            if select.contains(el) {
                continue
            }
            select.append(el)
        }
        
        return select
    }
}
