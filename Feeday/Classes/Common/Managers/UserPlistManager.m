//
//  UserPlistManager.m
//  Feeday
//
//  Created by Nils Kasseckert on 22.04.15.
//  Copyright (c) 2015 Nils Kasseckert. All rights reserved.
//

#import "UserPlistManager.h"
#import "NSDictionary+Safe.h"

@import FeedayCommonKit;
@import InstagramKit;

@interface UserPlistManager ()

@property NSMutableDictionary *userPlist;
@property NSURL               *storeURL;

@end

static NSString *kProfilePictureKey = @"profilePicture";

@implementation UserPlistManager

#pragma mark - Life Cycle

- (instancetype)init
{
    self = [super init];

    if (self) {
        self.storeURL  = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"User.plist"];
        self.userPlist = [NSMutableDictionary dictionaryWithContentsOfFile:[self.storeURL path]];
    }

    return self;
}

+ (instancetype)sharedManager
{
    static UserPlistManager *_instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[UserPlistManager alloc] init];
    });
    
    return _instance;
}

#pragma mark - User Data

- (nonnull NSString *)getUsernanme
{
    return [self.userPlist safeStringObjectForKey:@"username"];
}

- (nonnull NSString *)getUserID
{
    return [self.userPlist safeStringObjectForKey:@"id"];
}

- (void)logoutUser
{
    ShareDefaultsManager *mgr = [ShareDefaultsManager sharedManager];
    mgr.isLoggedIn  = NO;
    mgr.accessToken = @"";

    for (NSString *key in self.userPlist.allKeys) {
        [self.userPlist setObject:@"" forKey:key];
    }
    [self.userPlist writeToURL:self.storeURL atomically:YES];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:[UserPlistManager logoutNotificationName] object:nil];
}

- (void)saveUserData:(nonnull InstagramUser *)user
{
    [self loadProfilePictureFromURL:[[user profilePictureURL] absoluteString]];

    self.userPlist[@"id"]              = [user Id];
    self.userPlist[@"username"]        = [user username];
    self.userPlist[@"profile_picture"] = [[user profilePictureURL] absoluteString];

    [self.userPlist writeToURL:self.storeURL atomically:YES];
}

+ (NSString *)logoutNotificationName
{
    return @"userLogoutNotificationName";
}

#pragma mark - Profile Picture

- (nullable NSData *)getProfilePicture
{
    id pictureData = [self.userPlist valueForKey:kProfilePictureKey];

    // if picture data is not exists or is not kind of NSData class, return nil
    if (!pictureData || ![pictureData isKindOfClass:[NSData class]]) {
        return nil;
    }

    return pictureData;
}

- (void)loadProfilePictureData
{
    if (!self.userPlist) {
        return;
    }

    if (![self.userPlist isKindOfClass:[NSDictionary class]]) {
        return;
    }

    NSString *picPath = [self.userPlist safeStringObjectForKey:@"profile_picture"];
    if (!picPath || [picPath isEqualToString:@""])
        return;

    [self loadProfilePictureFromURL:picPath];
}

- (void)loadProfilePictureFromURL:(nonnull NSString *)url
{
    if (!url || [url isEqualToString:@""])
        return;

    if ([url isEqualToString:@"https://instagramimages-a.akamaihd.net/profiles/anonymousUser.jpg"]) {
        return;
    }

    // fetch user profile image from network

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];

        dispatch_async(dispatch_get_main_queue(), ^{

            // save image data retrieved from network
            if (imageData) {
                self.userPlist[kProfilePictureKey] = imageData;
                [self.userPlist writeToURL:self.storeURL atomically:YES];
            }

            [[NSNotificationCenter defaultCenter] postNotificationName:[UserPlistManager finishedLoadingProfilePictureNotificationName] object:nil];
        });
    });
}

+ (NSString *)finishedLoadingProfilePictureNotificationName
{
    return @"finishedLoadingProfilePicture";
}

#pragma mark - Additional

- (nonnull NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
