//
//  Touch3DManager.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/4/9.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import FeedayCommonKit

@available(iOS 9.0, *)
class Touch3DManager: NSObject {
    static let sharedManager = Touch3DManager()
    
    enum ItemType: String {
        case ChangeBlock = "de.fiorearcangelo.Feeday.Touch3D-ChangeBlock"
        case AddPeople = "de.fiorearcangelo.Feeday.Touch3D-AddPeople"
        case AddHashtag = "de.fiorearcangelo.Feeday.Touch3D-AddHashtag"
    }
    
    func openAppFrom3DTouchItem(item: UIApplicationShortcutItem) -> NSURL? {
        if let itemType = ItemType.init(rawValue: item.type) {
            // set deep link url
            var linkType: FeedayConstant.LinkType = .App
            switch itemType {
            case .AddPeople:
                linkType = .AddPeople
            case .AddHashtag:
                linkType = .AddHashtag
            case .ChangeBlock:
                linkType = .Setting
            }
            
            return NSURL(string: linkType.rawValue)
        }
        
        return nil
    }
}
