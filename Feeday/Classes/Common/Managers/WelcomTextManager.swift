//
//  WelcomTextManager.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/8/5.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import FeedayCommonKit

private let key = "WelcomTextManager_showCount"

class WelcomTextManager: NSObject {
    
    static let sharedManager = WelcomTextManager()
    
    private var defaults = NSUserDefaults.standardUserDefaults()
    
    var showCount: Int {
        get {
            return defaults.integerForKey(key)
        }
        set {
            defaults.setInteger(newValue, forKey: key)
            defaults.synchronize()
        }
    }
    
    var showCountIncreased: Bool = false
    
    func increaseShowCount() {
        if showCountIncreased == true {
            return
        }
        
        showCount += 1
        showCountIncreased = true
    }
    
    func loadText() -> String {
        increaseShowCount()
        
        // Fresh Start
        let count = PeopleManager.sharedManager.feedPeopleList.count + TagsManager.sharedManager.tagList.count
        if count == 0 {
            return  "Customize your feed experience,\nadding People and Tags"
        }
        
        // Loop Hint
        let num = showCount % 4
        var text = "Today is a fresh start"
        
        switch num {
        case 1:
            text = "Today is a fresh start"
        case 2:
            text = "Let's keep moving"
        case 3:
            let calendar = NSCalendar.currentCalendar()
            let dayIndex = calendar.component(NSCalendarUnit.Weekday, fromDate: NSDate())
            let dayNames = calendar.weekdaySymbols
            if dayIndex < dayNames.count {
                let day = dayNames[max(0, dayIndex-1)]
                text = "\(day) fresh start"
            }
        case 0:
            text = "Never miss your daily feed"
        default:
            break
        }
        
        return text
    }
    
}
