//
//  IAPManager.m
//  Feeday
//
//  Created by 陈圣晗 on 16/3/20.
//  Copyright © 2016年 Feeday. All rights reserved.
//

#import "IAPManager.h"
#import "RMStore.h"
#import "UserPlistManager.h"
#import "AppDelegate.h"
#import "Feeday-Swift.h"

@import FeedayCommonKit;

@interface IAPManager ()
    
    @property(nonatomic, strong) UserPlistManager *userPlistManager;
    
    @end

@implementation IAPManager
    
    static NSString *kBuyBlocks   = @"de.fiorearcangelo.Feeday.buyBlocks";
    static NSString *kBuyHidename = @"de.fiorearcangelo.Feeday.buyHidename";
    static NSString *kBuyPremium  = @"de.fiorearcangelo.Feeday.buyPremium";
    
#pragma mark - Life Cycle
    
+ (IAPManager *)sharedManager {
    static IAPManager *_instance = nil;
    
    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[self alloc] init];
        }
    }
    
    return _instance;
}
    
- (instancetype)init {
    self = [super init];
    if (self) {
        _userPlistManager = [UserPlistManager sharedManager];
    }
    return self;
}
    
#pragma mark - Buy Blocks
    
- (void)buyBlocksSuccess {
    [self buyPremiumSuccess];
}
    
#pragma mark - Buy Hidename
- (void)buyHidenameSuccess {
    [self buyPremiumSuccess];
}
    
#pragma mark - Buy Premium

- (void)fetchPremiumPrice:(void (^)(NSString *price, NSError *error))onComplete {
    [[RMStore defaultStore] requestProducts:[NSSet setWithArray:@[kBuyPremium]]
                                    success:^(NSArray *products, NSArray *invalidProductIdentifiers) {
                                        if (products.count == 0) {
                                            if (onComplete) {
                                                onComplete(@"", [NSError errorWithDomain:NSStringFromClass([IAPManager class])
                                                                                    code:-1
                                                                                userInfo:@{
                                                                                        NSLocalizedDescriptionKey: @"Fetch IAP Price Failed."
                                                                                }]);
                                                return;
                                            }
                                        }

                                        SKProduct *product = [products firstObject];
                                        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                                        [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
                                        [formatter setLocale:product.priceLocale];
                                        NSString *localizedPrice = [formatter stringFromNumber:product.price];
                                        if (onComplete) {
                                            onComplete(localizedPrice, nil);
                                        }

                                    }
                                    failure:^(NSError *error) {
                                        if (onComplete) {
                                            onComplete(@"", error);
                                        }
                                    }];
}

- (void)fetchPrimiumPriceDetails:(void (^)(NSString *currentPrice, NSString *originalPrice, NSString *discount, NSError *error))onComplete {
    [[RMStore defaultStore] requestProducts:[NSSet setWithArray:@[kBuyBlocks, kBuyPremium]]
                                    success:^(NSArray *products, NSArray *invalidProductIdentifiers) {
                                        if (products.count == 0) {
                                            if (onComplete) {
                                                onComplete(@"",@"",@"", [NSError errorWithDomain:NSStringFromClass([IAPManager class])
                                                                                    code:-1
                                                                                userInfo:@{
                                                                                           NSLocalizedDescriptionKey: @"Fetch IAP Price Failed."
                                                                                           }]);
                                                return;
                                            }
                                        }

                                        NSNumber *currentPriceNumber = @0;
                                        NSNumber *originalPriceNumber = @0;

                                        NSString *currentPrice = @"";
                                        NSString *originalPrice = @"";
                                        NSString *discount = @"";

                                        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                                        [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];

                                        for (SKProduct *product in products) {
                                            if ([product.productIdentifier isEqualToString:kBuyPremium]) {
                                                currentPriceNumber = product.price;
                                                [formatter setLocale:product.priceLocale];
                                                currentPrice = [formatter stringFromNumber:product.price];
                                                continue;
                                            }
                                            if ([product.productIdentifier isEqualToString:kBuyBlocks]) {
                                                originalPriceNumber = product.price;
                                                [formatter setLocale:product.priceLocale];
                                                originalPrice = [formatter stringFromNumber:product.price];
                                            }
                                        }

                                        double double_current = [currentPriceNumber doubleValue];
                                        double double_original = [originalPriceNumber doubleValue];


                                        if (double_current > 0 && double_original > 0 && double_current < double_original) {
                                            int int_discount = (double_original - double_current) / double_original * 100;
                                            discount = [NSString stringWithFormat:@"%d%%", int_discount];
                                        }

                                        if (onComplete) {
                                            onComplete(currentPrice, originalPrice, discount, nil);
                                        }

                                    }
                                    failure:^(NSError *error) {
                                        if (onComplete) {
                                            onComplete(@"",@"",@"", error);
                                        }
                                    }];
}
    
- (void)buyPremium:(void (^)(NSError *error))onComplete {
#if TARGET_IPHONE_SIMULATOR
    [self buyPremiumSuccess];
    if (onComplete) {
        onComplete(nil);
    }
    return;
#endif
    
    RMStore *store = [RMStore defaultStore];
    [store requestProducts:[[NSSet alloc] initWithObjects:kBuyPremium, nil]
                   success:^(NSArray *products, NSArray *invalidProductIdentifiers) {
                       [store addPayment:kBuyPremium
                                 success:^(SKPaymentTransaction *transaction) {
                                     [self buyPremiumSuccess];
                                     if (onComplete) {
                                         onComplete(nil);
                                     }
                                 }
                                 failure:^(SKPaymentTransaction *transaction, NSError *error) {
                                     [self buyPremiumFail:error];
                                     if (onComplete) {
                                         onComplete(error);
                                     }
                                 }];
                   }
                   failure:^(NSError *error) {
                       [self buyPremiumFail:error];
                       if (onComplete) {
                           onComplete(error);
                       }
                   }];
    
    return;
}
    
+ (NSString *)buyPremiumSuccessNotification {
    return @"IAPManager_buyPremiumSuccessNotification";
}
    
+ (NSString *)buyPremiumFailNotification {
    return @"IAPManager_buyPremiumFailNotification";
}
    
- (void)buyPremiumSuccess {
    [[ShareDefaultsManager sharedManager] unlockPremium:true];
    [[NSNotificationCenter defaultCenter] postNotificationName:[IAPManager buyPremiumSuccessNotification] object:nil];
}
    
- (void)buyPremiumFail:(NSError *)error {
    NSLog(@"Buy Premium Fail: %@", error);
    [[NSNotificationCenter defaultCenter] postNotificationName:[IAPManager buyPremiumFailNotification] object:nil];
}
    
#pragma mark - Restore Purchase
    
- (void)restorePurchase {
#if TARGET_IPHONE_SIMULATOR
    [self buyHidenameSuccess];
    [self buyBlocksSuccess];
    [self buyPremiumSuccess];
    [self alertRestorePurchaseSuccess];
    return;
#endif
    
    [[RMStore defaultStore] restoreTransactionsOnSuccess:^(NSArray *transactions) {
        for (SKPaymentTransaction *transaction in transactions) {
            if ([transaction.payment.productIdentifier isEqualToString:kBuyBlocks]) {
                [self buyBlocksSuccess];
            } else if ([transaction.payment.productIdentifier isEqualToString:kBuyHidename]) {
                [self buyHidenameSuccess];
            } else if ([transaction.payment.productIdentifier isEqualToString:kBuyPremium]) {
                [self buyPremiumSuccess];
            }
        }
        
        if (transactions && transactions.count > 0) {
            [self alertRestorePurchaseSuccess];
        }
    }
                                                 failure:^(NSError *error) {
                                                     NSLog(@"RestorePurchase Fail: %@", error);
                                                 }];
}
    
- (void)alertRestorePurchaseSuccess {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Restore purchases"
                                                    message:@"Now Restore Purchase has been completed!"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}
    
    @end
