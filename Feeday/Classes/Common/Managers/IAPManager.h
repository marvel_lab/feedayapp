//
//  IAPManager.h
//  Feeday
//
//  Created by 陈圣晗 on 16/3/20.
//  Copyright © 2016年 Feeday. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IAPManager : NSObject

+ (IAPManager *)sharedManager;

- (void)fetchPremiumPrice:(void (^)(NSString *price, NSError *error))onComplete;

- (void)fetchPrimiumPriceDetails:(void (^)(NSString *currentPrice, NSString *originalPrice, NSString *discount, NSError *error))onComplete;

- (void)buyPremium:(void (^)(NSError *error))onComplete;
    
+ (NSString *)buyPremiumSuccessNotification;

+ (NSString *)buyPremiumFailNotification;

- (void)restorePurchase;

@end
