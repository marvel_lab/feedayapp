//
//  UserPlistManager.h
//  Feeday
//
//  Created by Nils Kasseckert on 22.04.15.
//  Copyright (c) 2015 Nils Kasseckert. All rights reserved.
//

#import <Foundation/Foundation.h>

@class InstagramUser;

@interface UserPlistManager : NSObject

+ (nonnull instancetype)sharedManager;

#pragma mark - User Data

- (nonnull NSString *)getUsernanme;

- (nonnull NSString *)getUserID;

- (void)logoutUser;

- (void)saveUserData:(nonnull InstagramUser *)user;

+ (nonnull NSString *)logoutNotificationName;

#pragma mark - Profile Picture

- (nullable NSData *)getProfilePicture;

- (void)loadProfilePictureData;

+ (nonnull NSString *)finishedLoadingProfilePictureNotificationName;

@end
