//
//  DeepLinkManager.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/4/22.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit

import RxSwift

import FeedayCommonKit

private let kDeepLinkKey = "DeepLinkManager_URL"

class DeepLinkManager: NSObject {
    
    var rx_linkType = Variable<FeedayConstant.LinkType>(.App)
    
    var paramsDict = [String: AnyObject]()
    
    static let sharedManager = DeepLinkManager()
    
    func openAppFromURL(url: NSURL? = nil) {
        guard let url = url else {return}
        
        NSUserDefaults.standardUserDefaults().setURL(url, forKey: kDeepLinkKey)
        NSUserDefaults.standardUserDefaults().synchronize()
        
        checkAndFireIntent()
    }
    
    func checkAndFireIntent() {
        if let urlString = NSUserDefaults.standardUserDefaults().URLForKey(kDeepLinkKey)?.absoluteString?.lowercaseString {
            
            guard let url = NSURL(string: urlString) where url.scheme == "feedayios" else { return }
            
            if processNoParamsLink(urlString) { return }
            
            if processMediaLink(urlString) { return }
        }
    }
    
    func clearIntent() {
        NSUserDefaults.standardUserDefaults().removeObjectForKey(kDeepLinkKey)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    private func processNoParamsLink(urlString: String) -> Bool {
        let urlString = urlString.lowercaseString
        guard let linkType = FeedayConstant.LinkType.init(rawValue: urlString) else {
            return false
        }
        
        rx_linkType.value = linkType
        return true
    }
    
    private func processMediaLink(urlString: String) -> Bool {
        let urlString = urlString.lowercaseString
        guard let urlComponents = NSURLComponents(string: urlString)
            where urlComponents.host == "media" else {
                return false
        }
        
        guard let queryItems = urlComponents.queryItems else {
            return false
        }
        
        var queryDict = [String: String]()
        for item in queryItems {
            queryDict[item.name] = item.value ?? ""
        }
        
        guard let id = queryDict["id"] where id.isEmpty == false else {
            return false
        }
        
        guard let short_code = queryDict["short_code"] where short_code.isEmpty == false else {
            return false
        }
        
        paramsDict = [
            "id": id,
            "short_code": short_code
        ]
        
        rx_linkType.value = .Media
        
        return true
    }
}
