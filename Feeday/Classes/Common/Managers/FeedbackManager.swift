//
//  RateManager.swift
//  Feeday
//
//  Created by 陈圣晗 on 16/4/18.
//  Copyright © 2016年 Feeday. All rights reserved.
//

import UIKit
import FeedayCommonKit
import FeedayUIKit
import MessageUI
import SDVersion

class FeedbackManager: NSObject, MFMailComposeViewControllerDelegate {

    static let sharedManager = FeedbackManager()
    
    var count: Int {
        get {
            return self.userDefaults.integerForKey(self.countKey) ?? 0
        }
    }
    
    private let userDefaults = NSUserDefaults.standardUserDefaults()
    private let countKey = "RateManager_count"
    
    func increateCount() {
        self.userDefaults.setInteger(self.count + 1, forKey: self.countKey)
        self.userDefaults.synchronize()
    }
    
    func shouldOpenRatePopup() -> Bool {
        return count % 5 == 0 && count != 0
    }
    
    func openPopupMain() {
        guard let vc = UIApplication.sharedApplication().delegate?.window??.rootViewController else {return}
        FeedayAlertHelper.showAlert(
            inViewController: vc,
            message: "How do you feel about \nusing the Feeday app?",
            okTitle: "Happy",
            cancelTitle: "Not happy",
            showCancel: true,
            okHandler: { (action) in
                self.openPopupRate()
            },
            cancelHandler: { (action) in
                self.openPopupFeedback()
        })
    }
    
    func openPopupRate() {
        guard let vc = UIApplication.sharedApplication().delegate?.window??.rootViewController else {return}
        FeedayAlertHelper.showAlert(
            inViewController: vc,
            message: "So you're pretty happy with Feeday. \nWould you mind taking 1 minute to rate the app? \nWe will love you forever if you give 5 stars!",
            okTitle: "Yes",
            cancelTitle: "Later",
            showCancel: true,
            okHandler: { (action) in
                self.gotoRate()
            },
            cancelHandler: { (action) in
        })
    }
    
    func openPopupFeedback() {
        guard let vc = UIApplication.sharedApplication().delegate?.window??.rootViewController else {return}
        FeedayAlertHelper.showAlert(
            inViewController: vc,
            message: "How can we make you happy? \nWe would love to know how \nwe can improve your experience.",
            okTitle: "Email us",
            cancelTitle: "Cancel",
            showCancel: true,
            okHandler: { (action) in
                self.gotoEmail()
            },
            cancelHandler: { (action) in
        })
    }
    
    func gotoRate() {
        let appId = "986398806"
        let urlStr = "https://itunes.apple.com/app/viewContentsUserReviews?id=\(appId)"
        let url = NSURL(string: urlStr)!
        
        if UIApplication.sharedApplication().canOpenURL(url) {
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
    func gotoEmail() {
        guard let vc = UIApplication.sharedApplication().delegate?.window??.rootViewController else {return}
        
        let locale = NSLocale.currentLocale()
        let countryCode = locale.objectForKey(NSLocaleCountryCode)
        let device = SDiOSVersion.readableDeviceVersionName()
        let system = UIDevice.currentDevice().systemVersion
        let username = (UIApplication.sharedApplication().delegate as! AppDelegate).userPlistManager.getUsernanme()
        let version = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleShortVersionString") as? String ?? "1.0"
        let messageBody = "Question / Suggestion: \n\n\n\n\n\n\n\n\n\n ------------------------------\nNation: \(countryCode!) \nDevice: \(device) - \(system) \nUsername: \(username)\nFeeday version: \(version)"
        
        if device == "Simulator" {
            print(messageBody)
            return
        }
        
        if(!MFMailComposeViewController.canSendMail()){
            FeedayAlertHelper.showAlert(
                inViewController: vc,
                title: "This device can not send a mail!",
                message: "This device can not send a mail! Please add a Mail Account!"
            )
            return
        }
        
        let mailVC = MFMailComposeViewController()
        mailVC.setSubject("Feeday")
        mailVC.setToRecipients(["hello@feedayapp.com"])
        mailVC.setMessageBody(messageBody, isHTML: false)
        mailVC.mailComposeDelegate = self
        vc.presentViewController(mailVC, animated: true, completion: nil)
    }
    
    // MARK: - Mail
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
}
