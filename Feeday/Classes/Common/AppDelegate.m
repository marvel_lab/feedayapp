//
//  AppDelegate.m
//  Feeday
//
//  Created by Nils Kasseckert on 13.04.15.
//  Copyright (c) 2015 Nils Kasseckert. All rights reserved.
//

#import "AppDelegate.h"
#import "UserPlistManager.h"
#import "InstagramAppMonitor.h"
#import "Feeday-Swift.h"

@import MessageUI;

@import AFNetworking;
@import Flurry_iOS_SDK;
@import Fabric;
@import Crashlytics;
@import FBNotifications;
@import FBSDKCoreKit;
@import SVProgressHUD;
@import Inapptics;

@import FeedayCommonKit;

@interface AppDelegate () <MFMailComposeViewControllerDelegate>

@end

@implementation AppDelegate

#pragma mark - App Delegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    NSLog(@"App Launch.");
    
    [Fabric with:@[[Crashlytics class]]];

    [Inapptics letsGoWithAppToken:@"Ik8cPGfjFS93uJpwnxNEm1T7eBfAP4hF4sIqQAjz"];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                    UIUserNotificationTypeBadge |
                                                    UIUserNotificationTypeSound);
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                             categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    NSURL *launchedURL = [launchOptions objectForKey:UIApplicationLaunchOptionsURLKey];
    [[DeepLinkManager sharedManager] openAppFromURL:launchedURL];
    NSLog(@"launchedURL: %@", launchedURL);

    [FlurryManager startSession];
    [FlurryManager logEvent:FlurryEventAppLaunch];

    self.reachabilityManager = [AFNetworkReachabilityManager managerForDomain:@"www.instagram.com"];
    [self.reachabilityManager startMonitoring];
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"User.plist"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    // If the expected store doesn't exist, copy the default store.
    if (![fileManager fileExistsAtPath:[storeURL path]]) {
        NSURL *defaultStoreURL = [[NSBundle mainBundle] URLForResource:@"User" withExtension:@"plist"];
        if (defaultStoreURL) {
            [fileManager copyItemAtURL:defaultStoreURL toURL:storeURL error:NULL];
        }
    }
         
    self.userPlistManager = [UserPlistManager sharedManager];
    
    HomeContainerViewController *rootVC = [[UIStoryboard storyboardWithName:@"Home" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeContainerViewController"];
    
    self.window.rootViewController = rootVC;
    [self.window makeKeyAndVisible];
        
    // Pre load data
    [[PeopleManager sharedManager] loadFeaturedPeopleList];
    [[TagsManager sharedManager] loadFeaturedTagsList];
    [[ShareDefaultsManager sharedManager] updatePremiumStateFromCloud];

    [RateHelper increaseAccessCount];
    
    return YES;
}

- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler
{
    if ([[UIDevice currentDevice].systemVersion floatValue] < 9.0) {
        completionHandler(NO);
        return;
    }

    // use deep link to deal with 3d touch
    Touch3DManager *manager = [Touch3DManager sharedManager];
    NSURL *url = [manager openAppFrom3DTouchItem:shortcutItem];
    if (url) {
        [self application:application handleOpenURL:url];
    }
    
    completionHandler(YES);
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    // Deep Link
    [[DeepLinkManager sharedManager] openAppFromURL:url];
    
    return YES;
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    // Deep Link
    NSLog(@"application openURL: %@ options: %@", url, options);
    [[DeepLinkManager sharedManager] openAppFromURL:url];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    [[DeepLinkManager sharedManager] openAppFromURL:url];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                                                   openURL:url
                                         sourceApplication:sourceApplication
                                                annotation:annotation];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [InstagramAppMonitor setIsInstagramInstalled:[application canOpenURL:[NSURL URLWithString:@"instagram://app"]]];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
    
    [InstagramAppMonitor setIsInstagramInstalled:[application canOpenURL:[NSURL URLWithString:@"instagram://app"]]];

    NSString *userId = [[UserPlistManager sharedManager] getUserID];
    [Flurry setUserID:userId];
    
    [[PeopleManager sharedManager] addPeopleFromPasteboard:nil];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [FBSDKAppEvents setPushNotificationsDeviceToken:deviceToken];
    
    NSString * deviceTokenString = [[[[deviceToken description]
                                      stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                     stringByReplacingOccurrencesOfString: @">" withString: @""]
                                    stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    NSLog(@"The generated device token string is: %@", deviceTokenString);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [FBSDKAppEvents logPushNotificationOpen:userInfo];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(nonnull void (^)(UIBackgroundFetchResult))completionHandler {
    FBNotificationsManager *notificationsManager = [FBNotificationsManager sharedManager];
    [notificationsManager presentPushCardForRemoteNotificationPayload:userInfo
                                                   fromViewController:nil
                                                           completion:^(FBNCardViewController * _Nullable viewController, NSError * _Nullable error) {
                                                               if (error) {
                                                                   completionHandler(UIBackgroundFetchResultFailed);
                                                               } else {
                                                                   completionHandler(UIBackgroundFetchResultNewData);
                                                               }
                                                           }];
}

- (void)application:(UIApplication *)application
handleActionWithIdentifier:(NSString *)identifier
forRemoteNotification:(NSDictionary *)userInfo
  completionHandler:(void (^)())completionHandler {
    [FBSDKAppEvents logPushNotificationOpen:userInfo action:identifier];
}

#pragma mark - Mail Delegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self.window.rootViewController dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Private

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


@end
