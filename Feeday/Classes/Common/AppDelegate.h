//
//  AppDelegate.h
//  Feeday
//
//  Created by Nils Kasseckert on 13.04.15.
//  Copyright (c) 2015 Nils Kasseckert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UserPlistManager;
@class AFNetworkReachabilityManager;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) AFNetworkReachabilityManager *reachabilityManager;

@property UserPlistManager *userPlistManager;
@end