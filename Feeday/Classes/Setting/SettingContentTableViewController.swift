import UIKit
import Social

import RxSwift
import RxCocoa
import Flurry_iOS_SDK

import FeedayUIKit
import FeedayCommonKit

class SettingContentTableViewController: UITableViewController, Rxable {
    
    @IBOutlet weak var showUsernameTitleLabel: UILabel!
    @IBOutlet weak var showUsernameSwitch: UISwitch!
    @IBOutlet weak var widgetTutorialButton: UIButton!
    @IBOutlet weak var messageTutorialButton: UIButton!
    @IBOutlet weak var restorPurchaseButton: UIButton!
    @IBOutlet weak var rateButton: UIButton!
    @IBOutlet weak var feedbackButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var tweetButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    
    @IBOutlet weak var versionLabel: UILabel!
    
    var viewModel = HomeViewModel()
}

// MARK: - Life Cycle

extension SettingContentTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initVersionLabel()
        initShowName()
    }
}

// MARK: - IBAction

extension SettingContentTableViewController {
    
    @IBAction func showUsernameValueChanged(sender: UISwitch) {
        
    }
    
    @IBAction func widgetTutorialButtonPressed(sender: AnyObject) {
        guard let vc = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewControllerWithIdentifier("VideoViewController") as? VideoViewController else { return }
        vc.forWidget = true
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    @IBAction func messageTutorialButtonPressed(sender: AnyObject) {
        guard let vc = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewControllerWithIdentifier("VideoViewController") as? VideoViewController else { return }
        vc.forMessage = true
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    @IBAction func restorePurchaseButtonPressed(sender: AnyObject) {
        let alert = UIAlertController(
            title: nil,
            message: "Restore purchases? \n WiFi is good idea.",
            preferredStyle: UIAlertControllerStyle.Alert
        )
        alert.addAction(
            UIAlertAction(
                title: "Cancel",
                style: UIAlertActionStyle.Cancel,
                handler: nil)
        )
        
        alert.addAction(
            UIAlertAction(
                title: "Restore purchases",
                style: UIAlertActionStyle.Default,
                handler: {
                    (action: UIAlertAction) -> Void in
                    IAPManager.sharedManager().restorePurchase()
            })
        )
        
        presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func rateButtonPressed(sender: AnyObject) {
        FlurryManager.logEvent(.TapRateUs)
        FeedbackManager.sharedManager.gotoRate()
    }
    
    @IBAction func feedbackButtonPressed(sender: AnyObject) {
        FeedbackManager.sharedManager.gotoEmail()
    }
    
    @IBAction func shareButtonPressed(sender: AnyObject) {
        FlurryManager.logEvent(.TapShare)
        let actItems = ["Use @FeedayApp for browse and share your #Instagram feed from the widgets screen 👉 http://get.feedayapp.com for free 🔥"]
        let actVC = UIActivityViewController(activityItems: actItems, applicationActivities: nil)
        actVC.popoverPresentationController?.sourceView = sender as? UIView
        actVC.popoverPresentationController?.sourceRect = (sender as? UIView)?.frame ?? CGRectZero
        if actVC.popoverPresentationController?.sourceRect != nil {
            actVC.popoverPresentationController?.sourceRect.origin.x /= 2;
            actVC.popoverPresentationController?.sourceRect.size.width /= 2;
        }
        presentViewController(actVC, animated: true, completion: nil)
    }
    
    @IBAction func tweetButtonPressed() {
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter) {
            if let vc = SLComposeViewController.init(forServiceType: SLServiceTypeTwitter) {
                vc.setInitialText("@feedayapp")
                presentViewController(vc, animated: true, completion: nil)
            } else {
                FeedayAlertHelper.showAlert(inViewController: self, message: "Please log in your twitter account in device setting")
            }
        }
    }
    
    @IBAction func gifButtonPressed() {
        guard let url = NSURL.init(string: "http://www.giphy.com") else { return }
        UIApplication.sharedApplication().openURL(url)
    }
    
    @IBAction func logoutButtonPressed(sender: AnyObject) {
        FlurryManager.logEvent(.TapLogout)
        Flurry.setUserID("")
        UserPlistManager.sharedManager().logoutUser()
    }
}

// MARK: - Private

extension SettingContentTableViewController {
    
    private func initVersionLabel() {
        var version = "2.0"
        
        if let dictionary = NSBundle.mainBundle().infoDictionary {
            if let v = dictionary["CFBundleShortVersionString"] as? String {
                version = v
            }
        }
        
        let arr = [
            "Version " + "\(version)",
            "Made with love in Italy."
        ]
        let str = arr.joinWithSeparator("\n")
        versionLabel.text = str
    }
    
    private func initShowName() {
        showUsernameSwitch.on = !ShareDefaultsManager.sharedManager.hideName
        
        showUsernameSwitch
            .rx_value
            .subscribeNext { [weak self] isOn in
                guard let sender = self?.showUsernameSwitch else { return }
                
                // turn off hide name
                if sender.on {
                    FlurryManager.logEvent(.TapShowUsernameOn)
                    ShareDefaultsManager.sharedManager.hideName = false
                    return
                }
                
                // turn on hide name
                FlurryManager.logEvent(.TapShowUsernameOff)
                
                // set hidename directly for premium users
                if (ShareDefaultsManager.sharedManager.isPremium) {
                    ShareDefaultsManager.sharedManager.hideName = true
                    return
                }
                
                // temprory turn off hide name then alert to buy
                sender.on = true
                
                guard let alert = IAPPremiumViewController.instantiateViewController() else { return }
                self?.presentViewController(alert, animated: true, completion: {
                    alert.view.frame.origin.y = 0
                })
            }
            .addDisposableTo(disposeBag)
    }
}

// MARK: - Instantiatable

extension SettingContentTableViewController: Instantiatable {
    
    static func storyboardName() -> String {
        return "Home"
    }
    
    static func storyboardID() -> String {
        return "SettingContentTableViewController"
    }
}

