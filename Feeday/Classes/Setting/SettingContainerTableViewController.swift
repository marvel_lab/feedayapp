import UIKit
import Social
import RxSwift
import RxCocoa
import FeedayUIKit
import FeedayCommonKit

class SettingContainerTableViewController: UITableViewController, Rxable {
    
    @IBOutlet weak var premiumButton: UIButton!
    
    @IBOutlet weak var blockMonoLabel: UILabel!
    @IBOutlet weak var blockAsymmetricLabel: UILabel!
    @IBOutlet weak var blockGridLabel: UILabel!
    
    @IBOutlet weak var blockMonoButton: UIButton!
    @IBOutlet weak var blockAsymmetricButton: UIButton!
    @IBOutlet weak var blockGridButton: UIButton!
}

// MARK: - Life Cycle

extension SettingContainerTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.feeday_roundCorner()
        
        initPremiumButton()
        initObservingSelectBlock()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
}

// MARK: - IBAction

extension SettingContainerTableViewController {
    
    @IBAction func premiumButtonPressed(sender: AnyObject) {
        guard let alert = IAPPremiumViewController.instantiateViewController() else { return }
        presentViewController(alert, animated: true, completion: {
            alert.view.frame.origin.y = 0
        })
    }
}

// MARK: - Private

extension SettingContainerTableViewController {
    
    private func initPremiumButton() {
        
        func _updateButton(isPremium: Bool = false) {
            premiumButton.hidden = isPremium
        }
        
        _updateButton(ShareDefaultsManager.sharedManager.isPremium)
        
        ShareDefaultsManager
            .sharedManager
            .rx_premiumUnlocked
            .asObservable()
            .subscribeNext { isPremium in
                _updateButton(isPremium)
            }
            .addDisposableTo(disposeBag)
    }
    
    private func initObservingSelectBlock() {
        
        func _updateBlocks(type: ShareDefaultsManager.BlockType) {
            let alphaOff: CGFloat = 0.4
            let alphaOn: CGFloat = 1
            
            blockMonoLabel.alpha = alphaOff
            blockAsymmetricLabel.alpha = alphaOff
            blockGridLabel.alpha = alphaOff
            
            blockMonoButton.alpha = alphaOff
            blockAsymmetricButton.alpha = alphaOff
            blockGridButton.alpha = alphaOff
            
            switch type {
            case .Mono:
                blockMonoLabel.alpha = alphaOn
                blockMonoButton.alpha = alphaOn
            case .Asymmetric:
                blockAsymmetricLabel.alpha = alphaOn
                blockAsymmetricButton.alpha = alphaOn
            case .Grid:
                blockGridLabel.alpha = alphaOn
                blockGridButton.alpha = alphaOn
            }
        }
        
        let mgr = ShareDefaultsManager.sharedManager
        
        // update it immediately
        _updateBlocks(mgr.blockType)
        
        // observe button tap
        
        let checkPremiumSignal = Observable<Bool>.create { [weak self] (observer) -> Disposable in
            
            if !ShareDefaultsManager.sharedManager.isPremium {
                if let alert = IAPPremiumViewController.instantiateViewController() {
                    self?.presentViewController(alert, animated: true) {
                        alert.view.frame.origin.y = 0
                    }
                }
            }
            
            observer.onNext(ShareDefaultsManager.sharedManager.isPremium)
            observer.onCompleted()
            
            return NopDisposable.instance
        }
        
        blockMonoButton
            .rx_tap
            .map { () -> () in
                FlurryManager.logEvent(.TapMono)
            }
            .flatMap { return checkPremiumSignal }
            .subscribeNext { isPremium in
                mgr.blockType = .Mono
            }
            .addDisposableTo(disposeBag)
        
        blockAsymmetricButton
            .rx_tap
            .map { () -> () in
                FlurryManager.logEvent(.TapAsymmetric)
            }
            .flatMap { return checkPremiumSignal }
            .subscribeNext { isPremium in
                mgr.blockType = isPremium ? .Asymmetric : .Mono
            }
            .addDisposableTo(disposeBag)
        
        blockGridButton
            .rx_tap
            .map { () -> () in
                FlurryManager.logEvent(.TapGrid)
            }
            .flatMap { return checkPremiumSignal }
            .subscribeNext { isPremium in
                mgr.blockType = isPremium ? .Grid : .Mono
            }
            .addDisposableTo(disposeBag)
        
        // observe type change
        mgr.blockTypeChangeSignal
            .subscribeNext { type in
                _updateBlocks(type)
            }
            .addDisposableTo(disposeBag)
    }
}

// MARK: - Instantiatable

extension SettingContainerTableViewController: Instantiatable {
    
    static func storyboardName() -> String {
        return "Home"
    }
    
    static func storyboardID() -> String {
        return "SettingContainerTableViewController"
    }
}

