//
//  VideoViewController.h
//  Feeday
//
//  Created by Nils Kasseckert on 20.05.15.
//  Copyright (c) 2015 Nils Kasseckert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoViewController : UIViewController

@property(nonatomic, assign) BOOL forWidget;
@property(nonatomic, assign) BOOL forMessage;

@end
