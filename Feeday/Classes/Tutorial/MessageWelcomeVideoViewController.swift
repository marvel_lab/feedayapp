//
//  MessageWelcomeVideoViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 2017/2/20.
//  Copyright © 2017年 Feeday. All rights reserved.
//

import UIKit
import FeedayCommonKit
import RxSwift
import RxCocoa
import AVKit
import AVFoundation

private let PLIST_FILE_NAME = "MessageWelcomeVideoMark.plist"
private let PLIST_KEY       = "watched"
private let RESOURCE_NAME   = "MessageWelcome"

class MessageWelcomeVideoViewController: UIViewController {

    @IBOutlet weak var viewContainerView: UIView?
    @IBOutlet weak var doneButton: UIButton?

    private var playerController: AVPlayerViewController? = nil

    private let disposeBag = DisposeBag()

    static func instantiateViewController() -> MessageWelcomeVideoViewController? {
        let sb = UIStoryboard.init(name: "MessageWelcomeVideoViewController", bundle: nil)
        return sb.instantiateInitialViewController() as? MessageWelcomeVideoViewController
    }

    static func present(overViewController: UIViewController) {
        if let vc = instantiateViewController() {
            overViewController.presentViewController(vc, animated: true, completion: nil)
        }
    }

    static func isWatched() -> Bool {
        if let url = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first,
            let fileURL = url.URLByAppendingPathComponent(PLIST_FILE_NAME),
            let data = NSData(contentsOfURL: fileURL),
            let dict = NSKeyedUnarchiver.unarchiveObjectWithData(data),
            let watched = dict[PLIST_KEY] as? Bool {
            return watched
        }
        return false
    }

    static func markWatched() {
        let dict = [PLIST_KEY: true]
        let data = NSKeyedArchiver.archivedDataWithRootObject(dict)
        if let url = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first,
            let fileURL = url.URLByAppendingPathComponent(PLIST_FILE_NAME) {
            do {
                try data.writeToURL(fileURL, options: NSDataWritingOptions.AtomicWrite)
                print("MessageWelcomeVideoViewController::markWatched OK")
            } catch {
                print("MessageWelcomeVideoViewController::markWatched Error")
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        doneButton?.rx_tap.subscribeNext { [weak self] in self?.dismissViewControllerAnimated(true, completion: nil) }.addDisposableTo(disposeBag)

        if let path = NSBundle.mainBundle().pathForResource(RESOURCE_NAME, ofType: "mp4") {
            let url = NSURL(fileURLWithPath: path)

            let player = AVPlayer(URL: url)
            playerController = AVPlayerViewController()
            if let playerController = playerController {
                playerController.player = player
                self.addChildViewController(playerController)
                self.viewContainerView?.addSubview(playerController.view)
                playerController.view.frame = self.viewContainerView?.bounds ?? CGRectZero
                playerController.showsPlaybackControls = false

                let tapGesture = UITapGestureRecognizer()
                tapGesture
                    .rx_event
                    .subscribeNext { (gesture) in
                        playerController.showsPlaybackControls = true
                    }
                    .addDisposableTo(disposeBag)
                tapGesture.delegate = self
                self.viewContainerView?.addGestureRecognizer(tapGesture)
            }

        }

        MessageWelcomeVideoViewController.markWatched()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        FlurryManager.logTimeEventStart(.TimingMessageWelcomeVideo)
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        playerController?.player?.play()
    }

    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        FlurryManager.logTimeEventStop(.TimingMessageWelcomeVideo)
    }
}

extension MessageWelcomeVideoViewController: UIGestureRecognizerDelegate {

    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailByGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if self.playerController?.showsPlaybackControls == false {
            // print("\nshouldBeRequiredToFailByGestureRecognizer? \(otherGestureRecognizer)")
            if let tapGesture = otherGestureRecognizer as? UITapGestureRecognizer {
                if tapGesture.numberOfTouchesRequired == 1 {
                    return true
                }
            }
        }
        return false
    }
}
