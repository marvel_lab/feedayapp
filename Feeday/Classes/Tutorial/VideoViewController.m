//
//  VideoViewController.m
//  Feeday
//
//  Created by Nils Kasseckert on 20.05.15.
//  Copyright (c) 2015 Nils Kasseckert. All rights reserved.
//

#import "VideoViewController.h"
#import <MediaPlayer/MediaPlayer.h>

@interface VideoViewController ()

@property MPMoviePlayerController *moviePlayerController;

@end

@implementation VideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *resourceName = @"WidgetTutorial";
    
    if (self.forMessage) {
        resourceName = @"MessageTutorial";
    }
    
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:resourceName ofType:@"mp4"]];
    self.moviePlayerController = [[MPMoviePlayerController alloc] initWithContentURL:url];
    self.moviePlayerController.view.frame = self.view.frame;
    self.moviePlayerController.controlStyle = MPMovieControlStyleFullscreen;
    
    [self.view addSubview:self.moviePlayerController.view];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissVideoController) name:MPMoviePlayerPlaybackDidFinishNotification object:self.moviePlayerController];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.moviePlayerController play];
}

- (void)dismissVideoController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
