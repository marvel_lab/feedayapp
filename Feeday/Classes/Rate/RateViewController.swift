//
//  RateViewController.swift
//  Feeday
//
//  Created by 陈圣晗 on 21/05/2017.
//  Copyright © 2017 Feeday. All rights reserved.
//

import UIKit
import FeedayCommonKit

class RateHelper: NSObject {

    private static let ACCESS_COUNT_KEY: String = "RateViewController_accessCount"

    static var didPresentRateGuide: Bool = false

    static func shouldDisplayRateGuide() -> Bool {
        let count =  getAccessCount()
        let flag = count % 5 == 0
        return flag && !didPresentRateGuide
    }

    static func getAccessCount() -> Int {
        let count = NSUserDefaults.standardUserDefaults().integerForKey(ACCESS_COUNT_KEY)
        return count ?? 0
    }

    static func increaseAccessCount() {
        NSUserDefaults.standardUserDefaults().setInteger(getAccessCount() + 1, forKey: ACCESS_COUNT_KEY)
        NSUserDefaults.standardUserDefaults().synchronize()
    }

    static func openReviewWebPage() {
        let appId = "986398806"
        let urlStr = "https://itunes.apple.com/app/viewContentsUserReviews?id=\(appId)"
        let url = NSURL(string: urlStr)!

        if UIApplication.sharedApplication().canOpenURL(url) {
            UIApplication.sharedApplication().openURL(url)
        }
    }
}


class RateViewController: UIViewController {

    @IBOutlet weak var alertBoardView: UIView?

    static func presentIfNeed(uponViewController baseViewController: UIViewController?) {
        guard RateHelper.shouldDisplayRateGuide() else { return }
        guard ShareDefaultsManager.sharedManager.isLoggedIn else { return }

        guard let rateViewController = UIStoryboard
            .init(name: "Rate", bundle: NSBundle.init(forClass: RateViewController.self))
            .instantiateInitialViewController() as? RateViewController
            else {
                return
        }

        RateHelper.didPresentRateGuide = true
        baseViewController?.presentViewController(rateViewController, animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        alertBoardView?.feeday_roundCorner(13)
    }

    @IBAction func positiveButtonPressed() {
        RateHelper.openReviewWebPage()
    }

    @IBAction func negtiveButtonPressed() {
        dismissViewControllerAnimated(true, completion: nil)
    }
}
